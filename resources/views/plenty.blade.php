
<section class="space-ptb bg-holder-bottom building-space" style="background-image:url('{{asset('images/building-bg.png')}}');">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-9">
        <div class="section-title mb-0">
          <h2 class="text-center">Plenty of reasons to choose us</h2>
          <p class="text-center mx-auto">Our agency has many specialized areas, but our CUSTOMERS are our real specialty.</p>
        </div>
      </div>
      <div class="col-lg-3 text-lg-right">
        <a class="btn btn-success" href="{{url('About')}}">More about us </a>
      </div>
    </div>
    <div class="row no-gutters mt-4">
      <div class="col-lg-3 col-sm-6">
        <div class="feature-info h-100">
          <div class="feature-info-icon">
            <i class="flaticon-like"></i>
          </div>
          <div class="feature-info-content">
            <h6 class="mb-3 feature-info-title">Excellent reputation</h6>
            <p class="mb-0">Our comprehensive database of listings and market info give the most accurate view of the market and your home value.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6">
        <div class="feature-info h-100">
          <div class="feature-info-icon">
            <i class="flaticon-agent"></i>
          </div>
          <div class="feature-info-content">
            <h6 class="mb-3 feature-info-title">Best local agents</h6>
            <p class="mb-0">You are just minutes from joining with the best agents who are fired up about helping you Buy or sell.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6">
        <div class="feature-info h-100">
          <div class="feature-info-icon">
            <i class="flaticon-like-1"></i>
          </div>
          <div class="feature-info-content">
            <h6 class="mb-3 feature-info-title">Peace of mind</h6>
            <p class="mb-0">Rest guaranteed that your agent and their expert team are handling every detail of your transaction from start to end.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6">
        <div class="feature-info h-100">
          <div class="feature-info-icon">
            <i class="flaticon-house-1"></i>
          </div>
          <div class="feature-info-content">
            <h6 class="mb-3 feature-info-title">Tons of options</h6>
            <p class="mb-0">Discover a place you’ll love to live in. Choose from our vast inventory and choose your desired house.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-center mt-5">
      <div class="col-lg-7 text-center">
        <p class="mb-4">Ten years and thousands of home buyers have turned to Ghardhundo to find their dream home. We offer a comprehensive list of for-sale properties, as well as the knowledge and tools to make informed real estate decisions. Today, more than ever, Ghardhundo is the home of home Search.</p>
        <div class="popup-video">
          <a class="popup-icon popup-youtube" href="https://www.youtube.com/watch?v=LgvseYYhqU0"> <i class="flaticon-play-button"></i> </a>
        </div>
      </div>
    </div>
  </div>
</section>
  
