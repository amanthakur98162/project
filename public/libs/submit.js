submit =()=>{
    let token= $("input[name=_token]").val()
    let email=$('#email').val()
    let Property_Title = $('#Property_Title').val()
    let  Offer= $('#Offer').val()
    let Rental_Period = $('#Rental_Period').val()
    let Property_Type = $('#Property_Type').val()
    let Property_price = $('#Property_price').val()
    let Area = $('#Area').val()
    let Rooms = $('#Rooms').val()
    let Video = $('#Video').val()
    let Images=document.getElementById('Images').files[0]
    let Google_Maps_Address = $('#Google_Maps_Address').val()
    let Friendly_Address = $('#Friendly_Address').val()
    let Longitude = $('#Longitude').val()
    let Latitude = $('#Latitude').val()
    let Regions = $('#Regions').val()
    let Building_age = $('#Building_age').val()
    let Bedrooms = $('#Bedrooms').val()
    let Bathrooms = $('#Bathrooms').val()
    let Parking = $('#Parking').val()
    let AC = $('#AC').val()
    let Heater = $('#Heater').val()
    let Sewer = $('#Sewer').val()
    let Water = $('#Water').val()
    let Kitchen = $('#Kitchen').val()
    let Balcony = $('#Balcony').val()
    let File=document.getElementById('File').files[0]
    let Floorplan_Title = $('#Floorplan_Title').val()
    let Areatype = $('#Areatype').val()
    let Description = $('#Description').val()
    let url ='submit';
    let formData = new FormData();
     formData.append('_token',token);
     formData.append('email',email);
     formData.append('Property_Title',Property_Title);
     formData.append('Offer',Offer);
     formData.append('Rental_Period',Rental_Period);
     formData.append('Property_Type',Property_Type);
     formData.append('Property_price', Property_price);
     formData.append('Area',Area);
       formData.append('Rooms',Rooms);
       formData.append('Video',Video);
       formData.append('Images',Images);
       formData.append('Google_Maps_Address',Google_Maps_Address);
       formData.append('Friendly_Address',Friendly_Address);
       formData.append('Longitude',Longitude);
       formData.append('Latitude',Latitude);
     formData.append('Regions',Regions);
     formData.append('Building_age',Building_age);
     formData.append('Bedrooms',Bedrooms);
     formData.append('Bathrooms',Bathrooms);
     formData.append('Parking',Parking);
     formData.append('AC',AC);
     formData.append('Heater',Heater);
     formData.append('Sewer',Sewer);
     formData.append('Water',Water);
     formData.append('Kitchen',Kitchen);
     formData.append('Balcony',Balcony);
     formData.append('File',File);
     formData.append('Floorplan_Title',Floorplan_Title);
     formData.append('Areatype',Areatype);
     formData.append('Description',Description);
     let xhr = new XMLHttpRequest();
     xhr.open('POST',url)
     xhr.send(formData)
     xhr.onload = function(){
     console.log(xhr.responseText);
      let obj=JSON.parse(xhr.responseText);[]
      console.log(obj);  
     let st=obj.status;
    let me=obj.message;
     if(obj.status == false){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
        footer: '<span style="color:red">'+me+'<span>',
      })
            return false;
      }
      Swal.fire({
        icon: 'success',
        title: me,
        showConfirmButton: false,
        timer: 2000
      })
         setTimeout(() => {
            window.location.href = 'submit'
        }, 2000);  
}
}
