<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class CreateSubmitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submits', function (Blueprint $table) {
            $table->id();
            $table->string('Property_title')->nullable();
            $table->string('Offer')->nullable();
            $table->string('Rental_Period')->nullable();
            $table->string('Property_Type')->nullable();
            $table->string('Property_price')->nullable();
            $table->string('Area')->nullable();
            $table->string('Rooms')->nullable();
            $table->string('Video')->nullable();
            $table->string('Images')->nullable();
            $table->string('Google_Maps_Address')->nullable();
            $table->string('Friendly_Address')->nullable();
            $table->string('Longitude')->nullable();
            $table->string('Latitude')->nullable();
            $table->string('Regions')->nullable();
            $table->string('Building_age')->nullable();
            $table->string('Bedrooms')->nullable();
            $table->string('Bathrooms')->nullable();
            $table->string('Parking')->nullable();
            $table->string('AC')->nullable();
            $table->string('Heater')->nullable();
            $table->string('Sewer')->nullable();
            $table->string('Water')->nullable();
            $table->string('Kitchen')->nullable();
            $table->string('Balcony')->nullable();
            $table->string('File')->nullable();
            $table->string('Floorplan_Title')->nullable();
            $table->string('Areatype')->nullable();
            $table->string('Description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submits');
    }
}
