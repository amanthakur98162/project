<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div style="margin:100px;">
   <p style="text-align:center;color:#d75a4a;font-size:24px;font-weight:500;">Sorry! your payment failed!</p>
   <p style="text-align:center;padding:10px;color:#555555;">Transaction ID:&nbsp;<strong style="font-weight:500;font-size:16px;color: #222222;">{{$data['txn_id']}}</strong>
   <br>Payment amount:&nbsp;<strong style="font-weight:500;font-size:16px;color: #222222;">Rs.{{$data['price']}}</strong>
   <br>If your payment got detucted for above transaction, the same shall be credited back to your account in<strong style="font-weight:500;font-size:15px;color: #222222;"> 5 working days</strong></p>
        </div>
</body>
</html>