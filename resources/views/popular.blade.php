
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="https://unpkg.com/jarallax@1.9.3/dist/jarallax.css">
<script src="https://unpkg.com/jarallax@1.12.5/dist/jarallax.min.js"></script>
<script src="https://unpkg.com/jarallax@1.12.5/dist/jarallax-video.min.js"></script>
<style>
.jarallax {
  position: relative;
  float: left;
  width: 100%;
  height:100%;
}
.jarallax:after {
  content: '';
  display: block;
  padding-top: 56%;
}

.jarallax-video-pause {
  position: absolute;
  top: 10px;
  left: 10px;
  padding: 10px 20px;
  background-color: #fff;
  cursor: pointer;
}


</style>
</head>
<body>
  
<div class="container py-4">
    <div class="row justify-content-center">
      <div class="col-lg-8">
        <div class="section-title text-center">
          <h2>Most popular places</h2>
          <p>Discover homes in Ghardhundo Most Popular Cities</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="row">
          <div class="col-md-6 mb-4 mb-lg-0">
            <a href="property-grid.html">
              <div class="location-item bg-overlay-gradient bg-holder" style="background-image:url('{{asset('images/location/01.jpg')}}');;">
                <div class="location-item-info">
                  <h5 class="location-item-title">Shimla</h5>
                  <span class="location-item-list">10 Properties</span>
                </div>
              </div>
            </a>
          </div>
          <div class="col-md-6 mb-4 mb-md-0">
            <a href="property-grid.html">
              <div class="location-item bg-overlay-gradient bg-holder" style="background-image:url('{{asset('images/location/02.jpg')}}');;">
                <div class="location-item-info">
                  <h5 class="location-item-title">Dharamshala</h5>
                  <span class="location-item-list">14 Properties</span>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-12 mt-0 mt-lg-4">
            <a href="">
              <div class="location-item bg-overlay-gradient bg-holder" style="background-image:url('{{asset('images/location/04.jpg')}}');">
                <div class="location-item-info">
                  <h5 class="location-item-title">Chandigarh</h5>
                  <span class="location-item-list">22 Properties</span>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-lg-6 mt-4 mt-lg-0">
        <a href="#">
        <div class="jarallax" data-video-src="https://youtu.be/48Xu8_-orms" data-speed="1"></div>
        </a>
      </div>
    </div>
  </div>
  
  <script>
  $('.jarallax').jarallax({
  videoVolume: 0,
  onInit: function () {
    var self = this;
    var video = self.video;
    if (video) {
      var $muteBtn = $('<div class="jarallax-video-pause">unmute</div>');
      video.on('ready', function() {
        $(self.$item).append($muteBtn);
      });
      
      $muteBtn.on('click', function () {
        video.getMuted(function (muted) {
          if (muted) {
            video.unmute();
            $muteBtn.text('mute');
          } else {
            video.mute();
            $muteBtn.text('unmute');
          }
        });
      });
    }
  }
});


  </script>
</body>
</html>


