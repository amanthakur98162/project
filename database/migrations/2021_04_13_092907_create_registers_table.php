<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->id();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('identity_type')->nullable();
            $table->string('identity_number')->unique();
            $table->string('password')->nullable();
            $table->string('confirm_password')->nullable();
            $table->string('user_type')->nullable();
            $table->string('locality')->nullable();
            $table->string('city')->nullable();
            $table->string('profile_pic')->nullable();
            $table->string('address')->nullable();
            $table->string('status')->default(0)->comment('0=not_verified,1=verified,2=blocked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
