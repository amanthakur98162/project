<?php

namespace App\Http\Controllers;
use App\Models\payment;
use Illuminate\Http\Request;
class paymcont2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paymentSignupForm(Request $request)
    {
        //  echo "<pre>";
        // print_r($request->all());
        //  echo "</pre>";
        $submit = new Payment();
        $submit->user_id = $request->user_id;
        $submit->Order_id = $request->order_id;
        $submit->Transaction_id = $request->transaction_id;
        $submit->property_id = $request->property_id;
        $submit->prize = $request->prize;
        $submit->discount = $request->discount;
        $submit->save();

        session()->flash('success','Data inserted successfully');
        return redirect('paymentsignup');
    }
    public function editDetails($id)
    {
        //$getParticularData = Register::where('id ,$id)->get();
        $getParticularData = Payment::where('id',$id)->first();
        if($getParticularData){
            return view('edit_payment_data')->with('editData',$getParticularData);
        }
        session()->flash('error','Details are not in DB');
        return redirect('paymentsignup');
        //echo "<pre>";
        //print_r($getParticularData);
       // echo "</pre>";
       //   echo $id;

    }
    public function updateForm(Request $request){
        $getParticularData = Payment::where('id',$request->id)->first();
        //echo "<pre>";
        //print_r($request->all());
        //echo "</pre>";
        $getParticularData->Email = $request->email;
        $getParticularData->Order_id = $request->order_id;
        $getParticularData->Transaction_id = $request->transaction_id;
        $getParticularData->Amount = $request->amount;
        $getParticularData->save();
        session()->flash('success','Data updated successfully');
        return redirect('paymentsignup');
    }
    public function deleteDetails($id)
    {
        $getParticularData = Payment::where('id',$id)->first();
        if($getParticularData){
            $getParticularData->delete();
            session()->flash('success','Data deleted successfully');
            return redirect('paymentsignup');
        }
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
