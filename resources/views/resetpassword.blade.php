<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forget Password</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
 .field-icon {
  margin-left: -25px;
  margin-top:-25px;
  float:right;
  margin-right:10px;
  position: relative;
  z-index: 2;
}
</style>
</head>
<body>
<div class="container my-4">
	<div class="row my-5">
    <div class="col-md-12 mt-5">
    @if(Session::has('error'))
                <div class="alert alert-success mb-3 text-center">{{Session::get('error')}}</div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-danger mb-3 text-center">{{Session::get('success')}}</div>
            @endif
             <div id="msgg" class="text-center mb-4 py-3"></div>
    <h1 class="text-center text-success mb-5  "> Forget Password</h1>
       @csrf
       <div class="col-md-6 mx-auto ">
       <input type="hidden" value="{{$token}}" name="passwordToken" id="passwordToken">
            </div>
          <div class="col-md-6 mx-auto ">
           <label for="" class="my-2">New Password</label>
           <input  type="password" class="form-control" name="pass" id="pass">
           <span toggle="#pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
            </div>
    
          <div class="col-md-6 mx-auto">
          <label for=""class="my-2">Confirm Password</label>
          <input  type="password" class="form-control" name="confirm_pass" id="confirm_pass">
          <span toggle="#confirm_pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
        </div>
          <div class="col-md-6 mx-auto">
            <input  type="submit" class="form-control btn btn-success my-4" value="SUBMIT " onclick="forgetpass()">
          </div>
          <div class="col-md-6 mx-auto">
            <a class="text-dark" style="margin-left:38%;" href="{{url('login')}}">Click here to login</a>
          </div>
    </div>   
	</div>
</div>

<script src="{{asset('libs/register.js')}}"></script>
<script>
$(".toggle-password").click(function() {
$(this).toggleClass("fa-eye fa-eye-slash");
var input = $($(this).attr("toggle"));
if (input.attr("type") == "password") {
  input.attr("type", "text");
} else {
  input.attr("type", "password");
}
});
   </script> 
</body>
</html>