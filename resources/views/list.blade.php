<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ghardhundo</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/select2/select2.css')}}">
          <script src="{{asset('js/select2/select2.full.js')}}"></script>
          <link rel="stylesheet" href="{{asset('css/style.css')}}">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">

    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    <style>
input[type='checkbox']{
height:10px;
width:10px;
background-color: #26ae61;
position: relative;
outline: none;
-webkit-appearance: none;
transform: rotate(45deg);
}
input[type='checkbox']::before {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    top: -68%;
    left: 0px;
    border-radius: 75px 75px 0 0;
}

input[type='checkbox']::after {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    transform: rotate( 
-90deg
 );
    border-radius: 75px 75px 0 0;
    top: 12%;
    right: 75%;
}
input[type='checkbox']:checked{
  background-color: red;
}
input:checked[type='checkbox']::after{
  background-color: red;
}
input:checked[type='checkbox']::before{
  background-color: red;
}   
.short-by{
       width:179px;
   }
   .short-by-2{
       width:100px;
   }

.heart {
  font-size: 25px;
	color:red;
}
    </style>
  </head>
<body>
@include('header')
    <section class="space-ptb">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="section-title mb-3 mb-lg-4">
              <h2><span class="text-success">{{count($list)}}</span> Results</h2>
            </div>
          </div>
          <div class="col-md-6">
            <div class="property-filter-tag">
              <ul class="list-unstyled">
                <li><a href="#">Investment <i class="fas fa-times-circle"></i> </a></li>
                <li><a class="filter-clear" href="#">Reset Search <i class="fas fa-redo-alt"></i> </a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-4 mb-5 mb-md-0">
            <div class="sidebar">
              <div class="widget">
                <div class="widget-title">
                  <h6>Featured property</h6>
                </div>
                <div class="property-item mb-0">
                  <div class="property-image bg-overlay-gradient-04">
                    <img class="img-fluid" src="{{asset('images/property/grid/06.jpg')}}" alt="">
                    <div class="property-lable">
                      <span class="badge badge-md badge-success">Studio</span>
                      <span class="badge badge-md badge-info">New </span>
                    </div>
                    <div class="property-agent-popup">
                      <a href="#"><i class="fas fa-camera"></i> 02</a>
                    </div>
                  </div>
                  <div class="property-details">
                    <div class="property-details-inner">
                      <h5 class="property-title"><a href="{{url('property')}}">184 lexington avenue</a></h5>
                      <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>Hamilton rd. willoughby, oh</span>
                      <span class="property-agent-date"><i class="far fa-clock fa-md"></i>3 years ago</span>
                      <div class="property-price">$236.00<span> / month</span> </div>
                    </div>
                    <div class="property-btn">
                      <a class="property-link" href="{{url('property')}}">See Details</a>
                    </div>
                  </div>
              </div>
              </div>
              <div class="widget">
                <div class="widget-title">
                  <h6>Recently listed properties</h6>
                </div>
  @if(count($list)==0)
   <div class="row">
       <div class="col-md-12 text-center p-5">
           <h6 class="border border-primary p-5">No Data Available</h6>
       </div>
   </div>
    @endif
                @for($i=0;$i<$j=count($list);$i++)
                <div class="recent-list-item">
                  <img class="img-fluid" src="{{asset('images/property/list/01.jpg')}}" alt="">
                  <div class="recent-list-item-info">
                    <a class="address mb-2" href="{{url('property/'.$list[$i]['id'])}}">{{$list[$i]['Property_title']}}</a>
                    <span class="text-success">${{$list[$i]['Property_price']}}</span>
                  </div>
                </div>
                @endfor
              </div>
            </div>
          </div>
          <div class="col-lg-9 col-md-8">
            <div class="property-filter d-sm-flex">
              <ul class="property-short list-unstyled d-sm-flex mb-0">
                <li>
                  <form class="form-inline">
                    <div class="form-group d-lg-flex d-block">
                      <label class="justify-content-start">Short by:</label>
                      <div class="short-by">
                      <select class="form-control js-select2 " js-select2-id="1" tabindex="-1" aria-hidden="true">
                          <option >Date new to old</option>
                          <option>Price Low to High</option>
                          <option>Price High to Low</option>
                          <option>Date Old to New</option>
                          <option>Date New to Old</option>
                        </select>
                      </div>
                    </div>
                  </form>
                </li>
              </ul>
              <ul class="property-view-list list-unstyled d-flex mb-0">
                <li>
                  <form class="form-inline">
                    <div class="form-group d-lg-flex d-block">
                      <label class="justify-content-start pr-2">Sort by: </label>
                      <div class="short-by-2">
                      <select class="form-control js-select2 " js-select2-id="1" tabindex="-1" aria-hidden="true">
                          <option>12</option>
                          <option>18 </option>
                          <option>24 </option>
                          <option>64 </option>
                          <option>128</option>
                        </select>
                      </div>
                    </div>
                  </form>
                </li>
              </ul>
            </div>
            <div class="row">
            <div class="property-item property-col-list mt-4">
  @if(count($list)==0)
   <div class="row">
       <div class="col-md-12 text-center p-5 " style="margin-left:50%">
           <h1 class="">No Data Available</h1>
       </div>
   </div>
    @endif
        <div class="row mt-4">
        @for($i=0;$i<$j=count($list);$i++)
        <div class="col-sm-6">
          @csrf
            <div class="property-item">
              <div class="property-image bg-overlay-gradient-04">
              @if($list[$i]['Offer']=='Sold')
                  <img class="img" src="{{asset('images/download.png')}}" height="100px" width="200px"alt="" style="position:absolute;transform:rotate(30deg);z-index:1;top:80px;left:60px;">
                  @endif
                <img class="img-fluid" src="{{asset('images/property/grid/01.jpg')}}" alt="">
                <div class="property-lable">
                  <span class="badge badge-md badge-primary">{{$list[$i]['Property_Type']}}</span>
                  <span class="badge badge-md badge-info">{{$list[$i]['Offer']}}</span>
                </div>
                <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>
                <div class="property-agent">
                  <div class="property-agent-image">
                    <img class="img" src="../profile_images/{{$list[$i]['userdata']['profile_pic']}}" height="34px"width="60px" alt="">
                  </div>
                  <div class="property-agent-info">
                    <a class="property-agent-name" href="#">{{$list[$i]['userdata']['firstname']}}{{$list[$i]['userdata']['lastname']}}</a>
                    <span class="d-block"></span>
                    <ul class="property-agent-contact list-unstyled">
                      <li><a href="#"><i class="fas fa-mobile-alt"></i></a></li>
                      <li><a href="#"><i class="fas fa-envelope"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="property-agent-popup">
                  <a href="#"><i class="fas fa-camera"></i> 06</a>
                </div>
              </div>
              <div class="property-details">
                <div class="property-details-inner">
                  <h5 class="property-title"><a href="property-detail-style-01.html"> </a></h5>
                  <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i></span>
                  <span class="property-agent-date"><i class="far fa-clock fa-md"></i>10 days ago</span>
                  <div class="property-price">$&nbsp;{{$list[$i]['Property_price']}}<span>/ month</span> </div>
                  <ul class="property-info list-unstyled d-flex">
                    <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>{{$list[$i]['Bedrooms']}}</span></li>
                    <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>{{$list[$i]['Bathrooms']}}</span></li>
                    <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>sqft<span>{{$list[$i]['Area']}}m</span></li>
                  </ul>
                </div>
                <div class="property-btn">
                  <a class="property-link" href="{{url('property/'.$list[$i]['id'])}}">See Details</a>
                  <ul class="property-listing-actions list-unstyled mb-0">
                    <li class="property-compare d-none"><a data-toggle="tooltip" data-placement="top" title="" href="#" data-original-title="Compare"><i class="fas fa-exchange-alt"></i></a></li>
                    @if($list[$i]['savecheck']==1)
                    <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" data-value="{{$list[$i]['id']}}" id="check" onclick="getid(this)"></i></a></li>
                 @else
                    <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="far fa-heart" style="font-size:28px;color:red" data-value="{{$list[$i]['id']}}" id="check" onclick="getid(this)"></i></a></li>
                    @endif
                </div>
              </div>
            </div>
          </div>
          @endfor
        </div>   
    </div>
          </div>
            <div class="row">
              <div class="col-12">
                <ul class="pagination mt-5">
                  <li class="page-item disabled mr-auto">
                    <span class="page-link b-radius-none">Prev</span>
                  </li>
                  <li class="page-item active" aria-current="page"><span class="page-link">1 </span> <span class="sr-only">(current)</span></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item ml-auto">
                    <a class="page-link b-radius-none" href="#">Next</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@include('emailsection')
@include('footer')

  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.js"></script>
<script>
$(document).ready(function(){
	$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});

		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 1000);
			return false;
		});});
</script>
<script>
            $(document).ready(function() {
                $(".js-select2").select2();
                // $(".js-select2-multi").select2();

                // $(".large").select2({
                //     dropdownCssClass: "big-drop",
                // });
            });
            getid=(data)=>{
        let p_id=$(data).attr('data-value');
        let _token= $("input[name=_token]").val()
        var check=1;
        $('#check').click(function(){
          $(this).removeClass("fa-heart-o")
          $(this).addClass("fa-heart")
        location.reload();
        })       
         let url = "getlist";
         let formData = new FormData()
         formData.append('p_id',p_id)
         formData.append('check',check)
         formData.append('_token',_token)
         let xhr = new XMLHttpRequest
         xhr.open('POST',url)
         xhr.send(formData)
         xhr.onload = function(){
            let Obj = JSON.parse(xhr.responseText);
            let st = Obj.status;
            let me = Obj.message;
            if(st==false){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    footer: '<span style="color:red">'+me+'<span>',
                  })
                  if(me=='already saved'){
                    $(this).removeClass("fa-heart-o")
                    $(this).addClass("fa-heart")
                  }
                  return false;
            }
            Swal.fire({
                icon: 'success',
                title: me,
                showConfirmButton: false,
                timer: 2000
              })
        location.reload();
           }
}
      </script>
</body>
</html>