<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">

    <style>
        body {
            background: #fff;
        }

        .rounded {
            border-radius: 1rem;
        }

        .nav-pills .nav-link {
            color: #26ae61;
        }

        .nav-pills .nav-link.active {
            color: white;
        }

        input[type="radio"] {
            margin-right: 5px
        }

        .bold {
            font-weight: bold
        }
        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: #fff;
            background-color: #26ae61;
        }

        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
       
</style>
</head>
<body>

<div class="container py-5">
    <!-- For demo purpose -->
    @if(Session::has('error'))
    <div class="alert alert-danger">{{Session::get('error')}}</div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-success text-center">{{Session::get('success')}}</div>
    @endif
    <div class="row mb-4">
        <div class="col-lg-8 mx-auto text-center">
            <h5 class="display-6">Payment</h5>
        </div>
    </div> <!-- End -->
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <div class="card ">
                <div class="card-header">
                    <div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
                        <!-- Credit card form tabs -->
                        <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
                            <li class="nav-item"> <a data-toggle="pill" href="#credit-card" class="nav-link active "> <i class="fas fa-credit-card mr-2"></i> All Card </a> </li>
                            <li class="nav-item"> <a data-toggle="pill" href="#paypal" class="nav-link "> <i class="fab fa-paypal mr-2"></i> Paypal </a> </li>
                            <li class="nav-item"> <a data-toggle="pill" href="#net-banking" class="nav-link "> <i class="fas fa-mobile-alt mr-2"></i> Net Banking </a> </li>
                        </ul>
                    </div> <!-- End -->
                    <!-- Credit card form content -->
                    <div class="tab-content">
                        <!-- credit card info-->
                        <div id="credit-card" class="tab-pane fade show active pt-3">
                            <form role="form" method="post" action="{{url('Payments')}}">
                            @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"> <label for=""><h6>User id</h6></label> 
                                            <input type="email" name="email" placeholder="Enter your user id" required class="form-control "> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"> <label for=""><h6>Order id</h6></label> 
                                            <input type="number" name="order_id" placeholder="Enter your order id" required class="form-control "> 
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> <label for="username"><h6>Transaction id</h6></label> 
                                    <input type="number" name="transaction_id" placeholder="Enter your transaction id" required class="form-control "> 
                                </div>
                                <div class="form-group"> <label for="username"><h6>Property id </h6></label> 
                                    <input type="number" name="transaction_id" placeholder="Enter your property id" required class="form-control "> 
                                </div>
                                <div class="form-group"> <label for=""><h6>Prize</h6>
                                    </label>
                                    <div class="input-group"> <input type="number" name="amount" class="form-control " required>
                                    </div>
                                </div>
                                <div class="form-group"> <label for=""><h6>Discount</h6>
                                    </label>
                                    <div class="input-group"> <input type="number" name="amount" class="form-control " required>
                                    </div>
                                </div>
                                <div class="card-footer"><input name="submit" type="Submit"  value="Confirm Payment"class="btn btn-success btn-block shadow-sm"> 
                            </form>
                        </div>
                    </div> <!-- End -->
                    <!-- Paypal info -->
                    <div id="paypal" class="tab-pane fade pt-3">
                        <h6 class="pb-2">Select your paypal account type</h6>
                        <div class="form-group "> <label class="radio-inline"> <input type="radio" name="optradio" checked> Domestic </label> <label class="radio-inline"> <input type="radio" name="optradio" class="ml-5">International </label></div>
                        <p> <button type="button" class="btn btn-primary "><i class="fab fa-paypal mr-2"></i> Log into my Paypal</button> </p>
                        <p class="text-muted"> Note: After clicking on the button, you will be directed to a secure gateway for payment. After completing the payment process, you will be redirected back to the website to view details of your order. </p>
                    </div> <!-- End -->
                    <!-- bank transfer info -->
                    <div id="net-banking" class="tab-pane fade pt-3">
                        <div class="form-group "> <label for="Select Your Bank">
                                <h6>Select your Bank</h6>
                            </label> <select class="form-control" id="ccmonth">
                                <option value="" selected disabled>--Please select your Bank--</option>
                                <option>Bank 1</option>
                                <option>Bank 2</option>
                                <option>Bank 3</option>
                                <option>Bank 4</option>
                                <option>Bank 5</option>
                                <option>Bank 6</option>
                                <option>Bank 7</option>
                                <option>Bank 8</option>
                                <option>Bank 9</option>
                                <option>Bank 10</option>
                            </select> </div>
                        <div class="form-group">
                            <p> <button type="button" class="btn btn-primary "><i class="fas fa-mobile-alt mr-2"></i> Proceed Payment</button> </p>
                        </div>
                        <p class="text-muted">Note: After clicking on the button, you will be directed to a secure gateway for payment. After completing the payment process, you will be redirected back to the website to view details of your order. </p>
                    </div> <!-- End -->
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    
</body>
</html>