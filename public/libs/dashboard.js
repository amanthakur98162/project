window.onload = function(){
  getData();
  getsolddata();
  dashcount();
  Data();
  Data2();
  Data3();
  Data4();
  profiledata();
  getpropertyData();
  Saved_home();
  getcartdata();
}
prodata=[];
solddataarray=[];
propertydata=[];
userDataArray=[];
propertyDataArray= [];
propertyDataArray2= [];
propertyDataArray3= [];
propertyDataArray4= [];
mypropertyDataArray=[];
Saved_DataArray=[];
cartDataArray=[];
getData =()=>{
    let url = 'newGetData';
    let xhr = new  XMLHttpRequest();
    xhr.open('get',url);
    xhr.send('');
    xhr.onload = function(){
     // console.log(typeof(xhr.responseText));
        let obj = JSON.parse(xhr.responseText)
      //  console.log(obj);  
        let st = obj.status;
        let me = obj.message;
        if(st == false){
            
            return false;
        }
  
        tag_name = ''
        let data =  obj.data
        userDataArray = data
        for(i=0 ; i<data.length ; i++){
        
            tag_name += '<tr>'
                tag_name += '<td>'+(i+1)+'</td>'
                tag_name += '<td>'+data[i].firstname+data[i].lastname+'</td>'
                tag_name += '<td>'+data[i].email+'</td>'
                tag_name += '<td>'+data[i].phone+'</td>'
                tag_name += '<td>'+data[i].identity_type+'</td>'
                tag_name += '<td>'+data[i].identity_number+'</td>'
                tag_name += '<td>'+data[i].user_type+'</td>'
                tag_name += '<td>'+data[i].locality+'</td>'
                tag_name += '<td>'+data[i].address+'</td>'
                tag_name+='<td><img src="profile_images/'+data[i].profile_pic+'" height="50px" width="50px"></td>'
                tag_name+='<td><a><button class="btn btn-success my-2 mx-2 px-4" onclick="editdata('+i+')" data-toggle="modal" data-target="#myModal">Edit</button></a><a><button class="btn btn-danger my-2 mx-2"  onclick="deleted('+i+')">Delete</button></td>'
                // tag_name+='<td><a href="dash/'+data[i].id+'"><button class="btn btn-success my-2 mx-2 px-4" onclick="editdata()" data-toggle="modal" data-target="#myModal">Edit</button></a><a href="delete/'+data[i].id +'"><button class="btn btn-danger my-2 mx-2" onclick="deleted()">Delete</button></td>'
                tag_name += '</tr>'
            
        }
        $('#data').html(tag_name)
        $('#userdatatable').DataTable();
    }
  }

  editdata=(index)=>{
    // //console.log(dataArray[index])
    //  let url="dash/{id}";
    //  let xhr=new XMLHttpRequest();
    //  xhr.open('get',url);
    //  xhr.send('');
    // xhr.onload=function(){
    // // console.log());
    //  let obj= JSON.parse(xhr.responseText);
    //  console.log(obj);
    //  let st=obj.status;
    //  let me=obj.message;
    //  if(st==false){
    //    }
    //       return false;
    //  }
    console.log(userDataArray[index])
     $('#id').val(userDataArray[index]['id']);
     $('#firstname').val(userDataArray[index]['firstname']);
     $('#lastname').val(userDataArray[index]['lastname']);
     $('#phone').val(userDataArray[index]['phone']);
     $('#email').val(userDataArray[index]['email']);
     $('#identity_type').val(userDataArray[index]['identity_type']);
     $('#identity_number').val(userDataArray[index]['identity_number']);
     $('#user_type').val(userDataArray[index]['user_type']);
     $('#locality').val(userDataArray[index]['locality']);
     $('#city').val(userDataArray[index]['city']);
     $('#address').val(userDataArray[index]['address']);
    }

    update =()=>{
      Swal.fire({
        title: 'Do you want to save the changes?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: `Save`,
        denyButtonText: `Don't save`,
      }).then((result) => {
        if (result.isConfirmed) {
          let token= $("input[name=_token]").val()
          let id=$('#id').val()
          let fname = $('#firstname').val()
          let  lname= $('#lastname').val()
          let email = $('#email').val()
          let phone = $('#phone').val()
          let identity_type = $('#identity_type').val()
          let identity_number = $('#identity_number').val()
          let user_type = $('#user_type').val()
          let locality = $('#locality').val()
          let city = $('#city').val()
          let address = $('#address').val()
          let url ='update';
          let formData = new FormData();
           formData.append('_token',token);
           formData.append('id',id);
           formData.append('firstname',fname);
           formData.append('lastname',lname);
           formData.append('email',email);
           formData.append('phone',phone);
           formData.append('identity_type',identity_type);
           formData.append('identity_number',identity_number);
             formData.append('user_type',user_type);
             formData.append('locality',locality);
             formData.append('city',city);
             formData.append('address',address);
           let xhr = new XMLHttpRequest();
           xhr.open('POST',url)
           xhr.send(formData)
           xhr.onload = function(){
          console.log(xhr.responseText);
            let obj=JSON.parse(xhr.responseText);[]
            console.log(obj);  
           let st=obj.status;
          let me=obj.message;
           if(obj.status == false){
            Swal.fire({
              position: 'top-center',
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!',
              footer: '<span style="color:red">'+me+'<span>',
            })
                  return false;
            } 
            $('#myModal').modal('hide');
        }
          Swal.fire('Saved!', '', 'success')
          location.reload();
        } else if (result.isDenied) {
          Swal.fire('Changes are not saved', '', 'info')
        }
      })
   }  

   deleted=(index)=>{
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        let url="delete/"+userDataArray[index]['id'];
        let xhr=new XMLHttpRequest();
        xhr.open('get',url);
        xhr.send('');
       xhr.onload=function(){
        console.log(xhr.responseText);
        let obj= JSON.parse(xhr.responseText);
        //console.log(obj);
        let st=obj.status;
        let me=obj.message;
        if(st==false){
          Swal.fire({
            position: 'top-center',
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red">'+me+'<span>',
          })
          return false;
        }
      } 
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        location.reload(); 
      }
    })
   }
   
  Data =()=>{
    let url = 'getPropertydetails';
    let xhr = new  XMLHttpRequest();
    xhr.open('get',url);
    xhr.send('');
    xhr.onload = function(){
      //console.log(typeof(xhr.responseText));
        let obj = JSON.parse(xhr.responseText)
       console.log(obj); 
        let st = obj.status;
        let me = obj.message;
        if(st == false){
            
            return false;
        }
        tag_name = ''
        let data =  obj.data
        propertyDataArray = data
        for(i=0 ; i<data.length ; i++){
            tag_name += '<tr>'
                tag_name += '<td>'+(i+1)+'</td>'
                tag_name += '<td>'+data[i].email+'</td>'
                tag_name += '<td>'+data[i].Property_title+'</td>'
                tag_name += '<td>'+data[i].Offer+'</td>'
                tag_name += '<td>'+data[i].Rental_Period+'</td>'
                tag_name += '<td>'+data[i].Property_Type+'</td>'
                tag_name += '<td>'+data[i].Property_price+'</td>'
                tag_name += '<td>'+data[i].Area+'</td>'
                tag_name += '<td>'+data[i].Rooms+'</td>'
                tag_name += '<td>'+data[i].Video+'</td>'
                tag_name+='<td><img src="image/'+data[i].Images+'" height="50px" width="50px"></td>'
                tag_name+='<td><a><button class="btn btn-success my-2 mx-2 px-4" onclick="editSubmitdata('+i+')" data-toggle="modal" data-target="#SubmitdataModal">Edit</button></a><a><button class="btn btn-danger my-2 mx-2"  onclick=" deletesumitformdata('+i+')">Delete</button></td>'
                // tag_name+='<td><a href="dash/'+data[i].id+'"><button class="btn btn-success my-2 mx-2 px-4" onclick="editdata()" data-toggle="modal" data-target="#myModal">Edit</button></a><a href="delete/'+data[i].id +'"><button class="btn btn-danger my-2 mx-2" onclick="deleted()">Delete</button></td>'
                tag_name += '</tr>'
        }
        $('#propertydetails').html(tag_name)
        $('#propertydetailsdatatable').DataTable();
    }
  }

  editSubmitdata=(index)=>{
    console.log(propertyDataArray[index])
     $('#submitid').val(propertyDataArray[index]['id']);
     $('#Property_Title').val(propertyDataArray[index]['Property_title']);
     $('#Offer').val(propertyDataArray[index]['Offer']);
     $('#Rental_Period').val(propertyDataArray[index]['Rental_Period']);
     $('#Property_Type').val(propertyDataArray[index]['Property_Type']);
     $('#Property_price').val(propertyDataArray[index]['Property_price']);
     $('#Area').val(propertyDataArray[index]['Area']);
     $('#Rooms').val(propertyDataArray[index]['Rooms']);
     $('#Video').val(propertyDataArray[index]['Video']);
    }

    submitformdetails =()=>{
      Swal.fire({
        title: 'Do you want to save the changes?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: `Save`,
        denyButtonText: `Don't save`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          let token= $("input[name=_token]").val()
          let id=$('#submitid').val()
          let Property_Title = $('#Property_Title').val()
          let Offer= $('#Offer').val()
          let Rental_Period = $('#Rental_Period').val()
          let Property_Type= $('#Property_Type').val()
          let Property_price = $('#Property_price').val()
          let Area = $('#Area').val()
          let Rooms = $('#Rooms').val()
          let Video = $('#Video').val()
          let url ='updateSubmitformdetails';
          let formData = new FormData();
           formData.append('_token',token);
           formData.append('id',id);
           formData.append('Property_title',Property_Title);
           formData.append('Offer',Offer);
           formData.append('Rental_Period',Rental_Period);
           formData.append('Property_Type',Property_Type);
           formData.append('Property_price',Property_price);
           formData.append('Area',Area);
             formData.append('Rooms',Rooms);
             formData.append('Video',Video);
           let xhr = new XMLHttpRequest();
           xhr.open('POST',url)
           xhr.send(formData)
           xhr.onload = function(){
          console.log(xhr.responseText);
            let obj=JSON.parse(xhr.responseText);[]
            console.log(obj);  
           let st=obj.status;
          let me=obj.message;
           if(obj.status == false){
            Swal.fire({
              position: 'top-center',
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!',
              footer: '<span style="color:red">'+me+'<span>',
            })
                  return false;
            } 
    
        }
          Swal.fire('Saved!', '', 'success')
          location.reload();
        } else if (result.isDenied) {
          Swal.fire('Changes are not saved', '', 'info')
        }
      })
          $('#SubmitdataModal').modal('hide');
     
    }

    deletesumitformdata=(index)=>{
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          let url="deletesubmitdetails/"+propertyDataArray[index]['id'];
          let xhr=new XMLHttpRequest();
          xhr.open('get',url);
          xhr.send('');
         xhr.onload=function(){
          console.log(xhr.responseText);
          let obj= JSON.parse(xhr.responseText);
          //console.log(obj);
          let st=obj.status;
          let me=obj.message;
          if(st==false){
            Swal.fire({
              position: 'top-center',
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!',
              footer: '<span style="color:red">'+me+'<span>',
            })
            return false;
          }
        } 
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          location.reload(); 
        }
      })
     
    }
    Data2 =()=>{
      let url = 'getPropertydetails2';
      let xhr = new  XMLHttpRequest();
      xhr.open('get',url);
      xhr.send('');
      xhr.onload = function(){
        //console.log(typeof(xhr.responseText));
          let obj = JSON.parse(xhr.responseText)
         console.log(obj); 
          let st = obj.status;
          let me = obj.message;
          if(st == false){
              return false;
          }
          tag_name = ''
          let data =  obj.data
          propertyDataArray2 = data
          for(i=0 ; i<data.length ; i++){
              tag_name += '<tr>'
                  tag_name += '<td>'+(i+1)+'</td>'
                  tag_name += '<td>'+data[i].email+'</td>'
                  tag_name += '<td>'+data[i].Google_Maps_Address+'</td>'
                  tag_name += '<td>'+data[i].Friendly_Address+'</td>'
                  tag_name += '<td>'+data[i].Longitude+'</td>'
                  tag_name += '<td>'+data[i].Latitude+'</td>'
                  tag_name += '<td>'+data[i].Regions+'</td>'
                  tag_name += '<td>'+data[i].Building_age+'</td>'
                  tag_name+='<td><a><button class="btn btn-success mx-2 px-4" onclick="editSubmitdata2('+i+')" data-toggle="modal" data-target="#SubmitdataModal2">Edit</button></a></td>'
                  // <a><button class="btn btn-danger my-2 mx-2"  onclick=" deletesumitformdata('+i+')">Delete</button></td>'
                  // tag_name+='<td><a href="dash/'+data[i].id+'"><button class="btn btn-success my-2 mx-2 px-4" onclick="editdata()" data-toggle="modal" data-target="#myModal">Edit</button></a><a href="delete/'+data[i].id +'"><button class="btn btn-danger my-2 mx-2" onclick="deleted()">Delete</button></td>'
                  tag_name += '</tr>'
          }
          $('#propertydetails2').html(tag_name)
          $('#propertydetails2datatable').DataTable()
      }
    }

    editSubmitdata2=(index)=>{
      console.log(propertyDataArray2[index])
      $('#submitid').val(propertyDataArray[index]['id']);
       $('#email').val(propertyDataArray2[index]['email']);
       $('#Google_Maps_Address').val(propertyDataArray2[index]['Google_Maps_Address']);
       $('#Friendly_Address').val(propertyDataArray2[index]['Friendly_Address']);
       $('#Longitude').val(propertyDataArray2[index]['Longitude']);
       $('#Latitude').val(propertyDataArray2[index]['Latitude']);
       $('#Regions').val(propertyDataArray2[index]['Regions']);
       $('#Building_age').val(propertyDataArray2[index]['Building_age']);
     
      }

      submitformdetails2 =()=>{
        Swal.fire({
          title: 'Do you want to save the changes?',
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: `Save`,
          denyButtonText: `Don't save`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            let token= $("input[name=_token]").val()
            let email=$('#email').val()
            let Google_Maps_Address = $('#Google_Maps_Address').val()
            let Friendly_Address= $('#Friendly_Address').val()
            let Longitude = $('#Longitude').val()
            let Latitude= $('#Latitude').val()
            let Regions= $('#Regions').val()
            let Building_age = $('#Building_age').val()
            let url ='updateSubmitformdetails2';
            let formData = new FormData();
             formData.append('_token',token);
             formData.append('email',email);
             formData.append('Google_Maps_Address',Google_Maps_Address);
             formData.append('Friendly_Address',Friendly_Address);
             formData.append('Longitude',Longitude);
             formData.append('Latitude',Latitude);
             formData.append('Regions',Regions);
             formData.append('Building_age',Building_age);
             let xhr = new XMLHttpRequest();
             xhr.open('POST',url)
             xhr.send(formData)
             xhr.onload = function(){
            console.log(xhr.responseText);
              let obj=JSON.parse(xhr.responseText);[]
              console.log(obj);  
             let st=obj.status;
            let me=obj.message;
             if(obj.status == false){
              Swal.fire({
                position: 'top-center',
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
                footer: '<span style="color:red">'+me+'<span>',
              })
                    return false;
              } 
          }
            Swal.fire('Saved!', '', 'success')
            location.reload();
          } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
          }
        })
            $('#SubmitdataModal2').modal('hide');
      }    

      Data3 =()=>{
        let url = 'getPropertydetails2';
        let xhr = new  XMLHttpRequest();
        xhr.open('get',url);
        xhr.send('');
        xhr.onload = function(){
          //console.log(typeof(xhr.responseText));
            let obj = JSON.parse(xhr.responseText)
           console.log(obj); 
            let st = obj.status;
            let me = obj.message;
            if(st == false){
                return false;
            }
            tag_name = ''
            let data =  obj.data
            propertyDataArray3 = data
            for(i=0 ; i<data.length ; i++){
                tag_name += '<tr>'
                    tag_name += '<td>'+(i+1)+'</td>'
                    tag_name += '<td>'+data[i].email+'</td>'
                    tag_name += '<td>'+data[i].Bedrooms+'</td>'
                    tag_name += '<td>'+data[i].Bathrooms+'</td>'
                    tag_name += '<td>'+data[i].Parking+'</td>'
                    tag_name += '<td>'+data[i].AC+'</td>'
                    tag_name += '<td>'+data[i].Heater+'</td>'
                    tag_name += '<td>'+data[i].Sewer+'</td>'
                    tag_name += '<td>'+data[i].Water+'</td>'
                    tag_name += '<td>'+data[i].Kitchen+'</td>'
                    tag_name += '<td>'+data[i].Balcony+'</td>'
                    tag_name+='<td><a><button class="btn btn-success  mx-2 px-4" onclick="editSubmitdata3('+i+')" data-toggle="modal" data-target="#SubmitdataModal3">Edit</button></a></td>'
                    // <a><button class="btn btn-danger my-2 mx-2"  onclick=" deletesumitformdata('+i+')">Delete</button></td>'
                    // tag_name+='<td><a href="dash/'+data[i].id+'"><button class="btn btn-success my-2 mx-2 px-4" onclick="editdata()" data-toggle="modal" data-target="#myModal">Edit</button></a><a href="delete/'+data[i].id +'"><button class="btn btn-danger my-2 mx-2" onclick="deleted()">Delete</button></td>'
                    tag_name += '</tr>'
            }
            $('#propertydetails3').html(tag_name)
            $('#propertydetails3datatable').DataTable()
        }
      }

      editSubmitdata3=(index)=>{
        console.log(propertyDataArray3[index])
        $('#submitid').val(propertyDataArray[index]['id']);
         $('#email').val(propertyDataArray3[index]['email']);
         $('#Bedrooms').val(propertyDataArray3[index]['Bedrooms']);
         $('#Bathrooms').val(propertyDataArray3[index]['Bathrooms']);
         $('#Parking').val(propertyDataArray3[index]['Parking']);
         $('#AC').val(propertyDataArray3[index]['AC']);
         $('#Heater').val(propertyDataArray3[index]['Heater']);
         $('#Sewer').val(propertyDataArray3[index]['Sewer']);
         $('#Water').val(propertyDataArray3[index]['Water']);
         $('#Kitchen').val(propertyDataArray3[index]['Kitchen']);
         $('#Balcony').val(propertyDataArray3[index]['Balcony']);
        }

        submitformdetails3 =()=>{
          Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              let token= $("input[name=_token]").val()
              let email=$('#email').val()
              let Bedrooms = $('#Bedrooms').val()
              let Bathrooms= $('#Bathrooms').val()
              let Parking = $('#Parking').val()
              let AC= $('#AC').val()
              let Heater= $('#Heater').val()
              let Sewer = $('#Sewer').val()
              let Water = $('#Water').val()
              let Kitchen = $('#Kitchen').val()
              let Balcony = $('#Balcony').val()
              let url ='updateSubmitformdetails3';
              let formData = new FormData();
               formData.append('_token',token);
               formData.append('email',email);
               formData.append('Bedrooms',Bedrooms);
               formData.append('Bathrooms',Bathrooms);
               formData.append('Parking',Parking);
               formData.append('AC',AC);
               formData.append('Heater',Heater);
               formData.append('Sewer',Sewer);
               formData.append('Water',Water);
               formData.append('Kitchen',Kitchen);
               formData.append('Balcony',Balcony);
               let xhr = new XMLHttpRequest();
               xhr.open('POST',url)
               xhr.send(formData)
               xhr.onload = function(){
              console.log(xhr.responseText);
                let obj=JSON.parse(xhr.responseText);[]
                console.log(obj);  
               let st=obj.status;
              let me=obj.message;
               if(obj.status == false){
                Swal.fire({
                  position: 'top-center',
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Something went wrong!',
                  footer: '<span style="color:red">'+me+'<span>',
                })
                      return false;
                } 
            }
              Swal.fire('Saved!', '', 'success')
              location.reload();
            } else if (result.isDenied) {
              Swal.fire('Changes are not saved', '', 'info')
            }
          })
              $('#SubmitdataModal3').modal('hide');
         
        }

        Data4 =()=>{
          let url = 'getPropertydetails2';
          let xhr = new  XMLHttpRequest();
          xhr.open('get',url);
          xhr.send('');
          xhr.onload = function(){
            //console.log(typeof(xhr.responseText));
              let obj = JSON.parse(xhr.responseText)
             console.log(obj); 
              let st = obj.status;
              let me = obj.message;
              if(st == false){
                  return false;
              }
              tag_name = ''
              let data =  obj.data
              propertyDataArray4 = data
              for(i=0 ; i<data.length ; i++){
                  tag_name += '<tr>'
                      tag_name += '<td>'+(i+1)+'</td>'
                      tag_name += '<td>'+data[i].email+'</td>'
                      tag_name += '<td>'+data[i].Floorplan_Title+'</td>'
                      tag_name += '<td>'+data[i].Areatype+'</td>'
                      tag_name += '<td>'+data[i].Description+'</td>'
                      tag_name+='<td><a><button class="btn btn-success  mx-2 px-4" onclick="editSubmitdata4('+i+')" data-toggle="modal" data-target="#SubmitdataModal4">Edit</button></a></td>'
                      // <a><button class="btn btn-danger my-2 mx-2"  onclick=" deletesumitformdata('+i+')">Delete</button></td>'
                      // tag_name+='<td><a href="dash/'+data[i].id+'"><button class="btn btn-success my-2 mx-2 px-4" onclick="editdata()" data-toggle="modal" data-target="#myModal">Edit</button></a><a href="delete/'+data[i].id +'"><button class="btn btn-danger my-2 mx-2" onclick="deleted()">Delete</button></td>'
                      tag_name += '</tr>'
              }
              $('#propertydetails4').html(tag_name)
              $('#propertydetails4datatable').DataTable()
          }
        }

        editSubmitdata4=(index)=>{
          console.log(propertyDataArray4[index])
          $('#submitid').val(propertyDataArray[index]['id']);
           $('#email').val(propertyDataArray4[index]['email']);
           $('#Floorplan_Title').val(propertyDataArray4[index]['Floorplan_Title']);
           $('#Areatype').val(propertyDataArray4[index]['Areatype']);
           $('#Description').val(propertyDataArray4[index]['Description']);
          }
          submitformdetails4 =()=>{
            Swal.fire({
              title: 'Do you want to save the changes?',
              showDenyButton: true,
              showCancelButton: true,
              confirmButtonText: `Save`,
              denyButtonText: `Don't save`,
            }).then((result) => {
              /* Read more about isConfirmed, isDenied below */
              if (result.isConfirmed) {
                let token= $("input[name=_token]").val()
            let email=$('#email').val()
            let Floorplan_Title = $('#Floorplan_Title').val()
            let Area= $('#Areatype').val()
            let Description = $('#Description').val()
            let url ='updateSubmitformdetails4';
            let formData = new FormData();
             formData.append('_token',token);
             formData.append('email',email);
             formData.append('Floorplan_Title',Floorplan_Title);
             formData.append('Areatype',Area);
             formData.append('Description',Description);
             let xhr = new XMLHttpRequest();
             xhr.open('POST',url)
             xhr.send(formData)
             xhr.onload = function(){
            console.log(xhr.responseText);
              let obj=JSON.parse(xhr.responseText);[]
              console.log(obj);  
             let st=obj.status;
            let me=obj.message;
             if(obj.status == false){
              Swal.fire({
                position: 'top-center',
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
                footer: '<span style="color:red">'+me+'<span>',
              })
                    return false;
              }}
                Swal.fire('Saved!', '', 'success')
                   location.reload();
              } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
              }
            })
                $('#SubmitdataModal4').modal('hide');
          }

profiledata=(index)=>{
let url="profileData";
let xhr= new XMLHttpRequest;
xhr.open('get',url)
xhr.send()
xhr.onload = function(){
let obj=JSON.parse(xhr.responseText)
let prodata=obj.data;
let st=obj.status;
let me=obj.message;
console.log(prodata);
if(st == false){
  return false;
}
var date=prodata.created_at;
var join=moment(date,"YYYY-MM-DD HH-mm-ss").format("dddd DD/MM/YYYY HH:mm:ss");
$('#first').val(prodata.firstname);
$('#last').val(prodata.lastname);
$('#phone_number').val(prodata.phone);
$('#usertype').val(prodata.user_type);
$('#loc').val(prodata.locality);
$('#cityy').val(prodata.city);
$('#add').val(prodata.address);
$('#aboutclientname').html(prodata.firstname+' '+prodata.lastname);
$('#topnavpro_img').attr("src","profile_images/"+prodata.profile_pic+"")
$('#about_img').attr("src","profile_images/"+prodata.profile_pic+"")
$('#about').val(prodata.about)
$('#aboutmetext').html(prodata.about)
$('#joindate').html('<div class="col-md-12"><span style="color:red;">Joined</span> '+join)+'</div>';
}
}

updateprofiledata=()=>{
    Swal.fire({
      title: 'Do you want to save the changes?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        let token= $("input[name=_token]").val()
        let first=$('#first').val()
        let last = $('#last').val()
        let email= $('#email_address').val()
        let usertype = $('#usertype').val()
        let loc= $('#loc').val()
        let cityy = $('#cityy').val()
        let add = $('#add').val()
        let about = $('#about').val()
        let url ='updateprofiledata';
        let formData = new FormData();
         formData.append('_token',token);
         formData.append('firstname',first);
         formData.append('lastname',last);
         formData.append('email',email);
         formData.append('user_type',usertype);
         formData.append('locality',loc);
         formData.append('city',cityy);
         formData.append('address',add);
        formData.append('about',about);
         let xhr = new XMLHttpRequest();
         xhr.open('POST',url)
         xhr.send(formData)
         xhr.onload = function(){
        console.log(xhr.responseText);
          let obj=JSON.parse(xhr.responseText);[]
          console.log(obj);  
         let st=obj.status;
        let me=obj.message;
         if(obj.status == false){
          Swal.fire({
            position: 'top-center',
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red">'+me+'<span>',
          })
                return false;
          } }
        Swal.fire('Saved!', '', 'success')
        location.reload();
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })

}   
getpropertyData =(index)=>{
  let url = 'GetMypropertyData';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('#nodata').html(me)
          return false;
      }
      tag_names = ''
      let data =  obj.data
      mypropertyDataArray = data
      for(i=0 ; i<mypropertyDataArray.length ; i++){
          tag_names+='  <div class="col-md-4 col-lg-4">'                                                                                                                                    
          tag_names+= '    <div class="property-item">'
          tag_names+= '      <div class="property-image bg-overlay-gradient-04">'
          tag_names+= '        <img class="img-fluid" src="images/property/grid/01.jpg" alt="">'
          tag_names+= '        <div class="property-lable">'
          tag_names+= '          <span class="badge badge-md badge-primary mt-2">'+mypropertyDataArray[i].Property_Type+'</span>'
          tag_names+= '          <span class="badge badge-md badge-info mx-2 mt-2">'+mypropertyDataArray[i].Offer+'</span>'
          tag_names+= '        </div>'
          tag_names+= '        <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>'
          tag_names+= '        <div class="property-agent">'
          tag_names+= '          <div class="property-agent-image">'
          tag_names+= '            <img class="img" src="profile_images/'+mypropertyDataArray[i].user_details.profile_pic+'" height="34px"width="60px" alt="">'
          tag_names+= '          </div>'
          tag_names+= '          <div class="property-agent-info">'
          tag_names+= '            <a class="property-agent-name text-center href="#">'+mypropertyDataArray[i].user_details.firstname+'  '+mypropertyDataArray[i].user_details.lastname+'</a>'
          tag_names+= '            <span class="d-block text-center">'+mypropertyDataArray[i].user_details.user_type+'</span>'
          tag_names+= '            <ul class="property-agent-contact list-unstyled items-align-center">'
          tag_names+= '              <li><a href="#"style="position:relative;"><i class="fas fa-mobile-alt"style="position:absolute;top:30%;left:37%;"></i> </a></li>'
          tag_names+= '              <li><a href="#"style="position:relative;"><i class="fas fa-envelope" style="position:absolute;top:30%;left:32%;"></i> </a></li>'
          tag_names+= '            </ul>'
          tag_names+= '          </div>'
          tag_names+= '        </div>'
          tag_names+= '        <div class="property-agent-popup">'
          tag_names+= '          <a href="#"><i class="fas fa-camera"></i> 06</a>'
          tag_names+= '        </div>'
          tag_names+= '      </div>'
          tag_names+= '      <div class="property-details">'
          tag_names+= '        <div class="property-details-inner">'
          tag_names+= '          <h5 class="property-title"><a href="{{url("property")}}">'+mypropertyDataArray[i].Property_title+' </a></h5>'
          tag_names+= '          <span class="property-address"><i class="fas fa-map-marker-alt fa-xs">'+mypropertyDataArray[i].Google_Maps_Address+'/'+mypropertyDataArray[i].Friendly_Address+'</i></span>'
          tag_names+= '          <span class="property-agent-date"><i class="far fa-clock fa-md"></i>10 days ago</span>'
          tag_names+= '          <div class="property-price">$'+mypropertyDataArray[i].Property_price+'<span> / month</span> </div>'
          tag_names+= '          <ul class="property-info list-unstyled d-flex">'
          tag_names+= '            <li class="flex-fill property-bed"><i class="fa fa-bed"></i>Bed<span>'+mypropertyDataArray[i].Bedrooms+'</span></li>'
          tag_names+= '            <li class="flex-fill property-bath"><i class="fa fa-bath"></i>Bath<span>'+mypropertyDataArray[i].Bathrooms+'</span></li>'
          tag_names+= '            <li class="flex-fill property-m-sqft"><i class="fa fa-square-o"></i>sqft<span>'+mypropertyDataArray[i].Area+'m</span></li>'
          tag_names+= '          </ul>'
          tag_names+= '        </div>'
          tag_names+= '        <div class="property-btn">'
          tag_names+= '          <a class="property-link" href="property/'+mypropertyDataArray[i].id+'">See Details</a>'
          tag_names+= '          <ul class="property-listing-actions list-unstyled mb-0">'
          tag_names+= '            <li class="property-compare"><a data-toggle="tooltip" data-placement="top" title="Compare" href="#"><i class="fa fa-exchange"></i></a></li>'
          tag_names+= '         <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" id="check" onclick="save(mypropertyDataArray[i].id)"></i></a></li>'
          tag_names+= '          </ul>'
          tag_names+= '        </div>'
          tag_names+= '      </div>'
          tag_names+= '    </div>'
          tag_names+= '  </div>'                                                                                                                                                                
                    }
            $('#mypropertydesign').html(tag_names)
                  }
                }

tips=()=>{
        let token= $("input[name=_token]").val()
        let title=$('#title').val()
        let msg = $('#msg').val()
        let email= $('#gmail').val()
        let url ='tips';
        let formData = new FormData();
         formData.append('_token',token);
         formData.append('email',email);
         formData.append('title',title);
         formData.append('msg',msg);
         let xhr = new XMLHttpRequest();
         xhr.open('POST',url)
         xhr.send(formData)
         xhr.onload = function(){
        console.log(xhr.responseText);
          let obj=JSON.parse(xhr.responseText);[]
          console.log(obj);  
         let st=obj.status;
        let me=obj.message;
        if(obj.status == false){
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red">'+me+'<span>',
          })
                return false;
          }

          Swal.fire({
            icon: 'success',
            title: me,
            showConfirmButton: false,
            timer: 2000
          })
          location.reload();
      }
      }
     
      Saved_home =(index)=>{
        let url = 'Saved_home';
        let xhr = new  XMLHttpRequest();
        xhr.open('get',url);
        xhr.send('');
        xhr.onload = function(){
         // console.log(typeof(xhr.responseText));
            let obj = JSON.parse(xhr.responseText)
          // console.log(obj); 
            let st = obj.status;
            let me = obj.message;
            if(st == false){
              $('#nosavedhome').html(me)
                return false;
            }
            tag_names = ''
            let data =  obj.data
          Saved_DataArray = data
            for(i=0 ; i< Saved_DataArray.length ; i++){
                tag_name+='@csrf'
                tag_names+='  <div class="col-md-3 col-lg-3">'                                                                                                                                    
                tag_names+= '    <div class="property-item">'
                tag_names+= '      <div class="property-image bg-overlay-gradient-04">'
                tag_names+= '        <img class="img-fluid" src="images/property/grid/01.jpg" alt="">'
                tag_names+= '        <div class="property-lable">'
                tag_names+= '          <span class="badge badge-md badge-primary mt-2">'+ Saved_DataArray[i]['saved'][0].Property_Type+'</span>'
                tag_names+= '          <span class="badge badge-md badge-info mx-2 mt-2">'+ Saved_DataArray[i]['saved'][0].Offer+'</span>'
                tag_names+= '        </div>'
                tag_names+= '        <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>'
                tag_names+= '        <div class="property-agent">'
                tag_names+= '          <div class="property-agent-image">'
                tag_names+= '            <img class="img" src="profile_images/'+ Saved_DataArray[i]['userdetail'].profile_pic+'" height="34px"width="60px" alt="">'
                tag_names+= '          </div>'
                tag_names+= '          <div class="property-agent-info">'
                tag_names+= '            <a class="property-agent-name text-center href="#">'+ Saved_DataArray[i]['userdetail'].firstname+'  '+Saved_DataArray[i]['userdetail'].lastname+'</a>'
                tag_names+= '            <span class="d-block text-center">'+Saved_DataArray[i]['userdetail'].user_type+'</span>'
                tag_names+= '            <ul class="property-agent-contact list-unstyled items-align-center">'
                tag_names+= '              <li><a href="#"style="position:relative;"><i class="fas fa-mobile-alt"style="position:absolute;top:30%;left:37%;"></i> </a></li>'
                tag_names+= '              <li><a href="#"style="position:relative;"><i class="fas fa-envelope" style="position:absolute;top:30%;left:32%;"></i> </a></li>'
                tag_names+= '            </ul>'
                tag_names+= '          </div>'
                tag_names+= '        </div>'
                tag_names+= '        <div class="property-agent-popup">'
                tag_names+= '          <a href="#"><i class="fas fa-camera"></i> 06</a>'
                tag_names+= '        </div>'
                tag_names+= '      </div>'
                tag_names+= '      <div class="property-details">'
                tag_names+= '        <div class="property-details-inner">'
                tag_names+= '          <h5 class="property-title"><a href="{{url("property")}}">'+ Saved_DataArray[i]['saved'][0].Property_title+' </a></h5>'
                tag_names+= '          <span class="property-address"><i class="fas fa-map-marker-alt fa-xs">'+Saved_DataArray[i]['saved'][0].Google_Maps_Address+'/'+ Saved_DataArray[i]['saved'][0].Friendly_Address+'</i></span>'
                tag_names+= '          <span class="property-agent-date"><i class="far fa-clock fa-md"></i>10 days ago</span>'
                tag_names+= '          <div class="property-price">$'+ Saved_DataArray[i]['saved'][0].Property_price+'<span> / month</span> </div>'
                tag_names+= '          <ul class="property-info list-unstyled d-flex">'
                tag_names+= '            <li class="flex-fill property-bed"><i class="fa fa-bed"></i>Bed<span>'+ Saved_DataArray[i]['saved'][0].Bedrooms+'</span></li>'
                tag_names+= '            <li class="flex-fill property-bath"><i class="fa fa-bath"></i>Bath<span>'+Saved_DataArray[i]['saved'][0].Bathrooms+'</span></li>'
                tag_names+= '            <li class="flex-fill property-m-sqft"><i class="fa fa-square-o"></i>sqft<span>'+Saved_DataArray[i]['saved'][0].Area+'m</span></li>'
                tag_names+= '          </ul>'
                tag_names+= '        </div>'
                tag_names+= '        <div class="property-btn">'
                tag_names+= '          <a class="property-link bg-success text-white" onclick="cart('+Saved_DataArray[i]["saved"][0].id+')"><i class="fas fa-shopping-cart style="font-size:27px"></i>Add to Cart</a>'
                tag_names +='          <a class="bg-danger text-white" onclick="removewishlist('+Saved_DataArray[i]['id']+')"><i class="fa fa-trash" style="font-size:20px;"></i>&nbsp;Remove</a>'
                tag_names+= '        </div>'
                tag_names+= '      </div>'
                tag_names+= '    </div>'
                tag_names+= '  </div>'                                                                                                                                                                
                          }
                  $('#save_homes').html(tag_names)
                        }
                      }         
cart=(i)=>{
  let _token= $("input[name=_token]").val()   
  let id=i;  
   let url = "cart";
   let formData = new FormData()
   formData.append('p_id',id)
   formData.append('_token',_token)
   let xhr = new XMLHttpRequest
   xhr.open('POST',url)
   xhr.send(formData)
   xhr.onload = function(){
      let Obj = JSON.parse(xhr.responseText);
      let st = Obj.status;
      let me = Obj.message;
      if(st==false){
          Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!',
              footer: '<span style="color:red">'+me+'<span>',
            })
            if(me=='already saved'){
              $(this).removeClass("fa-heart-o")
              $(this).addClass("fa-heart")
            }
            return false;
      }
      Swal.fire({
          icon: 'success',
          title: me,
          showConfirmButton: false,
          timer: 2000
        })
        location.reload();
     }
}
getcartdata=()=>{
    let url = 'getcartdata';
    let xhr = new  XMLHttpRequest();
    xhr.open('get',url);
    xhr.send('');
    xhr.onload = function(){
     // console.log(typeof(xhr.responseText));
        let obj = JSON.parse(xhr.responseText)
      // console.log(obj); 
        let st = obj.status;
        let me = obj.message;
        if(st == false){
          $('#nodataincart').html(me)
            return false;
        }
        tag_names = ''
        let data =  obj.data
      cartDataArray = data;
        count=1;
        let subtotal=[];
        let p_id=[];
        for(i=0 ; i< cartDataArray.length ; i++){
          tag_names +='<tr>'
          tag_names +='<th>'+ count++ +'</th>'
          tag_names +='<th><a href="property/'+cartDataArray[i]['prodata'][0]['id'] +'">'+cartDataArray[i]['prodata'][0]['Property_title']+'</a></th>'
          tag_names +='<th>'+ cartDataArray[i]['prodata'][0]['Description']+'</th>'
          tag_names +='<th class="price" >'+ cartDataArray[i]['prodata'][0]['Property_price'] +'</th>'  
          tag_names +='<th><a href="#"><i class="fa fa-trash bg-danger py-2 px-4" style="font:20px" onclick="removecart('+cartDataArray[i]['id']+')"></i></a></th>'
          tag_names +='<tr>'       
         subtotal.push(cartDataArray[i]['prodata'][0]['Property_price']);
              var total = 0;
              for(let j=0;j<subtotal.length;j++)
              {
               total+=parseInt(subtotal[j]);
              }
                  $('#gettotal').val(total) 
                  $('#carttotal').html('Subtotal:'+total)  
                  p_id.push(cartDataArray[i]['prodata'][0]['id'])
                  $('#p_id').val(p_id) 
                  }
  $('#cartdata').html(tag_names)
  if(cartDataArray.length==0){
    $('#cartcount').remove();
  }
  else{
  $('#cartcount').html(cartDataArray.length)
  }
  }}
  removewishlist=(i)=>{
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        let id=i;
        let _token= $("input[name=_token]").val()  
        let url="removewishlist";
        let formData = new FormData();
        formData.append('id',id);
        formData.append('_token',_token);
        let xhr=new XMLHttpRequest();
        xhr.open('post',url);
        xhr.send(formData);
       xhr.onload=function(){
        console.log(xhr.responseText);
        let obj= JSON.parse(xhr.responseText);
        //console.log(obj);
        let st=obj.status;
        let me=obj.message;
        if(st==false){
          Swal.fire({
            position: 'top-center',
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red">'+me+'<span>',
          })
          return false;
        }
      } 
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        location.reload();
      }
    }) 
   }

   removecart=(i)=>{
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        let id=i;
        let _token= $("input[name=_token]").val()  
        let url="removecart";
        let formData = new FormData();
        formData.append('id',id);
        formData.append('_token',_token);
        let xhr=new XMLHttpRequest();
        xhr.open('post',url);
        xhr.send(formData);
       xhr.onload=function(){
        console.log(xhr.responseText);
        let obj= JSON.parse(xhr.responseText);
        //console.log(obj);
        let st=obj.status;
        let me=obj.message;
        if(st==false){
          Swal.fire({
            position: 'top-center',
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red">'+me+'<span>',
          })
          return false;
        }
      } 
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        location.reload();

      }
    })
   }
  getsolddata=()=>{
        let url="sold";
        let xhr=new XMLHttpRequest();
        xhr.open('get',url);
        xhr.send();
       xhr.onload=function(){
        console.log(xhr.responseText);
        let obj= JSON.parse(xhr.responseText);
        //console.log(obj);
        let st=obj.status;
        let me=obj.message;
        if(st==false){
          Swal.fire({
            position: 'top-center',
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red">'+me+'<span>',
          })
          return false;
        }
        tag_name = ''
        let data =  obj.data
        solddataarray = data
        for(i=0 ; i<data.length ; i++){
        
            tag_name += '<tr>'
                tag_name += '<td>'+(i+1)+'</td>'
                tag_name += '<td>'+solddataarray[i].order_id+'</td>'
                tag_name += '<td>'+solddataarray[i].user_email+'</td>'
                tag_name += '<td>'+solddataarray[i].transaction_id+'</td>'  
                tag_name += '<td>'+solddataarray[i].property_id+'</td>' 
                if(solddataarray[i].payment_status==1){
                  tag_name += '<td>Done</td>'
                }  
                else{
                  tag_name += '<td>Pending</td>'
                }
             
                // tag_name+='<td><a href="dash/'+data[i].id+'"><button class="btn btn-success my-2 mx-2 px-4" onclick="editdata()" data-toggle="modal" data-target="#myModal">Edit</button></a><a href="delete/'+data[i].id +'"><button class="btn btn-danger my-2 mx-2" onclick="deleted()">Delete</button></td>'
                tag_name += '</tr>'

        }
        $('#paymentdata').html(tag_name)
        $('#paymentdatatable').DataTable();
    }
  }
  dashcount=()=>{
    let url="dashcount";
    let xhr=new XMLHttpRequest();
    xhr.open('get',url);
    xhr.send();
   xhr.onload=function(){
    console.log(xhr.responseText);
    let obj= JSON.parse(xhr.responseText);
    //console.log(obj);
    let st=obj.status;
    let me=obj.message;
    if(st==false){
      Swal.fire({
        position: 'top-center',
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
        footer: '<span style="color:red">'+me+'<span>',
      })
      return false;
    }
    let data =  obj.data
    $('#dashuser').html(data['user'])
    $('#dashproperty').html(data['property']) 
    $('#dashsold').html(data['payment'])
    $('#dashowner').html(data['owners'])
  }
    
}
  
   