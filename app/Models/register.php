<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class register extends Model
{
    use HasFactory;
    protected $fillable = ['firstname','lastname','email','phone','identity_type','identity_number','password','confirm_password','user_type','locality','city','profile_pic','address','about'];
}
