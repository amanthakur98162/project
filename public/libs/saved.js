Searched_DataArray=[];
save=(i)=>{
        let p_id=i;
        let _token= $("input[name=_token]").val()
        let email=$('#emailll').val();
        var check=1;
        $('#check').click(function(){
          $(this).removeClass("fa-heart-o")
          $(this).addClass("fa-heart")
        location.reload();
        })       
         let url = "saved";
         let formData = new FormData()
         formData.append('p_id',p_id)
         formData.append('check',check)
         formData.append('email',email)
         formData.append('_token',_token)
         let xhr = new XMLHttpRequest
         xhr.open('POST',url)
         xhr.send(formData)
         xhr.onload = function(){
            let Obj = JSON.parse(xhr.responseText);
            let st = Obj.status;
            let me = Obj.message;
            if(st==false){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    footer: '<span style="color:red">'+me+'<span>',
                  })
                  if(me=='already saved'){
                    $(this).removeClass("fa-heart-o")
                    $(this).addClass("fa-heart")
                  }
                  return false;
            }
            Swal.fire({
                icon: 'success',
                title: me,
                showConfirmButton: false,
                timer: 2000
              })
        location.reload();
           }
}
search=()=>{

  let _token= $("input[name=_token]").val()
  let type=$('#type').val();
  let offer=$('#offer').val();
  let location=$('#location').val();

  let bathrooms=$('#bathrooms').val();
  let sortby=$('#sortby').val();
  let bedrooms=$('#bedrooms').val();

  let from=$('#from').val();
  let to=$('#to').val();
  alert(type+''+offer+''+location);
   let url = "searchh";
   let formData = new FormData()
   formData.append('type',type)
   formData.append('offer',offer)
   formData.append('location',location)

   formData.append('bathrooms',bathrooms)
   formData.append('sortby',sortby)
   formData.append('bedrooms',bedrooms)
  
   formData.append('from',from)
   formData.append('to',to)
   formData.append('_token',_token)
   let xhr = new XMLHttpRequest
   xhr.open('POST',url)
   xhr.send(formData)
   xhr.onload = function(){
      let Obj = JSON.parse(xhr.responseText);
      let st = Obj.status;
      let me = Obj.message;
      if(st==false){
          Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!',
              footer: '<span style="color:red">'+me+'<span>',
            })
            if(me=='already saved'){
              $(this).removeClass("fa-heart-o")
              $(this).addClass("fa-heart")
            }
            return false;
      }
      Swal.fire({
          icon: 'success',
          title: me,
          showConfirmButton: false,
          timer: 2000
        })
        setTimeout(() => {
          window.location.href = 'search'
      }, 60);   
      tag_names = ''
      let data =  Obj.data
    Searched_DataArray = data
      for(i=0 ; i< Searched_DataArray.length ; i++){
          tag_name+='@csrf'
          tag_names+='  <div class="col-md-3 col-lg-3">'                                                                                                                                    
          tag_names+= '    <div class="property-item">'
          tag_names+= '      <div class="property-image bg-overlay-gradient-04">'
          tag_names+= '        <img class="img-fluid" src="images/property/grid/01.jpg" alt="">'
          tag_names+= '        <div class="property-lable">'
          tag_names+= '          <span class="badge badge-md badge-primary mt-2">'+ Searched_DataArray[i].Property_Type+'</span>'
          tag_names+= '          <span class="badge badge-md badge-info mx-2 mt-2">'+ Searched_DataArray[i].Offer+'</span>'
          tag_names+= '        </div>'
          tag_names+= '        <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>'
          tag_names+= '        <div class="property-agent">'
          tag_names+= '          <div class="property-agent-image">'
          tag_names+= '            <img class="img" src="profile_images/'+ Searched_DataArray[i]['userdetails'].profile_pic+'" height="34px"width="60px" alt="">'
          tag_names+= '          </div>'
          tag_names+= '          <div class="property-agent-info">'
          tag_names+= '            <a class="property-agent-name text-center href="#">'+ Searched_DataArray[i]['userdetails'].firstname+'  '+Searched_DataArray[i]['userdetails'].lastname+'</a>'
          tag_names+= '            <span class="d-block text-center">'+Searched_DataArray[i]['userdetails'].user_type+'</span>'
          tag_names+= '            <ul class="property-agent-contact list-unstyled items-align-center">'
          tag_names+= '              <li><a href="#"style="position:relative;"><i class="fas fa-mobile-alt"style="position:absolute;top:30%;left:37%;"></i> </a></li>'
          tag_names+= '              <li><a href="#"style="position:relative;"><i class="fas fa-envelope" style="position:absolute;top:30%;left:32%;"></i> </a></li>'
          tag_names+= '            </ul>'
          tag_names+= '          </div>'
          tag_names+= '        </div>'
          tag_names+= '        <div class="property-agent-popup">'
          tag_names+= '          <a href="#"><i class="fas fa-camera"></i> 06</a>'
          tag_names+= '        </div>'
          tag_names+= '      </div>'
          tag_names+= '      <div class="property-details">'
          tag_names+= '        <div class="property-details-inner">'
          tag_names+= '          <h5 class="property-title"><a href="{{url("property")}}">'+ Searched_DataArray[i].Property_title+' </a></h5>'
          tag_names+= '          <span class="property-address"><i class="fas fa-map-marker-alt fa-xs">'+Searched_DataArray[i].Google_Maps_Address+'/'+ Searched_DataArray[i].Friendly_Address+'</i></span>'
          tag_names+= '          <span class="property-agent-date"><i class="far fa-clock fa-md"></i>10 days ago</span>'
          tag_names+= '          <div class="property-price">$'+ Searched_DataArray[i].Property_price+'<span> / month</span> </div>'
          tag_names+= '          <ul class="property-info list-unstyled d-flex">'
          tag_names+= '            <li class="flex-fill property-bed"><i class="fa fa-bed"></i>Bed<span>'+ Searched_DataArray[i].Bedrooms+'</span></li>'
          tag_names+= '            <li class="flex-fill property-bath"><i class="fa fa-bath"></i>Bath<span>'+Searched_DataArray[i].Bathrooms+'</span></li>'
          tag_names+= '            <li class="flex-fill property-m-sqft"><i class="fa fa-square-o"></i>sqft<span>'+Searched_DataArray[i].Area+'m</span></li>'
          tag_names+= '          </ul>'
          tag_names+= '        </div>'
          tag_names+= '        <div class="property-btn">'
          tag_names+= '          <a class="property-link bg-success text-white" onclick="cart('+Searched_DataArray[i]["saved"][0].id+')"><i class="fas fa-shopping-cart style="font-size:27px"></i>Add to Cart</a>'
          tag_names +='          <a class="bg-danger text-white" onclick="removewishlist('+Searched_DataArray[i]['id']+')"><i class="fa fa-trash" style="font-size:20px;"></i>&nbsp;Remove</a>'
          tag_names+= '        </div>'
          tag_names+= '      </div>'
          tag_names+= '    </div>'
          tag_names+= '  </div>'                                                                                                                                                                
                    }
            $('#searched_rslt').html(tag_names)   
     }
}