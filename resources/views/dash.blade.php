
<!DOCTYPE html>
<head>
<title>Ghardhundo</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- bootstrap-css -->
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" >
<!--bootstrap-css -->
<!-- Custom CSS -->
<link href="{{asset('dashboard/css/style.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('dashboard/css/style-responsive.css')}}" rel="stylesheet"/>
<link href="{{asset('css/property.css')}}" rel='stylesheet' type='text/css' /> 
<!-- font-awesome icons -->
<link rel="stylesheet" href="{{asset('dashboard/css/font.css')}} ">
<link href="{{asset('dashboard/css/font-awesome.css')}}" rel="stylesheet"> 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
<script src="{{asset('jquery/jquery.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.js')}}"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('libs/register.js')}}"></script>
<script src="{{asset('libs/dashboard.js')}}"></script>
<script src="{{asset('libs/const.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.2.1/moment.min.js"></script>
<style>
.checkout {
    border: 0;
    margin-top: 8px;
    margin-left: 148px;
    padding: 5px 15px;
    background-color: #6b6;
    color: #fff;
    font-size: 15px;
    border-radius: 3px;
}
.checkout:hover {
  background-color: #494;
}
  .heart{
    font-size:27px;
    color:red;
  }
.page-item.active .page-link {
    z-index: 3;
    color: #fff;
    background-color: #08b46c;
    border-color: #08b46c;
}
div.dataTables_wrapper div.dataTables_info {
    padding-top: .85em;
    color: white;
}
.heading {
    text-align: center;
    color: #454343;
    font-size: 30px;
    font-weight: 700;
    position: relative;
    margin-bottom: 70px;
    text-transform: uppercase;
    z-index: 999;
}
.white-heading{
    color: #ffffff;
}
.heading:after {
    content: ' ';
    position: absolute;
    top: 100%;
    left: 50%;
    height: 40px;
    width: 180px;
    border-radius: 4px;
    transform: translateX(-50%);
    background: url(img/heading-line.png);
    background-repeat: no-repeat;
    background-position: center;
}
.white-heading:after {
    background: url(https://i.ibb.co/d7tSD1R/heading-line-white.png);
    background-repeat: no-repeat;
    background-position: center;
}

.heading span {
    font-size: 18px;
    display: block;
    font-weight: 500;
}
.white-heading span {
    color: #ffffff;
}
/*-----Testimonial-------*/



.testimonial {
    min-height: 375px;
    position: relative;
    background: url('{{asset("images/testimonials.jpg")}}');
    padding-top: 50px;
    padding-bottom: 50px;
    background-position: center;
        background-size: cover;
}
#testimonial4 .carousel-inner:hover{
  cursor: -moz-grab;
  cursor: -webkit-grab;
}
#testimonial4 .carousel-inner:active{
  cursor: -moz-grabbing;
  cursor: -webkit-grabbing;
}
#testimonial4 .carousel-inner .item{
  overflow: hidden;
}

.testimonial4_indicators .carousel-indicators{
  left: 0;
  margin: 0;
  width: 100%;
  font-size: 0;
  height: 20px;
  bottom: 15px;
  padding: 0 5px;
  cursor: e-resize;
  overflow-x: auto;
  overflow-y: hidden;
  position: absolute;
  text-align: center;
  white-space: nowrap;
}
.testimonial4_indicators .carousel-indicators li{
  padding: 0;
  width: 14px;
  height: 14px;
  border: none;
  text-indent: 0;
  margin: 2px 3px;
  cursor: pointer;
  display: inline-block;
  background: #ffffff;
  -webkit-border-radius: 100%;
  border-radius: 100%;
}
.testimonial4_indicators .carousel-indicators .active{
  padding: 0;
  width: 14px;
  height: 14px;
  border: none;
  margin: 2px 3px;
  background-color: #9dd3af;
  -webkit-border-radius: 100%;
  border-radius: 100%;
}
.testimonial4_indicators .carousel-indicators::-webkit-scrollbar{
  height: 3px;
}
.testimonial4_indicators .carousel-indicators::-webkit-scrollbar-thumb{
  background: #eeeeee;
  -webkit-border-radius: 0;
  border-radius: 0;
}

.testimonial4_control_button .carousel-control{
  top: 175px;
  opacity: 1;
  width: 40px;
  bottom: auto;
  height: 40px;
  font-size: 10px;
  cursor: pointer;
  font-weight: 700;
  overflow: hidden;
  line-height: 38px;
  text-shadow: none;
  text-align: center;
  position: absolute;
  background: transparent;
  border: 2px solid #ffffff;
  text-transform: uppercase;
  -webkit-border-radius: 100%;
  border-radius: 100%;
  -webkit-box-shadow: none;
  box-shadow: none;
  -webkit-transition: all 0.6s cubic-bezier(0.3,1,0,1);
  transition: all 0.6s cubic-bezier(0.3,1,0,1);
}
.testimonial4_control_button .carousel-control.left{
  left: 7%;
  top: 50%;
  right: auto;
}
.testimonial4_control_button .carousel-control.right{
  right: 7%;
  top: 50%;
  left: auto;
}
.testimonial4_control_button .carousel-control.left:hover,
.testimonial4_control_button .carousel-control.right:hover{
  color: #000;
  background: #fff;
  border: 2px solid #fff;
}

.testimonial4_header{
  top: 0;
  left: 0;
  bottom: 0;
  width: 550px;
  display: block;
  margin: 30px auto;
  text-align: center;
  position: relative;
}
.testimonial4_header h4{
  color: #ffffff;
  font-size: 30px;
  font-weight: 600;
  position: relative;
  letter-spacing: 1px;
  text-transform: uppercase;
}
@media (min-width: 480px) and (max-width: 767px){
.header {
    position: relative! important;
    margin-top:80px ! important;
    height:80px;
}}
.testimonial4_slide{
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 70%;
  margin: auto;
  padding: 20px;
  position: relative;
  text-align: center;
}
.testimonial4_slide img {
    top: 0;
    left: 0;
    right: 0;
    width: 136px;
    height: 136px;
    margin: auto;
    display: block;
    color: #f2f2f2;
    font-size: 18px;
    line-height: 46px;
    text-align: center;
    position: relative;
    border-radius: 50%;
    box-shadow: -6px 6px 6px rgba(0, 0, 0, 0.23);
    -moz-box-shadow: -6px 6px 6px rgba(0, 0, 0, 0.23);
    -o-box-shadow: -6px 6px 6px rgba(0, 0, 0, 0.23);
    -webkit-box-shadow: -6px 6px 6px rgba(0, 0, 0, 0.23);
}

</style>
</head>
<body>
<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">
    <a href="{{url('index')}}" class="logo">
 Ghardhundo
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->
<div class="nav notify-row" id="top_menu">
   <div style="position:absolute;right:50px;bottom:5px;"><img id="topnavpro_img" src="https://www.nicepng.com/png/full/136-1366211_group-of-10-guys-login-user-icon-png.png" alt="profile_pic" style="height:70px;width:70px;border-radius:50%;border:2px solid #08b46c;"><span class="mx-2 text-dark"><i class="fa fa-circle mx-2 text-success proactive" style="border-radius:50%;background:white;padding:5px;position:absolute;bottom:10px;left:50px"  aria-hidden="true"></i> @if(Session::has('email'))
   {{Session::get('email')}}({{Session::get('user_type')}}) 
    @else
    {{Cookie::get('email')}} ({{Cookie::get('user_type')}})@endif</span>
    </div>
   </div>
</header>
<!--header end-->
<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" id="1" href="">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                @if(Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin'||Cookie::get('user_type')=='User'||Session::get('user_type')=='User'||Cookie::get('user_type')=='Broker'||Session::get('user_type')=='Broker'||Cookie::get('user_type')=='Owner'||Session::get('user_type')=='Owner')
                <li>
                    <a id="2" href="#profile" class="tablinks" onclick="sectionchange(event,'profile')">
                        <i class="fa fa-user"></i>
                        <span>Profile</span>
                    </a>
                </li>
                @endif
                @if(Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin')
					      <li><a id="defaultOpen"  href="#userdetails" class="tablinks " onclick="sectionchange(event, 'user')">  <i class="fa fa-users"></i><span>Users Details</span></a></li>
					      <li><a  id="4" href="#Property Details"class="tablinks"onclick="sectionchange(event, 'user1')">  <i class="fa fa-building"></i><span>Property Details</span></a></li>
                <li><a  id="5" href="#Property Details"class="tablinks"onclick="sectionchange(event, 'user2')">  <i class="fa fa-building"></i><span>Property Details</span></a></li>
                <li><a  id="6" href="#Property Details"class="tablinks"onclick="sectionchange(event, 'user3')">  <i class="fa fa-building"></i><span>Property Details</span></a></li>
                <li><a  id="7" href="#Property Details"class="tablinks"onclick="sectionchange(event, 'user4')">  <i class="fa fa-building"></i><span>Property Details</span></a></li>
               @endif
               @if(Cookie::get('user_type')=='Owner'||Session::get('user_type')=='Owner'||Cookie::get('user_type')=='Broker'||Session::get('user_type')=='Broker'||Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin')
                <li><a id="8" href="#myproperty" class="tablinks" onclick="sectionchange(event, 'myproperty')"><i class="fa fa-home"></i><span>My Properties</span> </a></li>
                @endif
                @if(Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin')
                <li>
                    <a id="" href="#payment Data"class="tablinks" onclick="sectionchange(event,'paymentdataa')">
                        <i class="fa fa-inr"></i>
                        <span>Payment data</span>
                    </a>
                </li>
                @endif
                @if(Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin')
                <li><a id="13" href="#" class="tablinks" onclick="sectionchange(event, 'tips')"><i class="fa fa-lightbulb"></i><span>Tips</span> </a></li>
                @endif
                @if(Cookie::get('user_type')=='User'||Session::get('user_type')=='User')
                <li>
                    <a id="9" href="#Saved_homes"class="tablinks"onclick="sectionchange(event, 'save_home')">
                        <i class="fa fa-bookmark"></i>
                        <span>Saved Homes</span>
                    </a>
                </li>
              
                <li>
                    <a id="10" href="#my_cart"class="tablinks"onclick="sectionchange(event, 'cart')">
                        <i class="fas fa-shopping-cart position-relative"></i><span class="badge badge-danger position-absolute" style="left:35px;top:10px;" id="cartcount"></span>
                        <span>My Cart</span>
                    </a>
                </li>
                @endif

                @if(Cookie::get('user_type')=='Owner'||Session::get('user_type')=='Owner'||Cookie::get('user_type')=='Broker'||Session::get('user_type')=='Broker')
                <li>
                    <a id="11" href="fontawesome.html">
                        <i class="fa fa-bell"></i>
                        <span>Requests</span>
                    </a>
                </li>
               @endif
              
             
                @if(Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin'||Cookie::get('user_type')=='User'||Session::get('user_type')=='User'||Cookie::get('user_type')=='Broker'||Session::get('user_type')=='Broker'||Cookie::get('user_type')=='Owner'||Session::get('user_type')=='Owner')
                <li>
                    <a id="12" href="#Aboutme"class="tablinks" onclick="sectionchange(event,'aboutme')">
                        <i class="fa fa-bullhorn"></i>
                        <span>About me</span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{url('logout')}}">
                        <i class="fa fa-sign-out"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>            </div>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
<!--main content start-->
<section id="main-content" >
<section class="wrapper">
<hr class="bg-light"> 
        <div class="row mx-2">
      <div class="col-md-3 px-3 py-3 rounded ">
         <div class=" row market-update-block clr-block-2">
                    <div class="col-md-4 market-update-right">
                        <i class="fa fa-eye"> </i>
                    </div>
                    <div class="col-md-8 market-update-left">
                            <h4>Total Users</h4>
                                    <h3 style="margin-left:30px;"id="dashuser">5</h3>
                            <p style="margin-left:-26px;">Total Registered Persons</p>
                    </div>
                    <div class="clearfix"> </div>
          </div>
      </div>

    
      <div class="col-md-3 px-3 py-3 ">
         <div class="row market-update-block clr-block-1">
					<div class="col-md-4 market-update-right">
						<i class="fa fa-users" ></i>
					</div>
					<div class="col-md-8  market-update-left">
					    <h4>Properties listed</h4>
					        	<h3 style="margin-left:30px;"id="dashproperty">6</h3>
						<p style="margin-left:-26px;">Total Listed Properties</p>
					</div>
			  <div class="clearfix"> </div>
		  </div>
      </div>

      <div class="col-md-3 px-3 py-3">
          <div class=" row market-update-block clr-block-3">
			  <div class="col-md-4 market-update-right">
				  <i class="fa fa-usd"></i>
			 </div>
					<div class="col-md-8 market-update-left">
						<h4>Sold Properties</h4>
				        		<h3 style="margin-left:30px;"id="dashsold">2</h3>
						<p style="margin-left:-26px;">Total sold properties</p>
					</div>
			  <div class="clearfix"> </div>
		  </div>
      </div>
    
      <div class="col-md-3 px-3 py-3 rounded ">
         <div class=" row market-update-block clr-block-2">
                    <div class="col-md-4 market-update-right">
                        <i class="fa fa-eye"> </i>
                    </div>
                    <div class="col-md-8 market-update-left">
                            <h4>Owners</h4>
                                    <h3 style="margin-left:30px;"id="dashowner">2</h3>
                            <p style="margin-left:-26px;">Total owners registered</p>
                    </div>
                    <div class="clearfix"> </div>
          </div>
      </div>
</div> 
</section>

<section id="section">
<div class="container-fluid tabcontent" id="profile">
        <div class="row mx-1">
          <div class="col-md-12 bg-white  mb-5 px-5 py-4">
          <div class="col-12">
        <div class="section-title my-2 d-flex align-items-center">
          <h2>Edit profile </h2>
          <span class="ml-auto" id="joindate"></span>
        </div>
        @csrf
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>First name</label>
              <input type="text" class="form-control editprofile"  id="first" value="" readonly>
            </div>
            <div class="form-group col-md-6">
              <labele>Last name</label>
              <input type="text" class="form-control editprofile"  id="last" value="" readonly>
            </div>
            <div class="form-group col-md-6">
              <label>Email address</label>
              <input type="email" class="form-control" id="email_address" value="{{Session::get('email')}}{{Cookie::get('email')}}" readonly>
            </div>
            <div class="form-group col-md-6">
              <label>Phone number</label>
              <input type="text" class="form-control editprofile" id="phone_number" value=""readonly>
            </div>
            <div class="form-group col-md-12 select-border">
              <label>User type</label>
              <select class="form-control editprofile"  name="usertype" id="usertype" value=""readonly>
              @if(Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin')
              <option value="Admin">Admin</option>
              @endif
              <option value="User">User</option>
              <option value="Owner">Owner</option>
              <option value="Broker">Broker</option>
              </select>
            </div>
        
            <div class="form-group col-md-6">
              <label>Locality</label>
              <input type="text" class="form-control editprofile" id="loc" value=""readonly>
            </div>
            <div class="form-group col-md-6">
              <label>City</label>
              <input type="text" class="form-control editprofile" id="cityy" value=""readonly>
            </div>
            <div class="form-group col-md-12">
              <label>Address</label>
              <textarea class="form-control editprofile" rows="4"  id="add" value=""readonly></textarea>
            </div>
            <div class="form-group col-md-12">
              <label>About Me</label>
              <textarea class="form-control editprofile" rows="4"  id="about" value="" readonly></textarea>
            </div>
            <div class="col-md-12">
              <a class="btn btn-success" href="#" id="save" onclick="updateprofiledata()">Save Changes</a><a class="btn btn-danger px-4 mx-2" href="#" id="Edit" onclick=" editprofile()">Editprofile</a>
            </div>
          </div>
        <hr class="my-5">
        <div class="mb-2">
          <h6>Delete Account?</h6>
          <p class="my-3">This will remove your login information from our system, and you will not be able to login again. It cannot be undone.</p>
          <a class="btn btn-danger" href="#">Yes Delete My Account</a>
        </div>
      </div>
          </div>
        </div>
      </div>
@if(Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin')
		<div class="container-fluid tabcontent" id="user">
		     	<div class="row">
      			<div class="col-md-12 col-12">
     <div class="modal" id="myModal">
    <div class="modal-dialog  modal-md modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
           <!-- Registration form -->
           @csrf
            <div class="row my-5">
            <div class="form-group col-sm-6">
            <input type="hidden" class="form-control"  name="id" id="id"  value="" required>
                <label>Firstname:</label>
                <input type="text" class="form-control"  name="firstname" id="firstname"  value="" required>
              </div>
              <div class="form-group col-sm-6">
                <label>Lastname:</label>
                <input type="text" class="form-control" name="lastname" id="lastname" value="" required>
              </div>
            </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Email Address:</label>
                <input type="email" class="form-control"  name="email" id="email" value=""required>
              </div>
              <div class="form-group col-sm-6">
                <label>Phone No:</label>
                <input type="text" class="form-control"  name="phone" id="phone" value="" required>
              </div>
              </div>
             <div class="row">
             <div class="form-group col-sm-6">
                <label>Identity Type:</label>
                <input type="text" class="form-control"  name="identity_type" id="identity_type" value="">
              </div>
              <div class="form-group col-sm-6">
                <label>Identity No:</label>
                <input type="text" class="form-control" name="identity_number" id="identity_number" value="">
              </div>
             </div>
              <div class="row">
              <div class="form-group col-sm-12">
              <label for="cars">Signup As:</label>
              <select class="form-control"  name="user_type" id="user_type" value="">
              <option value="Admin">Admin</option>
              <option value="User">User</option>
              <option value="Owner">Owner</option>
              <option value="Broker">Broker</option>
              </select>
              </div>
              </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Locality</label>
                <input type="text" class="form-control" name="locality" id="locality" value="">
              </div>
      
              <div class="form-group col-sm-6">
                <label>City</label>
                <input type="text" class="form-control" name="city"  id="city" value="">
              </div></div>
             <div class="row">
             <div class="form-group col-sm-12">
                <label>Address:</label>
              <textarea rows="4" cols="50" class="form-control" name="address" id="address" value="" >
            </textarea>
              </div>
             </div> 
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        <div class="col-sm-6 mx-auto">
                <button type="submit" onclick="update()" id="save" class="btn btn-success">Save</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> </div> 
        </div>
        
      </div>
    </div>
  </div>
  <hr class="bg-light"> 
 
     <div class="col-md-12 my-4"><h2 class="text-center ">
     @if(Session::has('email'))
    Welcome  {{Session::get('email')}}({{Session::get('user_type')}}) 
    @elseif(Cookie::has('email'))
    Welcome  {{Cookie::get('email')}} ({{Cookie::get('user_type')}})
    @else
    <h2 class="text-center">Please login first</h2>
    @endif
    </h2></div> 
                 <hr class="bg-light">  
                 <div class="text-center" id="message"></div>
				         	<table class="table table-responsive text-center table-dark" id="userdatatable">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Identity Type</th>
                                    <th>Identity Number</th>
                                    <th>User Type</th>
                                    <th>Locality</th>
                                    <th>Address</th>
                                    <th>Pic</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
				            <tbody id="data">
				            </tbody>
				     </table>
                     <hr class="bg-light"> 
					</div>
		     </div>
		  </div>	
		</div>	
	
     <div class="container-fluid tabcontent" id="user1">
          <div class="row">
              <div class="col-md-12">
  <div class="modal" id="SubmitdataModal">
    <div class="modal-dialog  modal-md modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
           <!-- Registration form -->
           @csrf
            <div class="row my-5">
            <div class="form-group col-sm-6">
            <input type="hidden" class="form-control"  name="submitid" id="submitid"  value="" required>
                <label>Property Title</label>
                <input type="text" class="form-control"  name="Property_Title" id="Property_Title"  value="" required>
              </div>
              <div class="form-group col-sm-6">
                <label>Offer</label>
                <input type="text" class="form-control" name="Offer" id="Offer" value="" required>
              </div>
            </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Rental Period</label>
                <input type="email" class="form-control"  name="Rental_Period" id="Rental_Period" value=""required>
              </div>
              <div class="form-group col-sm-6">
                <label>Property Type</label>
                <input type="text" class="form-control"  name="Property_Type" id="Property_Type" value="" required>
              </div>
              </div>
             <div class="row">
             <div class="form-group col-sm-6">
                <label>Property Price</label>
                <input type="text" class="form-control"  name="Property_price" id="Property_price" value="">
              </div>
              <div class="form-group col-sm-6">
                <label>Area</label>
                <input type="text" class="form-control" name="Area" id="Area" value="">
              </div>
             </div>
              <div class="row">
              <div class="form-group col-sm-12">
              <label for="cars">Rooms</label>
              <input type="text" class="form-control"  name="Rooms" id="Rooms" value="">
              </div>
              </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Video</label>
                <input type="text" class="form-control" name="Video" id="Video" value="">
              </div>
              </div>
             </div>
        <!-- Modal footer -->
        <div class="modal-footer">
        <div class="col-sm-6 mx-auto">
                <button type="submit" onclick="submitformdetails()" id="submit" class="btn btn-success">Save</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> </div> 
        </div>
        
      </div>
    </div>
  </div>
  <hr class="bg-light"> 
  
     <div class="col-md-12 my-4"><h2 class="text-center ">
     @if(Session::has('email'))
    Welcome  {{Session::get('email')}}  ({{Session::get('user_type')}}) 
    @elseif(Cookie::has('email'))
    Welcome  {{Cookie::get('email')}} ({{Cookie::get('user_type')}})
    @else
    <h2 class="text-center">Need to login first</h2>
    @endif
    </h2></div> 
                 <hr class="bg-light">  
                 <div class="text-center" id="message"></div>
					<table class="table table-responsive text-center table-dark" id="propertydetailsdatatable">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                     <th>Email</th>
                                    <th>Property Title</th>
                                    <th>Offer</th>
                                    <th>Rental Period</th>
                                    <th>Property Type</th>
                                    <th>Property Price</th>
                                    <th>Area</th>
                                    <th>Rooms</th>
                                    <th>Video</th>
                                    <th>Images</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
				            <tbody id="propertydetails">
				            </tbody>
				           </table>
                     <hr class="bg-light"> 
					        </div>
              </div>
          </div>
      </div>
      <div class="container-fluid tabcontent" id="user2">
          <div class="row">
              <div class="col-md-12">
  <div class="modal" id="SubmitdataModal2">
    <div class="modal-dialog  modal-md modal-dialog-centered">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
           <!-- Registration form -->
           @csrf
            <div class="row my-5">
            <div class="form-group col-sm-6">
            <input type="hidden" class="form-control"  name="email" id="email"  value="" required>
                <label>Google Maps Address</label>
                <input type="text" class="form-control"  name="Google_Maps_Address" id="Google_Maps_Address"  value="" required>
              </div>
              <div class="form-group col-sm-6">
                <label>Friendly Address</label>
                <input type="text" class="form-control" name="Friendly_Address" id="Friendly_Address" value="" required>
              </div>
            </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Longitude</label>
                <input type="text" class="form-control"  name="Longitude" id="Longitude" value=""required>
              </div>
              <div class="form-group col-sm-6">
                <label>Latitude</label>
                <input type="text" class="form-control"  name="Latitude" id="Latitude" value="" required>
              </div>
              </div>
             <div class="row">
             <div class="form-group col-sm-6">
                <label>Regions</label>
                <input type="text" class="form-control"  name="Regions" id="Regions" value="">
              </div>
              <div class="form-group col-sm-6">
                <label>Building age</label>
                <input type="text" class="form-control" name="Building_age" id="Building_age" value="">
              </div>
             </div>
             </div>
        <!-- Modal footer -->
        <div class="modal-footer">
        <div class="col-sm-6 mx-auto">
                <button type="submit" onclick="submitformdetails2()" id="submit" class="btn btn-success">Save</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> </div> 
        </div>
      </div>
    </div>
  </div>
      <hr class="bg-light">
                 <div class="text-center" id="message2"></div>
					<table class="table table-responsive text-center table-dark" id="propertydetails2datatable">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Email</th>
                                    <th>Google Maps Address</th>
                                    <th>Friendly Address</th>
                                    <th>Longitude</th>
                                    <th>Latitude</th>
                                    <th>Regions</th>
                                    <th>Building age</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
				            <tbody id="propertydetails2">
				            </tbody>
				           </table>
                     <hr class="bg-light"> 
					        </div>
              </div>
          </div>
      </div>
      <div class="container-fluid tabcontent" id="user3">
          <div class="row">
              <div class="col-md-12">
  <div class="modal" id="SubmitdataModal3">
    <div class="modal-dialog  modal-md modal-dialog-centered">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
           <!-- Registration form -->
           @csrf
            <div class="row my-5">
            <div class="form-group col-sm-6">
            <input type="hidden" class="form-control"  name="email" id="email"  value="" required>
                <label>Bedrooms</label>
                <input type="text" class="form-control"  name="Bedrooms" id="Bedrooms"  value="" required>
              </div>
              <div class="form-group col-sm-6">
                <label>Bathrooms</label>
                <input type="text" class="form-control" name="Bathrooms" id="Bathrooms" value="" required>
              </div>
            </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Parking</label>
                <input type="text" class="form-control"  name="Parking" id="Parking" value=""required>
              </div>
              <div class="form-group col-sm-6">
                <label>Ac</label>
                <input type="text" class="form-control"  name="AC" id="AC" value="" required>
              </div>
              </div>
             <div class="row">
             <div class="form-group col-sm-6">
                <label>Heater</label>
                <input type="text" class="form-control"  name="Heater" id="Heater" value="">
              </div>
              <div class="form-group col-sm-6">
                <label>Sewer</label>
                <input type="text" class="form-control" name="Sewer" id="Sewer" value="">
              </div>
              <div class="form-group col-sm-6">
                <label>Water</label>
                <input type="text" class="form-control" name="Water" id="Water" value="">
              </div>
              <div class="form-group col-sm-6">
                <label>Kitchen</label>
                <input type="text" class="form-control" name="Kitchen" id="Kitchen" value="">
              </div>
              <div class="form-group col-sm-6">
                <label>Balcony</label>
                <input type="text" class="form-control" name="Balcony" id="Balcony" value="">
              </div>
             </div>
             </div>
        <!-- Modal footer -->
        <div class="modal-footer">
        <div class="col-sm-6 mx-auto">
                <button type="submit" onclick="submitformdetails3()" id="submit" class="btn btn-success">Save</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> </div> 
        </div>
      </div>
    </div>
  </div>
      <hr class="bg-light">
                 <div class="text-center" id="message3"></div>
					<table class="table table-responsive text-center table-dark" id="propertydetails3datatable">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Email</th>
                                    <th>Bedrooms</th>
                                    <th>Bathrooms</th>
                                    <th>Parking</th>
                                    <th>AC</th>
                                    <th>Heater</th>
                                    <th>Sewer</th>
                                    <th>Water</th>
                                    <th>Kitchen</th>
                                    <th>Balcony</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
				            <tbody id="propertydetails3">
				            </tbody>
				           </table>
                     <hr class="bg-light"> 
					        </div>
              </div>
          </div>
      </div>
      <div class="container-fluid tabcontent" id="user4">
          <div class="row">
              <div class="col-md-12 mx-auto">
  <div class="modal" id="SubmitdataModal4">
    <div class="modal-dialog  modal-md modal-dialog-centered">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
           <!-- Registration form -->
           @csrf
            <div class="row my-5">
            <div class="form-group col-sm-6">
            <input type="hidden" class="form-control"  name="email" id="email"  value="" required>
                <label>Floorplan Title</label>
                <input type="text" class="form-control"  name="Floorplan_Title" id="Floorplan_Title"  value="" required>
              </div>
              <div class="form-group col-sm-6">
                <label>Area</label>
                <input type="text" class="form-control" name="Areatype" id="Areatype" value="" required>
              </div>
            </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Description</label>
                <input type="text" class="form-control"  name="Description" id="Description" value=""required>
              </div>
             </div>
             </div>
        <!-- Modal footer -->
        <div class="modal-footer">
        <div class="col-sm-6 mx-auto">
                <button type="submit" onclick="submitformdetails4()" id="submit" class="btn btn-success">Save</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> </div> 
        </div>
      </div>
    </div>
  </div>
      <hr class="bg-light">
                 <div class="text-center" id="message4"></div>
					<table class="table table-responsive text-center table-dark" id="propertydetails4datatable">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Email</th>
                                    <th>Floorplan Title</th>
                                    <th>Area</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
				            <tbody id="propertydetails4">
				            </tbody>
				           </table>
                     <hr class="bg-light"> 
					        </div>
              </div>
          </div>
      </div>
      @endif
      <hr class="bg-light">
      <section id="section">
<div class="container-fluid tabcontent" id="myproperty">
        <div class="row mx-1" id="mypropertydesign">
          <div class="col-md-12 bg-white text-dark border-danger mb-3 px-5 py-5 ">
          <h1 id="nodata" class="testimonial " style="color:white;text-align:center;border:2px solid red;padding-top:13%"></h1>
          </div>
        </div>
        </div>
</section>
<hr>
<section id="section">
<div class="container-fluid tabcontent" id="save_home">
        <div class="row mx-1" id="save_homes">
          <div class="col-md-12 bg-white text-dark border-danger mb-3 px-5 py-5 ">
          <h1 id="nosavedhome" class="testimonial " style="color:white;text-align:center;border:2px solid red;padding-top:13%"></h1>
          </div>
        </div>
        </div>
</section>
<hr>
<section id="section">
<div class="container-fluid tabcontent" id="cart">
        <div class="row mx-1 testmonial" id="cart_table">
          <div class="col-md-12 bg-white text-dark border-danger mb-3 px-5 py-5 ">
          <table class="table table-dark table-striped bg-dark text-center" >
          <thead>
          <tr><th>Sr.No.</th><th>Property</th><th>Description</th><th>Price</th><th>Action</th></tr>
          </thead>
          <tbody id="cartdata">
          </tbody >
          <tfoot>
          <tr><th colspan="3"></th><th class="pt-4" id="carttotal">Subtotal:0</th>
          <th>
          <form action="make_payment" method="post">
          @csrf
          <input type="hidden" id="p_id" value="" name="p_id">
          <input type="hidden" id="gettotal" value="" name="gettotal">
          <input type="submit" id="checkout" class="checkout" value="Checkout">
        </form>
        </th>
        </tr>
          </tfoot>
          </table>
        
          </div>
        </div>
        </div>
</section>
<hr>
<section id="section">
<div class="container-fluid tabcontent" id="tips">
        <div class="row mx-1">
          <div class="col-md-12 bg-dark text-white border-danger mb-3 px-5 py-5 ">
         @include('tips')
          </div>
        </div>
        </div>
</section>
<div class="container-fluid tabcontent" id="paymentdataa">
					<table class="table text-center  table-striped table-dark" id="paymentdatatable">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Order Id</th>
                                    <th>User email Id</th>
                                    <th>Transaction Id</th>
                                    <th>Properties Id</th>
                                    <th width="100px">Status</th>
                                </tr>
                            </thead>
				            <tbody id="paymentdata">
				            </tbody>
				           </table>
                     <hr class="bg-light"> 
					        </div>
                  </div>
<section id="section">
<div class="container-fluid tabcontent" id="aboutme">
        <div class="row mx-1">
          <div class="col-md-12 bg-white text-dark border-danger mb-3 px-5 py-5" style="border:1px solid red;">
    <section class="testimonial text-center">
        <div class="container">
            <div class="heading white-heading">
                About me
            </div>            
         <div class="testimonial4_slide">
 <img class="img-circle img-responsive" id="about_img" />
<p class="text-white my-4" id="aboutmetext">&nbsp;</p>
  <h4 class="text-white" id="aboutclientname"></h4>
          </div>
      </div>
    </section>
          </div>
        </div>
        </div>
</section>
<hr class="bg-light">

 <!-- footer -->
		  <div class="footer">
			<div class="wthree-copyright">
			  <p>© 2021 Visitors. All rights reserved | Design by <a href="http://Gharrdhundo.com">Ghardhundo</a></p>
			</div>
		  </div>
</section>
  <!-- / footer -->
<script>
        $(document).ready( function () {
            // $('#userdatatable').DataTable();
        } );
    </script>
    <script>
function sectionchange(evt, section) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent")
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(section).style.display = "block";
  evt.currentTarget.className += " active";
}
document.getElementById("2").click();
</script>
   <script>
   function editprofile(){
    $('input.editprofile,select.editprofile,textarea.editprofile').removeAttr('readonly');
   }
    </script>
 
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script src="{{asset('dashboard/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{asset('dashboard/js/scripts.js')}}"></script>
<script src="{{asset('dashboard/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('dashboard/js/jquery.nicescroll.js')}}"></script>
</body>
</html>
