
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Ghardhundo - Real Estate HTML5 Template" />
    <meta name="author" content="potenzaglobalsolutions.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>

    <!-- Favicon -->
    <!-- <link rel="shortcut icon" href="images/favicon.ico" />


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">


    <link rel="stylesheet" href="css/font-awesome/all.min.css" />
    <link rel="stylesheet" href="css/flaticon/flaticon.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />

    <link rel="stylesheet" href="css/style.css" /> -->

  </head>
<body>
<section class="py-5 bg-primary">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-9">
        <h2 class="text-white mb-0">Looking to sell or rent your property?</h2>
      </div>
      <div class="col-lg-3 text-lg-right mt-3 mt-lg-0">
        <a class="btn btn-white" href="#">Get a free quote</a>
      </div>
    </div>
  </div>
</section>
  <!-- <script src="jquery/jquery.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.js"></script> -->
</body>
</html>