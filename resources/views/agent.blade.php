
<div class="container py-5">
            <div class="row justify-content-center py-3">
            <div class="col-lg-8">
                <div class="section-title text-center">
                <h2>Meet our agents</h2>
                <p>Ghardhundo Agents are customer advocates. We are accountable for helping clients buy and sell the right home, at the right price.</p>
                </div>
            </div>
            </div>

            <div class="row no-gutters" id="data1">
            </div>
    
  <script>
      agents();

    agentsDataArray=[];
        function agents(index){
          let url = 'agents';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('').html(me)
          return false;
      }
      tag_name= ''
      let data =  obj.data
      agentsDataArray = data
      for(i=0 ; i<agentsDataArray.length ; i++){
    tag_name+='<div class="col-lg-3 col-sm-6 mb-4 mb-sm-0">'
    tag_name+='    <div class="agent text-center">'
    tag_name+='    <div class="agent-detail">'
    tag_name+='        <div class="agent-avatar avatar avatar-xllll">'
    tag_name+='        <img class="img rounded-circle" src="profile_images/'+agentsDataArray[i].profile_pic+'" height="180px" width="180px" alt="">'
    tag_name+='        </div>'
    tag_name+='        <div class="agent-info">'
    tag_name+='        <h6 class="my-2"> <a href="{{url("details")}}">'+ agentsDataArray[i].firstname+' '+ agentsDataArray[i].lastname+'</a></h6>'
    tag_name+='        <span class="text-successfont-sm my-3">'+agentsDataArray[i].user_type+'</span>'
    tag_name+='        <p class="my-4"></p>'
    tag_name+='        </div>'
    tag_name+='    </div>'
    tag_name+='    <div class="agent-button">'
    tag_name+='        <a class="btn btn-light btn-block" href="viewprofile/'+agentsDataArray[i].id+'">View Profile</a>'
    tag_name+='    </div>'
    tag_name+='    </div>'
    tag_name+='</div>'                                                                                                                                                  
          }
            $('#data1').html(tag_name);
        }
    }
    </script>
</div>  