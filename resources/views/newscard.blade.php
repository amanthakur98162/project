<style>
input[type='checkbox']{
height:10px;
width:10px;
background-color: #26ae61;
position: relative;
outline: none;
-webkit-appearance: none;
transform: rotate(45deg);
}
input[type='checkbox']::before {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    top: -68%;
    left: 0px;
    border-radius: 75px 75px 0 0;
}

input[type='checkbox']::after {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    transform: rotate( 
-90deg
 );
    border-radius: 75px 75px 0 0;
    top: 12%;
    right: 75%;
}
input[type='checkbox']:checked{
  background-color: red;
}
input:checked[type='checkbox']::after{
  background-color: red;
}
input:checked[type='checkbox']::before{
  background-color: red;
}
</style>
  <div class="container py-5 ">
    <div class="row justify-content-center">
      <div class="col-lg-8">
        <div class="section-title text-center">
          <h2>News, tips & articles</h2>
          <p>Grow your appraisal and real estate career with tips, trends, insights and learn more about our expert's advice.</p>
        </div>
      </div>
    </div>
    <div class="row py-2" id="data2">
    </div>
  </div>
  <script>
      tricks();
    tricksDataArray=[];
        function tricks(index){
          let url = 'tricks';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('').html(me)
          return false;
      }
      tag_name= ''
      let data =  obj.data
      tricksDataArray = data
      for(i=0 ; i<tricksDataArray.length ; i++){
   tag_name += '<div class="col-lg-4 mt-4 mt-lg-0">'
   tag_name += '  <div class="blog-post">'
   tag_name += '    <div class="blog-post-image">'
   tag_name += '      <img class="img-fluid" src="{{asset("images/blog/02.jpg")}}" alt="">'
   tag_name += '    </div>'
   tag_name += '    <div class="blog-post-content">'
   tag_name += '      <div class="blog-post-details">'
   tag_name += '        <div class="blog-post-category">'
   tag_name += '          <a class="mb-3" href="#"><strong>Tips and advice</strong></a>'
   tag_name += '        </div>'
   tag_name += '        <div class="blog-post-title">'
   tag_name += '          <h5><a href="#">'+ tricksDataArray[i].title+'</a></h5>'
   tag_name += '        </div>'
   tag_name += '        <div class="blog-post-description">'
   tag_name += '          <p class="mb-0">'+tricksDataArray[i].msg+'</p>'
   tag_name += '        </div>'
   tag_name += '      </div>'
   tag_name += '      <div class="blog-post-footer">'
   tag_name += '        <div class="blog-post-time">'
   tag_name += '          <a href="#" class="timeago"><i class="far fa-clock"></i>'+timeSince(tricksDataArray[i].created_at)+'</a>'
   tag_name += '        </div>'
   tag_name += '        <div class="blog-post-author">'
   tag_name += '          <span> By <a href="#"> <img class="img" src="profile_images/'+tricksDataArray[i]['userdata'].profile_pic+'" height="40px" width="30px" alt="">'+ tricksDataArray[i]['userdata'].firstname+' '+tricksDataArray[i]['userdata'].lastname+ '</a> </span>'
   tag_name += '        </div>'
   tag_name += '        <div class="blog-post-comment">'
   tag_name += '          <a href="#"> </a>'
   tag_name += '        </div>'
   tag_name += '      </div>'
   tag_name += '    </div>'
   tag_name += '  </div>'
   tag_name += '</div> '
}
            $('#data2').html(tag_name);

        }
      }
    </script>
    
