<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email</title>
<link rel="shortcut icon" href="images/favicon.ico" />

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
<!-- CSS Global Compulsory (Do not remove)-->
<!-- <link rel="stylesheet" href="css/font-awesome/all.min.css" />
<link rel="stylesheet" href="css/flaticon/flaticon.css" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/style.css" /> -->
</head>
<body>
<section class="py-5 bg-primary">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h3 class="text-white mb-0">Sign up to our newsletter to get the latest news and offers.</h3>
      </div>
      <div class="col-md-7 mt-3 mt-md-0">
        <div class="newsletter">
          <form>
            <div class="form-group mb-0">
              <input type="email" class="form-control" placeholder="Enter email">
            </div>
            <button type="submit" class="btn btn-dark b-radius-left-none">Get notified</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>            
</body>
</html>