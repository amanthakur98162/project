<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submit extends Model
{
    use HasFactory;
    protected $fillable = ['email','Property_Title','Offer','Rental_Period','Property_Type','Property_price','Area','Rooms','Video','Images','Google_Maps_Address','Friendly_Address','Longitude','Latitude','Regions','Building_age','Bedrooms','Bathrooms','Parking','AC','Heater','Sewer','Water','Kitchen','Balcony','File','Floorplan_Title','Areatype','Description'];
}
