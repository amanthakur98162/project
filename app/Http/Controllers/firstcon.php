<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;                //User_validator_for_validation
use Illuminate\Support\Facades\Hash;                    //Use_Hash_for_password_encryption 
use Illuminate\Support\Facades\Mail;                   //Use_mail
use Illuminate\Http\Request;                          //Use_Request
use App\Models\VerifyEmail;                          //Verify_email_model
use App\Models\register;   
use App\Models\PasswordReset;                      //Resgister_model
use App\Models\save;
use App\Models\Submit;  
use App\Models\cart;                          //submit          
use App\Models\payment;
use App\Models\trick;
use Illuminate\Support\Facades\Cookie;           //Use_cookie
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;        //Use_session
use Illuminate\Support\Facades\DB;      
use PharIo\Manifest\Email;
use PHPUnit\Framework\Constraint\Count;
use Illuminate\Pagination\Paginator;

class firstcon extends Controller
{

//----Insert data-----
public function Register(Request $request)
    {
     //----validator------    
      //   print_r($request->all());
        $validator = Validator::make($request->all(),[
            'email'=>'required|unique:registers|email:rfc',
            'identity_number'=>'required|unique:registers',
            'password' => ['required','string','min:6','regex:/[a-z]/',
            'regex:/[a-z]/',
            'regex:/[0-9]/',
            'regex:/[@$!%*#?&]/'],
            'confirm_password'=>'required_with:password|same:password|min:6',
        ]);
         if($validator->fails())
        {
            return $this->error_msg($validator->errors()->first());
        }
        $pass = $request->password;
        $confirm_pass=$request->confirm_password;
        //print_r($pass);
        $data = $request->all();
        unset($data['_token']);
        unset($data['password']);
        $data['password'] = Hash::make($pass);
        $data['confirm_password'] = Hash::make($confirm_pass);

      //----For profile image folder------

        if($request->hasFile('profile_pic')){
            $image = $request->profile_pic;
            $extension = $image->getClientOriginalExtension();
            $newname = rand().time().'.'.$extension;
            $destination = public_path('profile_images');
            $image->move($destination,$newname); 
            unset($data['profile_pic']);
            $data['profile_pic']=$newname; 
}
       // print_r($newname);
        $insertQuery = register::create($data);

       //  -----   Verification Token Genration   -------
          $ve = new VerifyEmail();
          $token = rand(5,10).time();
          $ve->email = $request->email;
          $ve->token = $token;
          $ve->save();
          // die();
          // Mail
            $mail_array = array('email'=>$request->email,'token'=>$token);
              Mail::send('emails.register_mail',$mail_array,function($m) use ($mail_array){
              $m->to($mail_array['email'])->subject('Registration done');
              $m->from('abc@gmail.com');
          });
        return $this->success_msg('Data inserted successfully');      
          }

           //------  Verification  ---------
    public function verify_email($token)
    {
        $getverificationdata = VerifyEmail::where('token',$token)->first();
        if($getverificationdata){
            $updateRegisterStatus = register::where('email',$getverificationdata->email)->update(['status'=>1]);
            if($updateRegisterStatus){
                VerifyEmail::where('token',$token)->delete();
                return $this->success_msg('success','Verification done successfully');
                return redirect('login');
            }
            return $this->error_msg('error','Verification not done successfully');
            return redirect('login');
        }
        echo 'Verification done successfully';
    }

 //------ Show Data --------
    public function newGetData()
    {
        $data=register::all();
        // print_r($data); 
        return $this->success_msg('All record',$data);
    }




    //----- Delete_Button -----
public function delete($id)
    {
       $data = register::where('id',$id)->first();
         $data->delete();
         return $this->success_msg('Data delete sucessfully'); 
    }


//------ Update_Button ------
    public function update(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $uu=Register::where('id',$request->id)->update($data);
        return $this->success_msg('Data updated successfully'); 
    } 


//------ Login ---------
    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'email'=>'required|email:rfc',
            'password' => ['required','string','min:6','regex:/[a-z]/',
            'regex:/[a-z]/',
            'regex:/[0-9]/',
            'regex:/[@$!%*#?&]/'],
        ]);
         if($validator->fails())
        {
            return $this->error_msg($validator->errors()->first());
        }
        $checkEmail = register::where('email',$request->email)->first();
        if($checkEmail){
            //--------- check verification status ---------
            if($checkEmail->status != 1){
                return $this->error_msg('Email is not verified '); 
            }
              //------- Password  Check ---------
            $checkPassword = Hash::check($request->password,$checkEmail->password);
            if($checkPassword){
                if($request->remember_me == 1){
                    //------ Cookie  -------
                    Cookie::queue(Cookie::make('email',$checkEmail->email,120));
                    Cookie::queue(Cookie::make('id',$checkEmail->id,120));
                    Cookie::queue(Cookie::make('user_type',$checkEmail->user_type,120));
                }else{
                    Session::put('email',$checkEmail->email,120);
                    Session::put('id',$checkEmail->id,120);
                    Session::put('user_type',$checkEmail->user_type,120);
                }
                return $this->success_msg('Login successfully');
            }
            return $this->error_msg('Invalid Password');
        }
        return $this->error_msg('Email is not found in our record'); 
    }


    //------- Dashboard --------
    public function dashboard()
    {
            $getlogindata = register::where('email',Cookie::get('email'))->first();
            $sessiondata=register::where('email',Session::get('email'))->first();
            // print_r($getlogindata);
             // die();
        if($getlogindata){
         return view('dash')->with('data',$getlogindata);    
        }
        elseif($sessiondata){
            return view('dash')->with('data',$sessiondata);   
        }
       // session()->flash('error','Need to login first to enter dasbhoard');
        return redirect('login');
        
    }
    //---- Logout ------
    
    
    public function logout()
    {
        //-------- Session destroy  --------
        Session::flush();
        //-------   Cookie   -----------
        Cookie::queue(Cookie::forget('email'));
        Cookie::queue(Cookie::forget('id'));
        Cookie::queue(Cookie::forget('user_type'));
        return redirect('login');
    }
    
    //------  Forget Password   --------
    public function forget(Request $request){
        $validator = Validator::make($request->all(),[
            'email'=>'required',]);
            if($validator->fails())
            {
                return $this->error_msg($validator->errors()->first());
            }
        $checkEmail = register::where('email',$request->email)->first();
        if($checkEmail){
            $ve = new PasswordReset();
            $token = rand(5,10).time();
            $ve->email = $request->email;
            $ve->token = $token;
            $ve->save();
            $mail_array = array('email'=>$request->email,'token'=>$token);
                Mail::send('emails.forgetemail',$mail_array,function($m) use ($mail_array){
                    $m->to($mail_array['email'])->subject('Password Reset');
                    $m->from('PasswordReset@gmail.com');
                });
                return $this->success_msg('Email sent Successfully');
              return false;
        }
        return $this->error_msg('Email is not in record please enter a valid Email');
    }

    public function reset_password($t){
               $checkToken = PasswordReset::where('token',$t)->first();
               if(!$checkToken){
               return '<h1 style="text-align:center;margin-top:20%;color:red;font-family:sans-serif";>Link Will be Expired</h1>';
                   return redirect('login');
               }
               return view('resetpassword')->with('token',$t);
           }

 //------  Forget Password   --------
  public function forget_pass(Request $request){
     // print_r($request->all());
       $validator = Validator::make($request->all(),[
      'password' => ['required','string','min:6','regex:/[a-z]/',
            'regex:/[a-z]/',
            'regex:/[0-9]/',
            'regex:/[@$!%*#?&]/'],
            'confirm_password'=>'required_with:password|same:password|min:6',
    ]);
     if($validator->fails())
    {
        return $this->error_msg($validator->errors()->first());
    }
    $checkToken = PasswordReset::where('token',$request->passwordToken)->first();
       if(!$checkToken){
        return $this->error_msg('Link expire. Please try again with new Link');
       }
       // condition to match password
       $updatePassword = register::where('email',$checkToken->email)->update(['password'=>Hash::make($request->password)]);
       PasswordReset::where('token',$request->passwordToken)->delete();
      return $this->success_msg('success','Password update successfully');
     return false;
    }
  
//Submit_details_form
public function Submit(Request $request){
    $validator = Validator::make($request->all(),[
        'Property_Title'=>'required',
        'Property_price'=>'required',
        'Description'=>'required',
    ]);
    if($validator->fails())
    {
        return $this->error_msg($validator->errors()->first());
    }
      $data=$request->all();

       unset($data['_token']);
 //----For add images to folder------

    if($request->hasFile('Images')){
        $images = $request->Images;
        $extension = $images->getClientOriginalExtension();
        $newname = rand().time().'.'.$extension;
        $destination = public_path('Image');
        $images->move($destination,$newname); 
        unset($data['Images']);
        $data['Images'] = $newname; 
    }
    if($request->hasFile('File')){
        $Files = $request->File;
        $extension = $Files->getClientOriginalExtension();
        $newname = rand().time().'.'.$extension;
        $destination = public_path('Files');
        $Files->move($destination,$newname); 
        unset($data['File']);
        $data['File'] = $newname; 
    }
    $insertQuery = Submit::create($data);
    if($insertQuery){
    return $this->success_msg('Details Submited successfully');   
    return false;
    }
    return $this->error_msg('Details Submission failed');   
}
//----------get_profile_details--------
public function profileData(Request $request){
    if(Session::has('email')){
        $data=register::where('email',Session::get('email'))->first();
        return $this->success_msg('success',$data);
    }
    else{
        $data=register::where('email',Cookie::get('email'))->first();
     return $this->success_msg('success',$data);
    }
    return $this->error_msg('No Data available');
}
//----------update_profile_details--------
public function updateprofile(Request $request){
    $data = $request->all();
    unset($data['_token']);
    $uu=register::where('email',$request->email)->update($data);
    return $this->success_msg('Data updated successfully'); 
}
//-----------getPropertydetails-------

public function getPropertydetails(){
  $data=Submit::all();
   //print_r($data); 
   return $this->success_msg('All record',$data);
}
/***** getPropertydetails in second tabel *****/
public function getPropertydetails2(){
    $data=Submit::all();
     //print_r($data); 
     return $this->success_msg('All record',$data);
  }


public function submitformdetails(Request $request)
{
    $data = $request->all();
    unset($data['_token']);
    $uu=Submit::where('id',$request->id)->update($data);
    return $this->success_msg('Data updated successfully'); 
} 
public function submitformdetails2(Request $request)
{
    $data = $request->all();
    unset($data['_token']);
    $uu=Submit::where('email',$request->email)->update($data);
    return $this->success_msg('Data updated successfully'); 
} 
public function submitformdetails3(Request $request)
{
    $data = $request->all();
    unset($data['_token']);
    $uu=Submit::where('email',$request->email)->update($data);
    return $this->success_msg('Data updated successfully'); 
} 
public function submitformdetails4(Request $request)
{
    $data = $request->all();
    unset($data['_token']);
    $uu=Submit::where('email',$request->email)->update($data);
    return $this->success_msg('Data updated successfully'); 
} 
public function savechanges(Request $request)
{
    $data = $request->all();
    unset($data['_token']);
    $uu=Register::where('email',$request->email)->update($data);
    return $this->success_msg('Data updated successfully'); 
} 
public function deletesubmitformdata($id)
{
   $data = Submit::where('id',$id)->first();
     $data->delete();
     return $this->success_msg('Data delete sucessfully'); 
}
public function deleteprofile(){

}
public function Myproperties(){
    $myproperty=array();
    if(Session::has('email')){
        $myproperty=Submit::where('email',Session::get('email'))->get();
    }
    else{
        $myproperty=Submit::where('email',Cookie::get('email'))->get();
    }if(sizeof($myproperty)==0){
        return $this->error_msg('No Data Available');
    }
    for($i=0 ; $i<sizeof($myproperty) ; $i++){
        $getdata=register::where('email',$myproperty[$i]->email)->first();
        $myproperty[$i]['user_details'] = $getdata;
    }
   return $this->success_msg('getdata',$myproperty);
    $mypropertysession=Submit::where('email',Session::get('email'))->get();
    $mypropertycookie=Submit::where('email',Cookie::get('email'))->get();
    if($mypropertysession){
        return $this->success_msg('getpropertydata',$mypropertysession);
    } 
    elseif($mypropertycookie){
        return $this->success_msg('getpropertydata',$mypropertycookie);
    }
    else{
        return $this->error_msg('No Data Available');
    }
}
public function propertydata($id)
{
    $dataarray=array();
    $data=Submit::where('id',$id)->first();
  $profiledetails=register::where('email',$data->email)->first();
   unset($profiledetails->password);
   unset($profiledetails->confirm_password);
   unset($profiledetails->status);
   $dataarray['data']=$data;
   $dataarray['userdetails']=$profiledetails;
   $dataarray['savecheck']=0;
   if(Session::has('email')||Cookie::has('email')){
    $saved=save::where('email',Session::get('email'))->orderBy('p_id','desc')->get();
    if(Count($saved)!=0){
        for($j = 0; $j<Count($saved); $j++)
        {
            // print_r($saved[$j]->p_id);
              if($saved[$j]->p_id == $data->id ){
                $dataarray['savecheck']=1;
            }
        }
    }
}
// return $this->success_msg('getdata',$dataarray);
//       echo '<pre>';
//    print_r($data->email);
//    echo '</pre>';
    return view('property')->with('data',$dataarray); 
}
public function view_property($id)
{
    $dataa=Submit::where('id',$id)->first('email');
    $data=Submit::where('email',$dataa->email)->get(); 
     for($i=0;$i<Count($data);$i++){
         $data[$i]->userdetail=array();
         $prodata=register::where('email',$data[$i]->email)->first(); 
         $data[$i]->userdetail=$prodata;
         $data[$i]->savecheck=0;
         if(Session::has('email')||Cookie::has('email')){
             $saved=save::where('email',Session::get('email'))->orderBy('p_id', 'desc')->get();
             if(Count($saved)!=0){
                 for($j = 0; $j<Count($saved); $j++)
                 {
                     // print_r($saved[$j]->p_id);
                       if($saved[$j]->p_id == $data[$i]->id ){
                         $data[$i]->savecheck=1;
                     }
                 } 
             }
       }
     }
//         echo'<pre>';
//    print_r($data); 
    return view('view-listing')->with('data',$data);
}
public function listing_list(){
   $data=Submit::simplePaginate(6);
   for($i=0;$i<Count($data);$i++){
    $data[$i]->userdetail=array();
    $prodata=register::where('email',$data[$i]->email)->first(); 
    $data[$i]->userdetail=$prodata;
    $data[$i]->savecheck=0;
    if(Session::has('email')||Cookie::has('email')){
        $saved=save::where('email',Session::get('email'))->orderBy('p_id', 'desc')->get();
        if(Count($saved)!=0){
            for($j = 0; $j<Count($saved); $j++)
            {
                // print_r($saved[$j]->p_id);
                  if($saved[$j]->p_id == $data[$i]->id ){
                    $data[$i]->savecheck=1;
                }
            } 
        }
 }
   }
//    echo'<pre>';
//    print_r($data[0]['savecheck']); 
 
   return view('listing-list')->with('list',$data);
}
public function listing(){
    $data=Submit::simplePaginate(6);
   for($i=0;$i<Count($data);$i++){
    $data[$i]->userdetail=array();
    $prodata=register::where('email',$data[$i]->email)->first(); 
    $data[$i]->userdetail=$prodata;
    $data[$i]->savecheck=0;
    if(Session::has('email')||Cookie::has('email')){
        $saved=save::where('email',Session::get('email'))->orderBy('p_id', 'desc')->get();
        if(Count($saved)!=0){
            for($j = 0; $j<Count($saved); $j++)
            {
                // print_r($saved[$j]->p_id);
                  if($saved[$j]->p_id == $data[$i]->id ){
                    $data[$i]->savecheck=1;
                }
            } 
        }
 }
   }
    // echo'<pre>';
    // print_r($data); 
    return view('listing')->with('list',$data);
 }
 public function owner_grid(){
     $getowner=register::where('user_type','Owner')->get(); 
     $datapro=Submit::take(4)->orderBy('id', 'desc')->get();
     $data=array(
                 'userdata'=>$getowner,
                 'prodata'=>$datapro,     
    );
    //     echo'<pre>';
    // print_r($data); 
    return view('ownergrid')->with('list',$data);
    }
    public function broker_grid(){
        $getbroker=register::where('user_type','Broker')->get(); 
        $datapro=Submit::take(4)->orderBy('id', 'desc')->get();
        $data=array(
                    'userdata'=>$getbroker,
                    'prodata'=>$datapro,     
       );
       //     echo'<pre>';
       // print_r($data); 
       return view('brokergrid')->with('list',$data);
       }
    public function owner_list(){
        $getowner=register::where('user_type','Owner')->get(); 
        $datapro=Submit::take(4)->orderBy('id', 'desc')->get();
        $data=array(
                    'userdata'=>$getowner,
                    'prodata'=>$datapro,     
       );
       //     echo'<pre>';
       // print_r($data); 
       return view('ownerlist')->with('list',$data);
       }
       public function broker_list(){
        $getbroker=register::where('user_type','Broker')->get(); 
        $datapro=Submit::take(4)->orderBy('id', 'desc')->get();
        $data=array(
                    'userdata'=>$getbroker,
                    'prodata'=>$datapro,     
       );
       //     echo'<pre>';
       // print_r($data); 
       return view('brokerlist')->with('list',$data);
       }

       public function newly(){
        // $data = DB::table('Submits')->take(4)
        // ->orderBy('submits.id', 'desc')
        // ->join('registers', 'registers.email', '=', 'submits.email')
        // ->select('submits.id','submits.email','submits.created_at')
        // ->join('saves', 'saves.p_id', '=', 'Submits.id')
        // ->where( 'saves.email', Session::get('email'))
        //  ->get();

        // $data=Submit::take(4)->orderBy('id', 'desc')->get();
        // for($i=0;$i<Count($data);$i++){
        //         $data[$i]->savecheck=0;  
        //     if($data[$i]->email==Session::get('email')){
        //         $data[$i]->savecheck=1;  
        //     }
        // }
        $data=Submit::take(4)->orderBy('id', 'desc')->get();
        for($i=0;$i<Count($data);$i++){
            $data[$i]->userdetail=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdetail=$prodata;
            $data[$i]->savecheck=0;
            if(Session::has('email')||Cookie::has('email')){
                $saved=save::where('email',Session::get('email'))->orderBy('p_id', 'desc')->get();
                if(Count($saved)!=0){
                    for($j = 0; $j<Count($saved); $j++)
                    {
                        // print_r($saved[$j]->p_id);
                          if($saved[$j]->p_id == $data[$i]->id ){
                            $data[$i]->savecheck=1;
                        }
                    } 
                }
         }
        }
        return $this->success_msg('newly',$data);
     }
     public function Saved_home(){
     $data=Save::where('email',Session::get('email'))->get();
      for($i=0;$i<Count($data);$i++){
        $data[$i]->userdetail=array(); 
        $prodata=register::where('email',$data[$i]->email)->first(); 
        $data[$i]->userdetail=$prodata;
        $saved=Submit::where('id',$data[$i]->p_id)->get();  
        $data[$i]->saved=array();
        $data[$i]->saved=$saved;
      }
      if(Count($data)>0){
      return $this->success_msg('saved_home',$data);
      }else{
        return $this->error_msg('No Data Available');  
      }
     }

     public function discover(){
        $data=Submit::take(3)->orderBy('id', 'desc')->get();
        for($i=0;$i<Count($data);$i++){
            $data[$i]->userdetail=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdetail=$prodata;
            $data[$i]->savecheck=0;
            if(Session::has('email')||Cookie::has('email')){
                $saved=save::where('email',Session::get('email'))->orderBy('p_id', 'desc')->get();
                if(Count($saved)!=0){
                    for($j = 0; $j<Count($saved); $j++)
                    {
                        // print_r($saved[$j]->p_id);
                          if($saved[$j]->p_id == $data[$i]->id ){
                            $data[$i]->savecheck=1;
                        }
                    } 
                }
         }
        }
        return $this->success_msg('discover',$data);
     }

     public function agent(){
        $data=register::take(4)->orderBy('id', 'desc')->get(); 
       //     echo'<pre>';
       // print_r($data); 
       return $this->success_msg('agentlist',$data);
       }
       public function testemonial(){
        $data=register::take(3)->orderBy('id', 'desc')->get(); 
       //     echo'<pre>';
       // print_r($data); 
       return $this->success_msg('testmonial',$data);
       }
       public function viewprofile($id){
        $dataa=register::where('id',$id)->first();
        $data=Submit::where('email',$dataa->email)->get();
        if(count($data)==0){
            $data[0]=array(
                'userdetails'=>$dataa,
                'data'=>'',
            );
        }
        else{
         for($i=0;$i<Count($data);$i++){
             $data[$i]->userdetail=array();
             $prodata=register::where('email',$data[$i]->email)->first(); 
             $data[$i]->userdetail=$prodata;
             $data[$i]->savecheck=0;

             if(Session::has('email')||Cookie::has('email')){
                 $saved=save::where('email',Session::get('email'))->orderBy('p_id', 'desc')->get();
                 if(Count($saved)!=0){
                     for($j = 0; $j<Count($saved); $j++)
                     {
                         // print_r($saved[$j]->p_id);
                           if($saved[$j]->p_id == $data[$i]->id ){
                             $data[$i]->savecheck=1;
                         }
                     } 
                 }
          }
        }
         }
        //  echo'<pre>';
        //  print_r($data);
        //  die();
        return view('viewprofile')->with('data',$data);
     }
      
     public function list(){
         $data=Submit::where('Property_type','Apartment')->get();
         for($i=0;$i<Count($data);$i++){
            $data[$i]->userdata=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdata=$prodata;
            $data[$i]->savecheck=0;
            if(Session::has('email')||Cookie::has('email')){
             $saved=save::where('email',Session::get('email'))->orderBy('p_id','desc')->get();
             if(Count($saved)!=0){
                 for($j = 0; $j<Count($saved); $j++)
                 {
                     // print_r($saved[$j]->p_id);
                       if($saved[$j]->p_id == $data[$i]->id ){
                        $data[$i]->savecheck=1;
                     }
                 }
             }
         }
     }
        //    echo'<pre>';
        //    print_r($data);
        //    die();
          return view('list')->with('list',$data);
     }
     public function hostel(){
        $data=Submit::where('Property_type','Hostel')->get();
        for($i=0;$i<Count($data);$i++){
            $data[$i]->userdata=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdata=$prodata;
            $data[$i]->savecheck=0;
            if(Session::has('email')||Cookie::has('email')){
             $saved=save::where('email',Session::get('email'))->orderBy('p_id','desc')->get();
             if(Count($saved)!=0){
                 for($j = 0; $j<Count($saved); $j++)
                 {
                     // print_r($saved[$j]->p_id);
                       if($saved[$j]->p_id == $data[$i]->id ){
                        $data[$i]->savecheck=1;
                     }
                 }
             }
         }
     }
       //    echo'<pre>';
       //    print_r($data);
         return view('hostel')->with('list',$data);
    }
    public function pg(){
        $data=Submit::where('Property_type','Pg')->get();
        for($i=0;$i<Count($data);$i++){
            $data[$i]->userdata=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdata=$prodata;
            $data[$i]->savecheck=0;
            if(Session::has('email')||Cookie::has('email')){
             $saved=save::where('email',Session::get('email'))->orderBy('p_id','desc')->get();
             if(Count($saved)!=0){
                 for($j = 0; $j<Count($saved); $j++)
                 {
                     // print_r($saved[$j]->p_id);
                       if($saved[$j]->p_id == $data[$i]->id ){
                        $data[$i]->savecheck=1;
                     }
                 }
             }
         }
     }
       //    echo'<pre>';
       //    print_r($data);
         return view('pg')->with('list',$data);
    }
    public function villa(){
        $data=Submit::where('Property_type','Villa')->get();
        for($i=0;$i<Count($data);$i++){
            $data[$i]->userdata=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdata=$prodata;
            $data[$i]->savecheck=0;
            if(Session::has('email')||Cookie::has('email')){
             $saved=save::where('email',Session::get('email'))->orderBy('p_id','desc')->get();
             if(Count($saved)!=0){
                 for($j = 0; $j<Count($saved); $j++)
                 {
                     // print_r($saved[$j]->p_id);
                       if($saved[$j]->p_id == $data[$i]->id ){
                        $data[$i]->savecheck=1;
                     }
                 }
             }
         }
     }
       //    echo'<pre>';
       //    print_r($data);
         return view('villa')->with('list',$data);
    }
    public function forrent(){
        $data=Submit::where('Offer','For rent')->get();
        for($i=0;$i<Count($data);$i++){
            $data[$i]->userdata=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdata=$prodata;
            $data[$i]->savecheck=0;
            if(Session::has('email')||Cookie::has('email')){
             $saved=save::where('email',Session::get('email'))->orderBy('p_id','desc')->get();
             if(Count($saved)!=0){
                 for($j = 0; $j<Count($saved); $j++)
                 {
                     // print_r($saved[$j]->p_id);
                       if($saved[$j]->p_id == $data[$i]->id ){
                        $data[$i]->savecheck=1;
                     }
                 }
             }
         }
     }
       //    echo'<pre>';
       //    print_r($data);
         return view('forrent')->with('list',$data);
    }
    public function forsale(){
        $data=Submit::where('Offer','For sale')->get();
        for($i=0;$i<Count($data);$i++){
            $data[$i]->userdata=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdata=$prodata;
            $data[$i]->savecheck=0;
            if(Session::has('email')||Cookie::has('email')){
             $saved=save::where('email',Session::get('email'))->orderBy('p_id','desc')->get();
             if(Count($saved)!=0){
                 for($j = 0; $j<Count($saved); $j++)
                 {
                     // print_r($saved[$j]->p_id);
                       if($saved[$j]->p_id == $data[$i]->id ){
                        $data[$i]->savecheck=1;
                     }
                 }
             }
         }
     }
       //    echo'<pre>';
       //    print_r($data);
         return view('forsale')->with('list',$data);
    }

    public function browse(){
       $sale=Submit::where('Offer','For sale')->get();
       $sale=count($sale);
       $forrent=Submit::where('Offer','For rent')->get();
       $forrent=count($forrent);
       $Apartment=Submit::where('Property_Type','Apartment')->get();
       $Apartment=count($Apartment);
       $hostel=Submit::where('Property_Type','Hostel')->get();
       $hostel=count($hostel);
       $villa=Submit::where('Property_Type','Villa')->get();
       $villa=count($villa);
       $pg=Submit::where('Property_Type','Pg')->get();
       $pg=count($pg);
        $data=array(
             'sale'=>$sale,
             'forrent'=>$forrent,
             'Apartment'=>$Apartment,
             'hostel'=>$hostel,
             'villa'=>$villa,
             'pg'=>$pg,
        );
        return $this->success_msg('data',$data);
    }

    public function tips(Request $request){
        $validator = Validator::make($request->all(),
        [
            'email'=>'required|email:rfc',
            'title'=>'required',
        ]);
         if($validator->fails())
        {
            return $this->error_msg($validator->errors()->first());
        }
     $data=$request->all();
     unset($data['_token']);
     $insertQuery = trick::create($data);
     return $this->success_msg('Data inserted successfully!');
    }
    public function tricks(){
        $data=trick::take(3)->orderBy('id', 'desc')->get(); 
        for($i=0;$i<Count($data);$i++){
            $data[$i]->userdata=array();
            $prodata=register::where('email',$data[$i]->email)->first(); 
            $data[$i]->userdata=$prodata;
           }
           echo'<pre>';
       print_r($data); 
       return $this->success_msg('tricks',$data);
       }
       public function recent(){
           $data=Submit::take(2)->orderBy('id','desc')->get();
           return $this->success_msg('recent',$data);
       }
       public function counter(){
        $users=register::all();
        $users=count($users);
        $property=Submit::all();
        $property=count( $property);
        $user=register::where('user_type','User')->get();
        $user=count($user);
        $Broker=register::where('user_type','Broker')->get();
        $Broker=count($Broker);
         $data=array(
              'users'=>$users,
              'properties'=> $property,
              'user'=>$user,
              'broker'=>$Broker,
         );
         return $this->success_msg('data',$data);
     }
     public function saved(Request $request){
        $data=save::where('email',$request->email)->get();
        for($i=0;$i<Count($data);$i++){
         if($request->p_id==$data[$i]->p_id){
            return $this->error_msg('already saved');
            return false;
         }
        }
        $validator = Validator::make($request->all(),
        [
            'email'=>'required|email:rfc',
        ]);
         if($validator->fails())
        {
            return $this->error_msg('please login first');
        }
        $ve = new save();
        $token = rand(5,10).time();
        $ve->p_id = $request->p_id;
        $ve->check = $request->check;
        $ve->email = $request->email;
        $ve->order_id = $token;
        $ve->save();
         return $this->success_msg('saved successfully');
     }
     public function getsaved(Request $request){
        $data=save::where('email',Session::get('email'))->get();
        for($i=0;$i<Count($data);$i++){
         if($request->p_id==$data[$i]->p_id){
            return $this->error_msg('already saved');
            return false;
         }
        }
         if(Session::get('email')=="")
        {
            return $this->error_msg('please login first');
        }
        $ve = new save();
        $token = rand(5,10).time();
        $ve->p_id = $request->p_id;
        $ve->check = $request->check;
        $ve->email = Session::get('email');
        $ve->order_id = $token;
        $ve->save();
         return $this->success_msg('saved successfully');
     }
     public function cart(Request $request){
         $data=cart::all();
         for($i=0;$i<Count($data);$i++){
         if($request->p_id==$data[$i]->p_id && Session::get('email')||Cookie::get('email')==$data[$i]->email){
            return $this->error_msg('Already exists in cart !!');
         }
        }
        $ve = new cart(); 
        $ve->p_id = $request->p_id;
        $ve->email = Session::get('email');
        $ve->save();
        return $this->success_msg('Add to cart successfully');
     }

     public function getcartdata(){
        $data=cart::where('email',Session::get('email'))->get();
        // echo'<pre>';
        // print_r(Session::get('email'));
        // die();
        for($i=0;$i<Count($data);$i++){
            $data[$i]->prodata=array();
            $prodata=Submit::where('id',$data[$i]['p_id'])->get();
            $data[$i]->prodata=$prodata;
        }
        return $this->success_msg('getcartdata',$data);
    }
    public function removewishlist(Request $request)
    {
        $data = save::where('id',$request->id)->first();
         $cart=cart::where('email',$data->email)->get();
         for($i=0;$i<Count($cart);$i++){
            if($cart[$i]->email==$data->email&&$cart[$i]->p_id==$data->p_id){
             $del = cart::where('id',$cart[$i]->id)->first();  
             $del->delete();
            } 
         }
        $data->delete();
      return $this->success_msg('Data delete sucessfully'); 
    }
    public function removecart(Request $request)
    {
        $data = cart::where('id',$request->id)->first();
         $data->delete();
         return $this->success_msg('Data delete sucessfully'); 
    }
    // public $dat=array();
    public function search(Request $request)
    {
        // print_r($request->all());
//    $data=Submit::all()->min('Property_price');
//    $dataa=Submit::all()->max('Property_price');
        $vehicles=Submit::where(['Property_Type'=>$request->type,'Offer'=>$request->offer])->get(); 
        // echo "<br>";
        // echo "<br>";
        // echo "<br>";
        // echo "<pre>";
        // print_r($vehicles);
        // die();

        if($request->sortby=='Highest price'){
            $vehicles=Submit::where(['Property_Type'=>$request->type,'Offer'=>$request->offer,'Google_Maps_Address'=>$request->location,'Bathrooms'=>$request->bathrooms,'Bedrooms'=>$request->bedrooms,])->orderBy('Property_price', 'asc')->get(); 
        }
        elseif($request->sortby=='Lowest price'){
            $vehicles=Submit::where(['Property_Type'=>$request->type,'Offer'=>$request->offer,'Google_Maps_Address'=>$request->location,'Bathrooms'=>$request->bathrooms,'Bedrooms'=>$request->bedrooms,])->orderBy('Property_price', 'desc')->get(); 
        }
        for($i=0;$i<Count($vehicles);$i++){
            $vehicles[$i]->userdetails=array();
            $prodata=register::where('email', $vehicles[$i]->email)->first(); 
            $vehicles[$i]->userdetails=$prodata;
           }
           $data=$vehicles;
        //    echo "<pre>";
        //     print_r($data);
        //     die();
   //   $this->searchrslt($data);
// $srch=$this->searchrslt($data);
    // echo'<pre>';
    // print_r($dat);
    // die();
    // searchrslt($data);
// return redirect()->route('search',$data);
return view('search')->with('list',$data);
}


public function searchrslt($data)
{
    echo "<pre>";
    print_r($data);
    die();
 return view('search')->with('list',$data);
  }
  public function sold(){
      $data=payment::where('payment_status',1)->get();
      
    return $this->success_msg('solddata',$data);
  }
  public function dashcount(){
      $data=array();
      $users=register::all();
      $usercount=count($users);
      $submit=Submit::all();
      $submitcount=count($submit);
      $paymentdata=payment::where('payment_status',1)->get();
      $paymentdatacount=count($paymentdata);
      $owner=register::where('user_type','Owner')->get();
      $ownercount=count($owner);
      $data['users']=$usercount;
      $data['property']=$submitcount;
      $data['payment']=$paymentdatacount;
      $data['owners']=$ownercount;
      return $this->success_msg('dashcount',$data); 
    }
    //-------   Success_msg  --------

    public function success_msg($message,$data=null){
            $response = array();
            $response['status']=true;
            $response['message']=$message;
            if($data != null){
                $response['data']=$data;
            }
            return $response;
        }     
 //---- Error_msg  -----

        public function error_msg($message){
            $response = array();
            $response['status']=false;
            $response['message']=$message;
            return $response;
        }
    
}