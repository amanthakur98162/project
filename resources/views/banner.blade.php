    <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
          <!-- <script src="{{asset('jquery/jquery.js')}}"></script> -->
         <link rel="stylesheet" href="css/font-awesome/all.min.css">
          <!-- <script src="{{asset('js/bootstrap.bundle.js')}}"></script>  -->
           <link rel="stylesheet" href="{{asset('css/select2/select2.css')}}">
          <script src="{{asset('js/select2/select2.full.js')}}"></script>
          <link rel="stylesheet" href="{{asset('css/style.css')}}">
         <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> -->
          <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->

      </head>
      <body>
<section class="banner bg-holder bg-overlay-black-30" style="background-image:url('{{asset('images/1.jpg')}}')">
  <div class="container mb-5">
    <div class="row">
      <div class="col-12">
        <h1 class="text-white text-center mb-2">Create lasting wealth through Ghardhundo</h1>
        <p class="lead text-center text-white mb-4 font-weight-normal">Take a step to realizing your dream. #TimeToMove</p>
        <div class="property-search-field bg-white" data-select2-id="22">
        <div class="property-search-field bg-white" data-select2-id="22">
          <div class="property-search-item" data-select2-id="21">
           <form action="{{url('searchh')}}" method="POST">
            <div class="form-row basic-select-wrapper" data-select2-id="20">
              <div class="form-group col-lg col-md-6">
              @csrf
                <label>Property type</label>
                <select class="form-control js-select2" id="type"  name="type"js-select2-id="1" tabindex="-1" aria-hidden="true">
                  <option value="">All Type</option>
                  <option value="Apartment">Apartment</option>
                  <option value="Villa">Villa</option>
                  <option value="Hostel">Hostel</option>
                  <option value="Pg">Pg</option>
                </select>
          
              </div>
              <div class="form-group col-md-3">
                  <label>Offer</label>
                  <select class="form-control js-select2" id='offer' name="offer" js-select2-id="1" tabindex="-1" aria-hidden="true">
                  <option value="For rent">For Rent</option>
                  <option value="For sale">For Sale</option>
                </select>
                    </div>
              <div class="form-group d-flex col-lg-4">
                <div class="form-group-search">
                  <label>Location</label>
                  <div class="d-flex align-items-center"><i class="far fa-compass mr-1"></i><input class="form-control" type="search" id="location" name="location" placeholder="Search Location"></div>
                </div>
        <span class="align-items-center ml-3 d-none d-lg-block"><button class="btn btn-primary d-flex align-items-center" type="submit"  id="search-filter" ><i class="fas fa-search mr-1"></i><span>Search</span></button></span>
              </div>
              <div class="form-group text-center col-lg-2">
                <div class="d-flex justify-content-center d-md-inline-block">
                  <a class="more-search p-0" data-toggle="collapse" href="#advanced-search" role="button" aria-expanded="false" aria-controls="advanced-search"> <span class="d-block pr-2 mb-1">Advanced search</span>
                  <i class="fas fa-angle-double-down"></i></a>
                </div>
              </div>
              <div class="collapse advanced-search" id="advanced-search">
                <div class="card card-body">
                  <div class="form-row">
                  
                    <div class="form-group col-md-3">
                      <label>Bedrooms</label>
                      <select class="form-control js-select2"id="bedrooms" name='bedrooms' js-select2-id="1" tabindex="-1" aria-hidden="true">
                      <option>No max</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>

                </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label>Sort by</label>
                      <select class="form-control js-select2" id='sortby' name="sortby" js-select2-id="1" tabindex="-1" aria-hidden="true">
                        <option data-select2-id="15">Most popular</option>
                        <option>Highest price</option>
                        <option>Lowest price</option>
                        <option>Most reduced</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label>Bathrooms</label>
                      <select class="form-control js-select2 " js-select2-id="1" id="bathrooms" name="bathrooms" tabindex="-1" aria-hidden="true">
                        <option >Select bathrooms</option>
                        <option value="1">01</option>
                        <option value="2">02</option>
                        <option value="3">03</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3 mx-auto property-price-slider ">
                      <label>Select Price Range</label>
                      @include('range1')
                    </div>
                  </div>
                </div>
              </div>
              <div class="d-lg-none btn-block btn-mobile m-3">
                <button class="btn btn-primary btn-block align-items-center" type="submit"><i class="fas fa-search mr-1"></i><span>Search</span></button>
              </div>
            </div>
           </form>
          </div>
        </div>
  </div>
</section>
    <script>
            $(document).ready(function() {
                $(".js-select2").select2();
            });
      </script>
      </body>
      </html>