
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Ghardhundo - Real Estate HTML5 Template" />
    <meta name="author" content="potenzaglobalsolutions.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico" />

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">

    <!-- CSS Global Compulsory (Do not remove)-->
     <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
     <link rel="stylesheet" href="{{asset('css/style.css')}}" />
     <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/select2/select2.css')}}">
    <script src="{{asset('js/select2/select2.full.js')}}"></script>
     <link rel="stylesheet" href="{{asset('css/style.css')}}">
     <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<style>

    body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}
 .field-icon {
  margin-left: -25px;
  margin-top:-25px;
  float:right;
  margin-right:10px;
  position: relative;
  z-index: 2;
}
</style>


  </head>
  <body>
  <div class="container py-5">
    <div class="row justify-content-center">
  
      <div class="col-md-8 col-sm-10">
        <div class="section-title">
          <h2 class="text-center">LOG IN</h2>
        </div>
        @if(Session::has('error'))
                <div class="alert alert-success mb-3 text-center">{{Session::get('error')}}</div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-danger mb-3 text-center">{{Session::get('success')}}</div>
            @endif
             <div id="msg2" class="text-center mb-4 py-3"></div>
        <ul class="nav nav-tabs nav-tabs-02 justify-content-center" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="card-tab" data-toggle="tab" href="#card" role="tab" aria-controls="card" aria-selected="false">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="regular-user-tab" data-toggle="tab" href="#regular-user" role="tab" aria-controls="regular-user" aria-selected="true">Sign Up</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="forget" data-toggle="tab" href="#forgetpass" role="tab" aria-controls="forgetpass" aria-selected="true">forget</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="card" role="tabpanel" aria-labelledby="card-tab">
            @csrf
              <div class="form-group col-sm-12">
                <label>Email Address:</label>
                <input type="email"  id="emaily" name="emaily" class="form-control" placeholder="">
              </div>
              <div class="form-group col-sm-12">
                <label>Password:</label>
                <input type="password" id="pas" name="pas"  class="form-control" placeholder="">
                <span toggle="#pas" class="fa fa-fw fa-eye field-icon toggle-password"></span>
              </div>
              <div class="form-group col-sm-12 py-2 mx-4">
              <input type="checkbox" id="remember_me" name="remember_me" class="form-check-input" >
              <label for="remember_me">Remember me</label><br><br>
              </div>
              <div class="col-sm-12">
                <button type="submit" onclick="signin()" class="btn btn-primary btn-block">Sign in &nbsp; <span class="reset"></span></button>
              </div>
              <div class="col-sm-12 mx-auto">
                <ul class="list-unstyled d-flex mb-1 mt-sm-0 mt-3">
                  <li class="mx-auto my-4"><a href="#" onclick="login1()">Already Registered User? Click here to login</a></li>
                </ul>
              </div>
              <div class="login-social-media col-md-12 border pl-4 pr-4 pb-4 pt-0 my-4 rounded d-none">
              <div class="mb-4 d-block text-center"><b class="bg-white pl-2 pr-2 mt-3 d-block">Login or Sign in with</b></div>
              <div class="row d-none">
                <div class="col-sm-12 col-md-6">
                  <a class="btn facebook-bg social-bg-hover d-block mb-3" href="#"><span><i class="fab fa-facebook-f"></i>Login with Facebook</span></a>
                </div>
             
                <div class="col-sm-12 col-md-6">
                  <a class="btn twitter-bg social-bg-hover d-block mb-3" href="#"><span><i class="fab fa-twitter"></i>Login with Twitter</span></a>
                </div>
                </div>
                <div class="row d-none">
                <div class="col-sm-12 col-md-6">
                  <a class="btn google-bg social-bg-hover d-block mb-3 mb-3" href="#"><span><i class="fab fa-google"></i>Login with Google</span></a>
                </div>
                <div class="col-sm-12 col-md-6">
                  <a class="btn linkedin-bg social-bg-hover d-block" href="#"><span><i class="fab fa-linkedin-in"></i>Login with Linkedin</span></a>
                </div>
                </div>
            </div>
          </div>

          <div class="tab-pane fade" id="regular-user" role="tabpanel" aria-labelledby="regular-user-tab">
        <!-- Registration form -->
            @csrf
            <div class="row my-5">
            <div class="form-group col-sm-6">
                <label>Firstname:</label>
                <input type="text" class="form-control"  name="firstname" id="firstname" placeholder="" required>
              </div>
              <div class="form-group col-sm-6">
                <label>Lastname:</label>
                <input type="text" class="form-control" name="lastname" id="lastname" placeholder="" required>
              </div>
            </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Email Address:</label>
                <input type="email" class="form-control"  name="email" id="email" placeholder=""required>
              </div>
              <div class="form-group col-sm-6">
                <label>Phone No:</label>
                <input type="text" class="form-control"  name="phone" id="phone" placeholder="" required>
              </div>
              </div>
             <div class="row">
             <div class="form-group col-sm-6">
                <label>Identity Type:</label>
                <select class="form-control js-select2 "  name="identity_type" id="identity_type" js-select2-id="1" tabindex="-1" aria-hidden="true">
                  <option>Aadhar card</option>
                  <option>Voter card</option>
                  <option>Pan card</option>
                </select>
              </div>
              <div class="form-group col-sm-6">
                <label>Identity No:</label>
                <input type="text" class="form-control" name="identity_number" id="identity_number" placeholder="" required>
              </div>
             </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Password:</label>
                <input type="password" class="form-control"  name="pass" id="pass" placeholder=""required>
                <span toggle="#pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
              </div>
              <div class="form-group col-sm-6">
                <label>Confirm Password:</label>
                <input type="password" class="form-control" name="confirm_pass" id="confirm_pass" placeholder=""required>
                <span toggle="#confirm_pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
              </div>
            
              </div>
              <div class="row">
              <div class="form-group col-sm-12">
              <label for="cars">Signup As:</label>
              <select class="form-control js-select2 " js-select2-id="1" name="user_type" id="user_type" tabindex="-1" aria-hidden="true">
                  <option data-select2-id="101">User</option>
                  <option data-select2-id="102">Owner</option>
                  <option data-select2-id="103">Broker</option>
                </select>
              </div>
              </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Locality</label>
                <input type="text" class="form-control" name="locality" id="locality" placeholder=""required>
              </div>
      
              <div class="form-group col-sm-6">
                <label>City</label>
                <input type="text" class="form-control" name="city"  id="city" placeholder=""required>
              </div></div>
             <div class="row">
             <div class="form-group col-sm-12">
                <label>Address:</label>
              <textarea rows="4" cols="50" class="form-control" name="address" id="address" placeholder="Enter your text" required>
            </textarea>
              </div>
             </div>
            <div class="row">
            <div class="form-group col-sm-12 ">
              <div class="input-field">
                <label class="active"> Upload Your Photo</label>
                <div class="custom-file ">
                <input type="file" onchange="previewFile()"  name="pic" id="pic" style="position:absolute;left:10px; top:10px; z-index:1;opacity:.01;height:100px;width:100px;">
                  <img src="{{asset('images/user_logo.png')}}" style="height: 50px;width:100px;position:relative;" alt="">
                </div>
                    </div>
              </div>
            </div>
              
              <div class="col-sm-12">
                <button type="submit" class="btn btn-primary btn-block mt-4" onclick="register()">Sign up&nbsp; <span class="reset"></span></button>
              </div>
              <div class="col-sm-12 mx-auto">
                <ul class="list-unstyled d-flex mb-1 mt-sm-0 mt-3">
                  <li class="mx-auto my-4"><a href="#"onclick="login1()">Already Registered User? Click here to login</a></li>
                </ul>
              </div>

          </div>
          <div class="tab-pane fade show" id="forgetpass" role="tabpanel" aria-labelledby="forget">
          @csrf
              <div class="form-group col-sm-12 py-5">
           
              <label>Enter Your Registered Email</label>
                <input type="email" class="form-control" id="emaile" placeholder="">
              </div>
              <div class="col-sm-12">
                <button type="submit" onclick="forget()" class="btn btn-primary btn-block ">Reset Password&nbsp; <span class="reset"></span></button>
               
              </div>
              <div class="col-sm-12 mx-auto">
                <ul class="list-unstyled d-flex mb-1 mt-sm-0 mt-3">
                  <li class="mx-auto my-4"><a href="#" onclick="login1()">Already Registered User? Click here to login</a></li>
                </ul>
              </div>
          </div>
        </div>
      </div>
      <div class="col-md-1 col-2 d-flex h-25 position-absolute top-2" style="top:40px;right:80px;" >
    <a href="{{url('index')}}"><i class="fas fa-arrow-circle-left" style="font-size:40px;"></i></a>
    </div>
    </div>
  </div>
  <script>
function previewFile() {
  var preview = document.querySelector('img');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }
  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}
 function login1(){
  $('[href="#card"]').tab('show');
  }
</script>
<script>
$(".toggle-password").click(function() {
$(this).toggleClass("fa-eye fa-eye-slash");
var input = $($(this).attr("toggle"));
if (input.attr("type") == "password") {
  input.attr("type", "text");
} else {
  input.attr("type", "password");
}
});
   </script> 
<script src="{{asset('libs/register.js')}}"></script>
<script src="{{asset('libs/const.js')}}"></script>
  <script src="{{asset('jquery/jquery.js')}}"></script>
  <!-- <script src="{{asset('js/popper.min.js')}}"></script> -->
  <script src="{{asset('js/bootstrap.js')}}"></script>
</body>
</html>