<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body class="bg-secondary">

<div class="conatiner">
    <div class="row">
        <div class="col-md-4 mx-auto bg-light my-4">
        
            <form action="{{url('updatePaymentForm')}}" method="post">
            <h5 class="card-title text-center pt-2">UpdateForm</h5>
            <hr>
                @csrf
                <input class="form-control" type="hidden" name="id" value="{{$editData['id']}}">
                <label class="form-group">Email</label>
                <input class="form-control" type="email" name="email" value="{{$editData['Email']}}">
                <label class="form-group">Order_id</label>
                <input class="form-control" type="number" name="order_id" value="{{$editData['Order_id']}}">
                <label class="form-group">Transaction_id</label>
                <input class="form-control" type="number" name="transaction_id" value="{{$editData['Transaction_id']}}">
                <label class="form-group">Amount</label>
                <input class="form-control" type="number" name="amount" value="{{$editData['Amount']}}">
                <input class="btn btn-warning my-2" type="submit">
            </form>
        </div>
    </div>
</div>

    
</body>
</html>
  