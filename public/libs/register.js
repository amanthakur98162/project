
dataArray = []
register =()=>{
    let token= $("input[name=_token]").val()
    let fname = $('#firstname').val()
    let  lname= $('#lastname').val()
    let email = $('#email').val()
    let phone = $('#phone').val()
    let identity_type = $('#identity_type').val()
    let identity_number = $('#identity_number').val()
    let pass = $('#pass').val()
    let confirm_pass = $('#confirm_pass').val()
    let user_type = $('#user_type').val()
    let locality = $('#locality').val()
    let city = $('#city').val()
    let address = $('#address').val()
    let pic=document.getElementById('pic').files[0]
    // console.log(identity_number,confirm_pass,pass,user_type,locality,city,address,pic);
    //  if(fname == '' ||lname == '' ||email == '' ||phone == ''||identity_type == '' ||identity_number==''||confirm_pass == '' ||pass == ''||user_type == '' ||locality == ''||city == '' ||address == '' ){
      // Swal.fire({
      //     position: 'top-center',
      //     icon: 'error',
      //     title: 'Oops...',
      //     text: 'Something went wrong!',
      //     footer: '<span style="color:red">All fields are required<span>',
      //   })
    //       return false;
    //   }
    $( ".reset" ).addClass("spinner-border spinner-border-sm text-white" );
    let url ='signup';
    let formData = new FormData();
     formData.append('_token',token);
     formData.append('firstname',fname);
     formData.append('lastname',lname);
     formData.append('email',email);
     formData.append('phone',phone);
     formData.append('identity_type',identity_type);
     formData.append('identity_number',identity_number);
       formData.append('password',pass);
       formData.append('confirm_password',confirm_pass);
       formData.append('user_type',user_type);
       formData.append('locality',locality);
       formData.append('city',city);
       formData.append('address',address);
     formData.append('profile_pic',pic);
     let xhr = new XMLHttpRequest();
     xhr.open('POST',url)
     xhr.send(formData)
     xhr.onload = function(){
     console.log(xhr.responseText);
      let obj=JSON.parse(xhr.responseText);[]
      console.log(obj);  
     let st=obj.status;
    let me=obj.message;
     if(obj.status == false){
      $( ".reset" ).removeClass("spinner-border spinner-border-sm text-white" );
       Swal.fire({
          position: 'top-center',
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
          footer: '<span style="color:red">'+me+'<span>',
        })

            return false;
      } 
    Swal.fire({
      position: 'top-center',
      icon: 'success',
      title: me,
      showConfirmButton: false,
      timer:1000
    })
         setInterval( window.location.href = 'login',9000); 
}
}
//login
signin = ()=>{
  let token = $('input[name="_token"]').val()
  let email = $('#emaily').val()
  let password = $('#pas').val()
  let remember_me = 0
  if(email == '' || password == ''){
      $('#msg').html('All fields are required')
      $('#msg').css('color','red')
      return false;
  }
  if ($('#remember_me').is(':checked')) {
      remember_me = 1
  }
  $( ".reset" ).addClass("spinner-border spinner-border-sm text-white" );
  let url = "login";
  let formData = new FormData()
  formData.append('_token',token);
  formData.append('email',email)
  formData.append('password',password)
  formData.append('remember_me',remember_me)
  let xhr = new XMLHttpRequest
  xhr.open('POST',url)
  xhr.send(formData)
  xhr.onload = function(){
      let Obj = JSON.parse(xhr.responseText);
      let st = Obj.status;
      let me = Obj.message;
      if(st==false){
        $( ".reset" ).removeClass("spinner-border spinner-border-sm text-white" );
        Swal.fire({
          position: 'top-center',
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
          footer: '<span style="color:red">'+me+'<span>',
        })
          return false;
      }
      Swal.fire({
        position: 'top-center',
        icon: 'success',
        title: me,
        showConfirmButton: false,
        timer: 2000
      })
      setTimeout(() => {
          window.location.href = 'dash'
      }, 2000);        
  }
}
//forget_password_email_form
forget =()=>{
  let email = $('#emaile').val()
  let token= $("input[name=_token]").val()
  let url ='forget';
  let formData = new FormData();
   formData.append('_token',token);
   formData.append('email',email);
   $( ".reset" ).addClass("spinner-border spinner-border-sm text-white" );
   let xhr = new XMLHttpRequest();
   xhr.open('POST',url)
   xhr.send(formData)
   xhr.onload = function(){
   console.log(xhr.responseText);
    let obj=JSON.parse(xhr.responseText);
    console.log(obj);  
   let st=obj.status;
  let me=obj.message;
   if(obj.status == false){
    $( ".reset" ).removeClass("spinner-border spinner-border-sm text-white" );
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong!',
      footer: '<span style="color:red">'+me+'<span>',
    })
          return false;
    }
    Swal.fire({
      icon: 'success',
      title: me,
      showConfirmButton: false,
      timer: 2000
    })
    location.reload();
}
}
forgetpass =()=>{
  let _token= $("input[name='_token']").val()
  let passwordToken=$('#passwordToken').val()
  let password = $('#pass').val()
  let confirm_password=$('#confirm_pass').val()
  let url ='/forget_password';
  let formData = new FormData();
   formData.append('_token',_token);
   formData.append('passwordToken',passwordToken);
   formData.append('password',password);
   formData.append('confirm_password',confirm_password);
   let xhr = new XMLHttpRequest();
   xhr.open('POST',url)
   xhr.send(formData)
   xhr.onload = function(){
   console.log(xhr.responseText);
    let obj=JSON.parse(xhr.responseText);
    console.log(obj);  
   let st=obj.status;
  let me=obj.message;
   if(obj.status == false){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong!',
      footer: '<span style="color:red">'+me+'<span>',
    })
          return false;
    }
    Swal.fire({
      icon: 'success',
      title: me,
      showConfirmButton: false,
      timer: 2000
    })
    location.reload();
}
}