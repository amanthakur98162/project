<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-
    tible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About us</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    <style>
      .heart{
        font-size:18px;
      color:red;
      }
.success{
    color:#28a745;
}
.carousel-control-next {
    right: 1%;
    top: 40%;
    width: 50px;
    height: 50px;
    background: #28a745;
    opacity:1;
}
.carousel-control-next:hover {
    right: 1%;
    top: 40%;
    width: 50px;
    height: 50px;
    background:#28a745;
    opacity:1;
}
.carousel-control-prev {
    left: 1%;
    top: 40%;
    width: 50px;
    height: 50px;
    background: #28a745;
    opacity:1;
}
.carousel-control-prev:hover {
    left: 1%;
    top: 40%;
    width: 50px;
    height: 50px;
    background: #28a745;
    opacity:1;
}

    </style>
</head>
<body>
@include('header')
<section class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 mb-5 mb-lg-0 order-lg-2">
          <div class="sticky-top">
            <div class="mb-4">
              <h3>{{$data['data']['Property_title']}}</h3>
              <span class="d-block mb-3"><i class="fas fa-map-marker-alt fa-xs pr-2"></i>{{$data['data']['Google_Maps_Address']}}--/--{{$data['data']['Friendly_Address']}}</span>
              <span class="price font-xll text-success d-block"></span>
              <span class="sub-price font-lg text-dark d-block"><b>${{$data['data']['Property_price']}}/Sqft</b> </span>
              <ul class="property-detail-meta list-unstyled ">
                <li><a href="#"> <i class="fas fa-star text-warning pr-2"></i>3.9/5 </a></li>
                <li class="share-box">
                  <a href="#"> <i class="fas fa-share-alt"></i> </a>
                  <ul class="list-unstyled share-box-social">
                    <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
                    <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
                    <li> <a href="#"><i class="fab fa-linkedin"></i></a> </li>
                    <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
                    <li> <a href="#"><i class="fab fa-pinterest"></i></a> </li>
                  </ul>
                </li>
                @csrf
                @if($data['savecheck']==1)
                    <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" data-value="{{$data['data']['id']}}" id="check" onclick="getid(this)"></i></a></li>
                 @else
                    <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="far fa-heart" style="font-size:18px;color:red" data-value="{{$data['data']['id']}}" id="check" onclick="getid(this)"></i></a></li>
                    @endif
                <li><a href="#"> <i class="fas fa-exchange-alt"></i> </a></li>
                <li><a href="#"> <i class="fas fa-print"></i> </a></li>
              </ul>
            </div>
            <div class="agent-contact">
              <div class="d-flex align-items-center p-4 border border-bottom-0">
                <div class="agent-contact-avatar mr-3">
                  <img class="img-fluid rounded-circle avatar avatar-xl"  src="../profile_images/{{$data['userdetails']['profile_pic']}}"  alt="">
                </div>
                <div class="agent-contact-name">
                  <h6>{{$data['userdetails']['firstname']}} {{$data['userdetails']['lastname']}}</h6>
                  <a href="#">{{$data['userdetails']['user_type']}}</a>
                  <span class="d-block"><i class="fas fa-phone-volume pr-2 mt-2"></i>{{$data['userdetails']['phone']}}</span>
                </div>
              </div>
              <div class="d-flex">
                <a href="{{url('view-listing')}}/{{$data['data']['id']}}" class="btn btn-dark col b-radius-none">View listings</a>
                <a href="#" class="btn btn-success col ml-0 b-radius-none d-none">Request info</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-8 order-lg-1">
          <div class="property-detail-img popup-gallery">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                <div class="carousel-item active">
                @if($data['data']['Offer']=='Sold')
                  <img class="img" src="{{asset('images/download.png')}}" height="100px" width="300px"alt="" style="position:absolute;transform:rotate(30deg);z-index:1;top:80px;left:200px;">
                  @endif
                <img src="{{asset('images/property/big-img-01.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                @if($data['data']['Offer']=='Sold')
                  <img class="img" src="{{asset('images/download.png')}}" height="100px" width="300px"alt="" style="position:absolute;transform:rotate(30deg);z-index:1;top:80px;left:200px;">
                  @endif
                <img src="{{asset('images/property/big-img-01.jpg')}}"class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                @if($data['data']['Offer']=='Sold')
                  <img class="img" src="{{asset('images/download.png')}}" height="100px" width="300px"alt="" style="position:absolute;transform:rotate(30deg);z-index:1;top:80px;left:200px;">
                  @endif
                <img src="{{asset('images/property/big-img-01.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
                </div>
          </div>
          <div class="property-info mt-5">
            <div class="row">
              <div class="col-sm-3 mb-3 mb-sm-0">
                <h5>Property details</h5>
              </div>
              <div class="col-sm-9">
                <div class="row mb-3">
                  <div class="col-sm-6">
                    <ul class="property-list list-unstyled">
                      <li><b>Property ID:</b>{{$data['data']['id']}}</li>
                      <li><b>Price:</b>${{$data['data']['Property_price']}}</li>
                      <li><b>Property Size:</b> {{$data['data']['Area']}} Sq Ft</li>
                      <li><b>Bedrooms:</b> {{$data['data']['Bedrooms']}}</li>
                      <li><b>Bathrooms:</b>{{$data['data']['Bathrooms']}}</li>
                    </ul>
                  </div>
                  <div class="col-sm-6">
                    <ul class="property-list list-unstyled">
                      <li><b>Kitchen:</b> {{$data['data']['Kitchen']}}</li>
                      <li><b>Balcony:</b>{{$data['data']['Balcony']}}</li>
                      <li><b>Property age:</b> {{$data['data']['Building_age']}}</li>
                      <li><b>Property Type:</b>{{$data['data']['Property_Type']}}</li>
                      <li><b>Property Status:</b>{{$data['data']['Offer']}}</li>
                    </ul>
                  </div>
                </div>
                <h6 class="text-success mb-3 mb-sm-0">Additional details</h6>
                <div class="row">
                  <div class="col-sm-6">
                    <ul class="property-list list-unstyled mb-0">
                      <li><b>Region:</b>{{$data['data']['Regions']}}</li>
                      <li><b>Rental period:</b>{{$data['data']['Rental_Period']}}</li>
                      <li><b>Water:</b>{{$data['data']['Water']}} </li>
                    </ul>
                  </div>
                  <div class="col-sm-6">
                    <ul class="property-list list-unstyled mb-0">
                      <li><b>Parking:</b> {{$data['data']['Parking']}}</li>
                      <li><b>Ac:</b> {{$data['data']['AC']}}</li>
                      <li><b>Heater:</b> {{$data['data']['Heater']}}</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr class="mt-4 mb-4 mb-sm-5 mt-sm-5">
          <div class="property-description">
            <div class="row">
              <div class="col-sm-3 mb-3 mb-sm-0">
                <h5>Description</h5>
              </div>
              <div class="col-sm-9">
                <p>{{$data['data']['Description']}}</p>
              </div>
            </div>
          </div>
          <hr class="mt-4 mb-4 mb-sm-5 mt-sm-5">
          <div class="property-features">
            <div class="row">
              <div class="col-sm-3 mb-3 mb-sm-0">
                <h5>Features</h5>
              </div>
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-sm-6">
                    <ul class="property-list-style-2 list-unstyled mb-0">
                   @if(($data['data']['Parking'])=='Available')
                      <li>Parking</li>
                      @endif
                      @if(($data['data']['AC'])=='Available')
                      <li>Air Conditioning</li>
                      @endif
                      @if(($data['data']['Heater'])=='Available')
                      <li>Heater</li>
                      @endif
                      @if(($data['data']['Tv'])=='Available')
                      <li>Tv</li>
                      @endif
                   
                    </ul>
                  </div>
                  <div class="col-sm-6">
                    <ul class="property-list-style-2 list-unstyled mb-0">
                    @if(($data['data']['Water'])=='Available')
                      <li>Water</li>
                      @endif
                      @if(($data['data']['Kitchen'])=='Available')
                      <li>Kitchen</li>
                      @endif
                      @if(($data['data']['Balcony'])=='Available')
                      <li>Balcony</li>
                      @endif                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr class="mt-4 mb-4 mb-sm-5 mt-sm-5">
          <div class="property-address">
            <div class="row">
              <div class="col-sm-3 mb-3 mb-sm-0">
                <h5>Address</h5>
              </div>
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-sm-6">
                    <ul class="property-list list-unstyled mb-0">
                      <li><b>Address:</b> Virginia drive temple hills</li>
                      <li><b>State/County:</b> San francisco</li>
                      <li><b>Area:</b> Embarcadero</li>
                    </ul>
                  </div>
                  <div class="col-sm-6">
                    <ul class="property-list list-unstyled mb-0">
                      <li><b>City:</b> San francisco</li>
                      <li><b>Zip:</b> 4848494</li>
                      <li><b>Country:</b> United States</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr class="mt-4 mb-4 mb-sm-5 mt-sm-5">
          <div class="property-video">
            <div class="row">
              <div class="col-sm-3 mb-3 mb-sm-0">
                <h5>Property video</h5>
              </div>
              <div class="col-sm-9">
                <div class="embed-responsive embed-responsive-16by9">
                <iframe width="420" height="345" src="https://www.youtube.com/embed/{{str_replace('https://youtu.be/','',$data['data']['Video'])}}"></iframe>
                </div>
              </div>
            </div>
          </div>
         
          <hr class="mt-4 mb-4 mb-sm-5 mt-sm-5">
          <div class="property-schedule">
            <div class="row">
              <div class="col-sm-3 mb-3 mb-sm-0">
                <h5>Schedule a tour</h5>
              </div>
              <div class="col-sm-9">
                <div class="form-row">
                  <div class="form-group col-sm-6 datetimepickers">
                    <div class="input-group date" id="datetimepicker-01" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" placeholder="Date" data-target="#datetimepicker-01">
                      <div class="input-group-append" data-target="#datetimepicker-01" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-6 datetimepickers">
                    <div class="input-group date" id="datetimepicker-03" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" placeholder="Time" data-target="#datetimepicker-03">
                      <div class="input-group-append" data-target="#datetimepicker-03" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-12">
                    <input type="text" class="form-control" placeholder="Name">
                  </div>
                  <div class="form-group col-sm-12">
                    <input type="email" class="form-control" placeholder="Email">
                  </div>
                  <div class="form-group col-sm-12">
                    <input type="Text" class="form-control" placeholder="Phone">
                  </div>
                  <div class="form-group col-sm-12">
                    <textarea class="form-control" rows="4" placeholder="Message"></textarea>
                  </div>
                  <div class="form-group col-sm-12">
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                  <div class="col-sm-6"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@include('emailsection')
@include('footer')
<script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.js')}}"></script>
  <script>
    $(function(){
  $(".location a").click(function(){
    
    $(".demo:first-child").text($(this).text());
     $(".demo:first-child").val($(this).text());
  });

});
</script>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script>
  <script>
   
      setTimeout(function(){
  $('body').addClass('loaded');
}, 2000);
getid=(data)=>{
        let p_id=$(data).attr('data-value');
        let _token= $("input[name=_token]").val()
        var check=1;
        $('#check').click(function(){
          $(this).removeClass("fa-heart-o")
          $(this).addClass("fa-heart")
        location.reload();
        })       
         let url = "/getinproperty";
         let formData = new FormData()
         formData.append('p_id',p_id)
         formData.append('check',check)
         formData.append('_token',_token)
         let xhr = new XMLHttpRequest
         xhr.open('POST',url)
         xhr.send(formData)
         xhr.onload = function(){
            let Obj = JSON.parse(xhr.responseText);
            let st = Obj.status;
            let me = Obj.message;
            if(st==false){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    footer: '<span style="color:red">'+me+'<span>',
                  })
                  if(me=='already saved'){
                    $(this).removeClass("fa-heart-o")
                    $(this).addClass("fa-heart")
                  }
                  return false;
            }
            Swal.fire({
                icon: 'success',
                title: me,
                showConfirmButton: false,
                timer: 2000
              })
        location.reload();
           }
}
</script>

</body>
</html>