<style>
  @import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
  .heart {
  font-size: 25px;
	color:red;
}
/* input[type='checkbox']{
height:10px;
width:10px;
background-color: #26ae61;
position: relative;
outline: none;
-webkit-appearance: none;
transform: rotate(45deg);
}
input[type='checkbox']::before {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    top: -68%;
    left: 0px;
    border-radius: 75px 75px 0 0;
}

input[type='checkbox']::after {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    transform: rotate( 
-90deg
 );
    border-radius: 75px 75px 0 0;
    top: 12%;
    right: 75%;
}
input[type='checkbox']:checked, input:checked[type='checkbox']::after,input:checked[type='checkbox']::before{
  background-color: red;
} */
</style>

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="section-title text-center">
          <h2>Newly listed properties</h2>
          <p>Find your dream home from our Newly added properties</p>
        </div>
      </div>
    </div>
    <div class="row"> 
     <div class="container">
        <div class="row" id="data">
        </div>
    </div>
  <input type="hidden" id="emailll" value="{{Session::get('email')}}">
  <script>
  newly();
  mypropertyDataArray=[];
  function newly(index){
  let url = 'data';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('').html(me)
          return false;
      }
      tag_name= ''
      let data =  obj.data
      mypropertyDataArray = data
      for(i=0 ; i<mypropertyDataArray.length ; i++){  
tag_name+='@csrf'
tag_name+='<div class="col-md-3 col-lg-3">'                                                                                                                                    
tag_name+= '    <div class="property-item">'
tag_name+= '      <div class="property-image bg-overlay-gradient-04">'
                   if( mypropertyDataArray[i].Offer=='Sold'){
                   tag_name+= '    <img class="img" src="{{asset("images/download.png")}}" height="80px" width="200px"alt="" style="position:absolute;transform:rotate(30deg);z-index:1;top:30px;left:20px;">'
                   }
tag_name+= '        <img class="img-fluid" src="{{asset("images/property/grid/01.jpg")}}" alt="">'
tag_name+= '        <div class="property-lable">'
tag_name+= '          <span class="badge badge-md badge-primary">'+ mypropertyDataArray[i].Property_Type+'</span>'
tag_name+= '          <span class="badge badge-md badge-info">'+ mypropertyDataArray[i].Offer+'</span>'
tag_name+= '        </div>'
tag_name+= '        <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>'
tag_name+= '        <div class="property-agent">'
tag_name+= '          <div class="property-agent-image">'
tag_name+= '           <img class="img" src="profile_images/'+mypropertyDataArray[i]['userdetail'].profile_pic+'" height="34px"width="60px" alt="">'
tag_name+= '          </div>'
tag_name+= '          <div class="property-agent-info">'
tag_name+= '            <a class="property-agent-name" href="#">'+ mypropertyDataArray[i]['userdetail'].firstname+' '+mypropertyDataArray[i]['userdetail'].lastname+'</a>'
tag_name+= '            <span class="d-block">'+mypropertyDataArray[i]['userdetail'].user_type+'</span>'
tag_name+= '            <ul class="property-agent-contact list-unstyled">'
tag_name+= '              <li><a href="#"><i class="fas fa-mobile-alt"style="position:absolute;top:68%;left:18%;"></i> </a></li>'
tag_name+= '              <li><a href="#"><i class="fas fa-envelope" style="position:absolute;top:67%;left:47%;"></i> </a></li>'
tag_name+= '            </ul>'
tag_name+= '          </div>'
tag_name+= '        </div>'
tag_name+= '        <div class="property-agent-popup">'
tag_name+= '          <a href="#"><i class="fas fa-camera"></i> 06</a>'
tag_name+= '        </div>'
tag_name+= '      </div>'
tag_name+= '      <div class="property-details">'
tag_name+= '        <div class="property-details-inner">'
tag_name+= '          <h5 class="property-title"><a href="{{url("property")}}">'+ mypropertyDataArray[i].Property_title+' </a></h5>'
tag_name+= '          <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>'+ mypropertyDataArray[i].Google_Maps_Address+'</span>'
tag_name+= '          <span class="property-agent-date"><i class="far fa-clock fa-md"></i>'+timeSince(mypropertyDataArray[i].created_at)+'</span>'
tag_name+= '          <div class="property-price">$'+mypropertyDataArray[i].Property_price+'<span> / month</span> </div>'
tag_name+= '          <ul class="property-info list-unstyled d-flex">'
tag_name+= '            <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>'+ mypropertyDataArray[i].Bedrooms+'</span></li>'
tag_name+= '            <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>'+ mypropertyDataArray[i].Bathrooms+'</span></li>'
tag_name+= '            <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>sqft<span>'+ mypropertyDataArray[i].Area+'m</span></li>'
tag_name+= '          </ul>'
tag_name+= '        </div>'
tag_name+= '        <div class="property-btn">'
tag_name+= '         <a class="property-link" href="property/'+mypropertyDataArray[i].id+'">See Details</a>'
tag_name+= '          <ul class="property-listing-actions list-unstyled mb-0">'
tag_name+= '            <li class="property-compare d-none"><a data-toggle="tooltip" data-placement="top" title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>'
if(mypropertyDataArray[i].savecheck == 1){
 // alert('ffff')
  tag_name+= '            <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" id="check" onclick="save('+mypropertyDataArray[i]['id']+')"></i></a></li>'
}
else{
 // alert('dddd')
  tag_name+= '            <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart-o" id="check" onclick="save('+mypropertyDataArray[i]['id']+')"></i></a></li>'
}
tag_name+= '          </ul>'
tag_name+= '        </div>'
tag_name+= '      </div>'
tag_name+= '    </div>'
tag_name+= '  </div>'   
//  if(mypropertyDataArray[i]['savecheck']==1){
//   $('#check').removeClass("fa-heart-o");
//   $('#check').addClass("fa-heart");
// }                                                                                                                                                 
          }
            $('#data').html(tag_name);
          
        }
      }
    </script>
      <div class="col-12 text-center">
        <a class="btn btn-link" href="{{url('listing-list')}}"><i class="fas fa-plus"></i>View All Listings</a>
      </div>
    </div>
  </div>
