
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ghardhundo</title>
    <!-- <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" /> -->

  </head>
<body>
<header class="header">
  <div class="topbar">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="d-block d-md-flex align-items-center text-center">
            <div class="mr-3 d-inline-block">
              <a href="tel:+91-70 182 96 941"><i class="fa fa-phone mr-2 fa fa-flip-horizontal"></i>+91 70 182 96 941 </a>
            </div>
            <div class="mr-auto d-inline-block">
              <span class="mr-2 text-white">Get App:</span>
              <a class="pr-1" href="#"><i class="fab fa-android"></i></a>
              <a href="#"><i class="fab fa-apple"></i></a>
            </div>
            <!-- <div class="dropdown d-inline-block pl-2 pl-md-0">
              <a class="dropdown-toggle " href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <span class="demo">   Choose location </span><i class="fas fa-chevron-down pl-2"></i>
              </a>
              <div class="dropdown-menu location" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="#">Chandigarh</a>
                <a class="dropdown-item" href="#">Mohali</a>
                <a class="dropdown-item" href="#">Hamirpur</a>
                <a class="dropdown-item" href="#">Shimla</a>
                <a class="dropdown-item" href="#">Lahaulspiti</a>
                <a class="dropdown-item" href="#">Dharamshala</a>
              </div>
            </div> -->
            <div class="social d-inline-block">
              <ul class="list-unstyled">
                <li><a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
                <li><a href="#"> <i class="fab fa-twitter"></i> </a></li>
                <li><a href="#"> <i class="fab fa-linkedin"></i> </a></li>
                <li><a href="#"> <i class="fab fa-pinterest"></i> </a></li>
                <li><a href="#"> <i class="fab fa-instagram"></i> </a></li>
              </ul>
            </div>
            <div class="login d-inline-block">
              <a  href="{{url('login')}}">Hello sign in<i class="fa fa-user pl-2"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <nav class="navbar navbar-light bg-white navbar-static-top navbar-expand-md sticky-top">
        <div class="container-fluid">
          <button type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse"><i class="fas fa-align-left"></i></button>
          <a class="navbar-brand" href="{{url('index')}}"><h2 class="d-inline p-0 ">Ghar Dhundo</h2></a>
          <div class="navbar-collapse collapse justify-content-center">
            <ul class="nav navbar-nav">
              <li class="nav-item dropdown active">
                <a class="nav-link" id="navbarDropdown" role="button" data-toggle="" aria-haspopup="true"  href="{{url('index')}}"><i class="fas fa-home" style="font-size: medium;padding-right:4px;padding-bottom:3px;margin-left:0;"></i>Home</a>
              </li>
    
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="{{url('listing-list')}}" >
            Property <i class="fas fa-chevron-down fa-xs"></i>
          </a>
        </li>
        
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Owner<i class="fas fa-chevron-down fa-xs"></i>
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="{{url('ownergrid')}}">Owner grid</a></li>
            <li><a class="dropdown-item" href="{{url('ownerlist')}}">Owner list</a></li>
            <li><a class="dropdown-item d-none" href="{{url('details')}}">Owner detail</a></li>
            @if(Cookie::get('user_type')=='Owner'||Session::get('user_type')=='Owner')
            <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="{{url('dash')}}">Account <span class="badge badge-danger ml-2">4</span> <i class="fas fa-chevron-right fa-xs"></i></a>
            <ul class="dropdown-menu left-side">
              <li><a class="dropdown-item" href="{{url('dash')}}">Profile</a></li>
              <li><a class="dropdown-item" href="{{url('dash')}}">Post Property</a></li>
              <li><a class="dropdown-item" href="{{url('dash')}}">My properties </a></li>
              <li><a class="dropdown-item" href="{{url('dash')}}">Reuqest</a></li>
              <li><a class="dropdown-item" href="{{url('dash')}}">About me</a></li>
            </ul> 
          </li>
          @endif
        </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           Broker<i class="fas fa-chevron-down fa-xs"></i>
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="{{url('brokergrid')}}">Broker grid</a></li>
            <li><a class="dropdown-item" href="{{url('brokerlist')}}">Broker list</a></li>
            <li><a class="dropdown-item d-none" href="{{url('details')}}">Broker detail</a></li>
            @if(Cookie::get('user_type')=='Broker'||Session::get('user_type')=='Broker')
            <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="{{url('dash')}}">Account <span class="badge badge-danger ml-2">2</span> <i class="fas fa-chevron-right fa-xs"></i></a>
            <ul class="dropdown-menu left-side">
              <li><a class="dropdown-item" href="{{url('dash')}}">Profile</a></li>
              <li><a class="dropdown-item" href="{{url('dash')}}">Post Property</a></li>
              <li><a class="dropdown-item" href="{{url('dash')}}">My properties </a></li>
              <li><a class="dropdown-item" href="{{url('dash')}}">Reuqest</a></li>
              <li><a class="dropdown-item" href="account-saved-rental.php">About me</a></li>
            </ul>
          </li>
          @endif
        </ul>
      </li>
    
    
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="{{url('contact')}}"  >
            Contact <i class="fas fa-chevron-down fa-xs"></i>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="{{url('About')}}">
            About Us <i class="fas fa-chevron-down fa-xs"></i>
          </a>
        </li>
        @if(Cookie::get('user_type')=='Admin'||Session::get('user_type')=='Admin')
    <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="{{url('dash')}}">
            Admin Dashboard <i class="fas fa-chevron-down fa-xs"></i>
          </a>
        </li>
      @endif  
      @if(Cookie::get('user_type')=='User'||Session::get('user_type')=='User')
    <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="{{url('dash')}}">
            User Dashboard <i class="fas fa-chevron-down fa-xs"></i>
          </a>
        </li>
      @endif  
    </ul>
   
    </div>
    @if(Cookie::has('email')||Session::has('email'))
    <div class="add-listing d-none d-sm-block">
      <a class="btn btn-primary btn-md" href="{{url('submit')}}"> <i class="fa fa-plus-circle"></i>Add listing </a>
    </div>
    @endif
    </div>
  </nav>
</header>
<script>
     $(function(){
  $(".location a").click(function(){
    
    $(".demo:first-child").text($(this).text());
     $(".demo:first-child").val($(this).text());
  });

});</script> 
  <!-- <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.js"></script> -->
</body>
</html>