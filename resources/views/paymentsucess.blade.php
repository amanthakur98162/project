<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<script src="{{asset('jquery/jquery.js')}}"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="https://rawgit.com/someatoms/jsPDF-AutoTable/master/dist/jspdf.plugin.autotable.js"></script>

    <style>
    body{
	background-color:#333;
	font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
	color:#333;
	text-align:left;
	font-size:18px;
	margin:0;
}
.container{
	margin:0 auto;
	margin-top:35px;
	padding:40px;
	width:750px;
	height:auto;
	background-color:#fff;
}
caption{
	font-size:28px;
	margin-bottom:15px;
}
table{
	border:1px solid #333;
	border-collapse:collapse;
	margin:0 auto;
	width:740px;
}
td, tr, th{
	padding:12px;
	border:1px solid #333;
	width:185px;
}
th{
	background-color: #f0f0f0;
}
h4, p{
	margin:0px;
}
    </style>
</head>
<body>
<div class="container">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="https://rawgit.com/someatoms/jsPDF-AutoTable/master/dist/jspdf.plugin.autotable.js"></script>
<button class="btn btn-danger" onclick="generate()">Generate PDF</button><button class="btn btn-primary" style="float:right" onclick="redirect()">Home</button>

<table id="table">
<caption>
		Payment Receipt
</caption>
  <thead>
  <tr><th>Name/Description</th>
     <th>Property Id</th>
				<th>Transaction Id</th>
				<th>Cost</th>
			</tr>
  </thead>
  <tbody>
	
						@for($j=0;$j<$l=count(explode("," , $data['data']['property_id']));$j++)
						<tr>
				<td>{{$data[$j]['property'][0]['Property_title']}}</td>
				<td>{{$data[$j]['property'][0]['id']}}</td>
				<td>{{$data['data']['transaction_id']}}</td>
                <td>{{$data[$j]['property'][0]['Property_price']}}</td>
			</tr>	
						@endfor
		
				
		</tbody>
  <tfoot>
			<tr>
				<th colspan="3">Grand Total</th>
				<td>${{$data['data']['price']}}</td>
			</tr>
		</tfoot>
</table>
</div>
	<script>
function generate() {
  var doc = new jsPDF('p', 'pt' ,'A4');

  var elem = document.getElementById('table');
  var imgElements = document.querySelectorAll('#table thead tbody tfoot');
  var data = doc.autoTableHtmlToJson(elem);
  var links = [];
  doc.autoTable(data.columns, data.rows, {
    bodyStyles: {
      rowHeight: 30
    },
    drawCell: function(cell, opts) {
      if (opts.column.dataKey === 5) {
        links.push({
          x: cell.textPos.x,
          y: cell.textPos.y
        });
      }
    },
    addPageContent: function() {
      for (var i = 0; i < links.length; i++) {
        doc.textWithLink('google.com', links[i].x, links[i].y + 10, {
          url: 'https://www.google.com'
        });
      }
    }
  });

  doc.save("Receipt.pdf");
}
redirect=()=>{
	setInterval( window.location.href = 'index',3000); 
}
	</script>
</div>
</body>
</html>
