<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery.js')}}"></script>
</head>
<body>
@section('main_content')
<div class="container-fluid py-4">
<div class="row mx-auto">
<div class="col-md-6 text-white mx-auto">
<table class="table table-info table-border table-dark  table-responsive">
<thead class="bg-dark text-center">
<tr>
<th>Sr.No.</th>
<th>Username</th>
<th>Email</th>
<th>Booking-date</th>
<th>Action</th>
</tr>
</thead>
<tbody>
@foreach($data as $d)
<tr>
<td>{{$d['id']}}</td>
<td>{{$d['username']}}</td>
<td>{{$d['useremail']}}</td>
<td>{{$d['bookingdate']}}</td>
<td>
<a href="{{url('bookingedit_page',$d['id'])}}"><button class="btn btn-info">edit</button></a>
<a href="{{url('bookingdelete_page',$d['id'])}}"><button class="btn btn-danger">delete</button></a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
<div class="col-md-6">
@if(Session::has('error'))
            <div class="alert alert-success">{{Session::get('error')}}</div>
            @endif
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
<h1 class="text-center border border-dark bg-light">Bookings</h1>
<form method="post" action="{{url('book')}}"  class="border border-light text-white bg-info ">
  <div class="form-group">
    <label for="email">Username</label>
    @csrf
    <input type="text" class="form-control" name="uname">
  </div>
  <div class="form-group">
    <label for="email">Useremail</label>
    <input type="text" class="form-control" name="email">
  </div>
  <div class="form-group">
    <label for="booking">Bookingdate</label>
    <input type="date" class="form-control" name="bdate">
  </div>
  <input name="signin"  type="submit" class="btn btn-primary my-3">
</form>
</div>
</div>
</div>

</body>
</html

