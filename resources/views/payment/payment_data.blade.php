<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
    </style>

</head>
<body class="bg-secondary">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">{{Session::get('error')}}</div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success text-center">{{Session::get('success')}}</div>
                @endif
                <h4 class="card-header bg-light text-center text-danger">Payment Data</h4>
                <table class="table table-bordered  text-center">
                    <thead>
                        <tr class=" bg-info">
                            <th>SR.no.</th>
                            <th>User id</th>
                            <th>Order id</th>
                            <th>Transaction id</th>
                            <th>Property id</th>
                            <th>Prize</th>
                            <th>Discount</th>
                            <th>Action</th>
                        </tr>  
                    </thead>
                    <tbody>
                    @foreach($data as $d)
                    <tr class="text-light">
                        <td>{{$d['id']}}</td>
                        <td>{{$d['user_id']}}</td>
                        <td>{{$d['Order_id']}}</td>
                        <td>{{$d['Transaction_id']}}</td>
                        <td>{{$d['property_id']}}</td>
                        <td>{{$d['prize']}}</td>
                        <td>{{$d['discount']}}</td>
                        <td><a href= "{{url('edit_payment_data' ,$d['id'])}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                            <a href= "{{url('delete_payment_data' ,$d['id'])}}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    @endforeach           
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    
</div>


</body>
<html>