
<div class="container py-5">
    <div class="row justify-content-center">
      <div class="col-lg-8">
        <div class="section-title text-center">
          <h2>Discover our best deals</h2>
          <p>Check the listings of the best dealer on Ghardhundo and contact the agency or its agent by phone or contact form.</p>
        </div>
      </div>
    </div>
<div class="container py-5">
      <div id="discover" class="carousel slide" data-ride="carousel" data-interval="1500" >
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="d-block w-100" >
      <div class="item">
            <div class="property-offer">
              <div class="property-offer-item">
                <div class="property-offer-image bg-holder" style="background:url('{{asset('images/property/big-img-01.jpg')}}');opacity:0.8;">
                  <div class="row">
                    <div class="col-lg-6 col-md-10 col-sm-12">
                      <div class="property-details">
                        <div class="property-details-inner">
                          <h5 class="property-title"><a href="{{url('property')}}" id="title1"> </a></h5>
                          <i class="fas fa-map-marker-alt fa-xs"></i>  <span class="property-address" id="location1"></span>
                          <i class="far fa-clock fa-md"></i> <span class="property-agent-date"id="date1"></span>
                          <p class="mb-0 d-block mt-3" id="description1"></p>
                          <div class="property-price" id="property_price1"></div><span class="per"></span>
                          <ul class="property-info list-unstyled d-flex">
                            <li class="flex-fill property-bed" ><i class="fas fa-bed"></i>Bed<span id="bedrooms1"></span></li>
                            <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span  id="bathrooms1"></span></li>
                            <li class="flex-fill property-m-sqft" ><i class="far fa-square"></i>sqft<span id="Area1">m</span></li>
                          </ul>
                        </div>
                        <div class="property-btn">
                          <a class="property-link" id="seedetail1" href="">See Details</a>
                          <ul class="property-listing-actions list-unstyled mb-0">
                            <li class="property-compare d-none"><a data-toggle="tooltip" data-placement="top" title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                            <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" id="check1"></i></a></li>
                          </ul>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
    <div class="carousel-item ">
    <div class="d-block w-100" >
      <div class="item">
            <div class="property-offer">
              <div class="property-offer-item">
                <div class="property-offer-image bg-holder" style="background:url('{{asset('images/property/big-img-01.jpg')}}');opacity:0.8;">
                  <div class="row">
                    <div class="col-lg-6 col-md-10 col-sm-12">
                      <div class="property-details">
                        <div class="property-details-inner">
                          <h5 class="property-title" ><a href="{{url('property')}}"id="title2"> </a></h5>
                          <i class="fas fa-map-marker-alt fa-xs"></i> <span class="property-address"id="location2"><i class="fas fa-map-marker-alt fa-xs"></i></span>
                          <i class="far fa-clock fa-md"></i> <span class="property-agent-date" id="date2">10 days ago</span>
                          <p class="mb-0 d-block mt-3" id="description2"></p>
                          <div class="property-price" id="property_price2"></div><span class="per"></span> 
                          <ul class="property-info list-unstyled d-flex">
                            <li class="flex-fill property-bed"><i class="fas fa-bed" ></i>Bed<span id="bedrooms2"></span></li>
                            <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span id="bathrooms2"></span></li>
                            <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>sqft<span id="Area2">m</span></li>
                          </ul>
                        </div>
                        <div class="property-btn">
                          <a class="property-link" id="seedetail2" href="">See Details</a>
                          <ul class="property-listing-actions list-unstyled mb-0">
                            <li class="property-compare d-none"><a data-toggle="tooltip" data-placement="top" title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                            <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" id="check2"></i></a></li>
                          </ul>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
    </div>
    </div>
    <div class="carousel-item">
      <div class="d-block w-100" >
      <div class="item">
            <div class="property-offer">
              <div class="property-offer-item">
                <div class="property-offer-image bg-holder" style="background: url('{{asset('images/property/big-img-01.jpg')}}');opacity:0.8;">
                  <div class="row">
                    <div class="col-lg-6 col-md-10 col-sm-12">
                      <div class="property-details">
                        <div class="property-details-inner">
                          <h5 class="property-title"><a href="{{url('property')}}"id="property_title3"></a></h5>
                          <i class="fas fa-map-marker-alt fa-xs"></i><span class="property-address"id="location3"><i class="fas fa-map-marker-alt fa-xs"></i></span>
                          <i class="far fa-clock fa-md"></i>  <span class="property-agent-date"id="date3"></span>
                          <p class="mb-0 d-block mt-3" id="description3"></p>
                          <div class="property-price" id="property_price3"><span class="per"></span></div>
                          <ul class="property-info list-unstyled d-flex">
                            <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span id="bedrooms3"></span></li>
                            <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span id="bathrooms3"></span></li>
                            <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>sqft<span id="Area3">m</span></li>
                          </ul>
                        </div>
                        <div class="property-btn">
                          <a class="property-link" id="seedetail3" href="">See Details</a>
                          <ul class="property-listing-actions list-unstyled mb-0">
                            <li class="property-compare d-none"><a data-toggle="tooltip" data-placement="top" title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                            <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" id="check3"></i></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#discover" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon d-none" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#discover" role="button" data-slide="next">
    <span class="carousel-control-next-icon d-none" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
<script>
discover();
    discoverDataArray=[];
        function discover(index){
          let url = 'carousel';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('').html(me)
          return false;
      }
      let data =  obj.data;
    discoverDataArray = data;
    $('#title1').html(discoverDataArray[0]['Property_title']);
    $('#location1').html(discoverDataArray[0]['Google_Maps_Address']);
    $('#description1').html(discoverDataArray[0]['Description']);
    $('#property_price1').html('$ '+discoverDataArray[0]['Property_price']);
    $('#bedrooms1').html(discoverDataArray[0]['Bedrooms']);
    $('#bathrooms1').html(discoverDataArray[0]['Bathrooms']);
    $('#Area1').html(discoverDataArray[0]['Area']);
    $('#title2').html(discoverDataArray[1]['Property_title']);
    $('#location2').html(discoverDataArray[1]['Google_Maps_Address']);
    $('#description2').html(discoverDataArray[1]['Description']);
    $('#property_price2').html('$ '+discoverDataArray[1]['Property_price']);
    $('#bedrooms2').html(discoverDataArray[1]['Bedrooms']);
    $('#bathrooms2').html(discoverDataArray[1]['Bathrooms']);
    $('#Area2').html(discoverDataArray[1]['Area']);
    $('#title3').html(discoverDataArray[2]['Property_title']);
    $('#location3').html(discoverDataArray[2]['Google_Maps_Address']);
    $('#description3').html(discoverDataArray[2]['Description']);
    $('#property_price3').html('$ '+discoverDataArray[2]['Property_price']);
    $('#bedrooms3').html(discoverDataArray[2]['Bedrooms']);
    $('#bathrooms3').html(discoverDataArray[2]['Bathrooms']);
    $('#Area3').html(discoverDataArray[2]['Area']);
    $("#seedetail1").attr("href",'property/'+discoverDataArray[0]['p_id']);
    $("#seedetail2").attr("href",'property/'+discoverDataArray[1]['p_id']);
    $("#seedetail3").attr("href",'property/'+discoverDataArray[2]['p_id']);
    $('#date1').html(timeSince(discoverDataArray[0]['created_at']));  
      $('#date2').html(timeSince(discoverDataArray[1]['created_at'])); 
       $('#date3').html(timeSince(discoverDataArray[2]['created_at']));
       $('.per').html('/ month');
       if(discoverDataArray[0]["savecheck"]==1){
        $('#check1').addClass('fa-heart')
        $('#check1').removeClass('fa-heart-o')
    }
    else{
      $('#check1').addClass('fa-heart-o')
    }
    if(discoverDataArray[1]["savecheck"]==1){
        $('#check2').addClass('fa-heart')
        $('#check2').removeClass('fa-heart-o')
    }
    else{
      $('#check2').addClass('fa-heart-o')
    }
    if(discoverDataArray[2]["savecheck"]==1){
        $('#check3').addClass('fa-heart')
        $('#check3').removeClass('fa-heart-o')
    }
    else{
      $('#check3').addClass('fa-heart-o')
    }
    $('#check1').attr('onclick','save('+discoverDataArray[0]["id"]+')');
       $('#check2').attr('onclick','save('+discoverDataArray[1]["id"]+')');
       $('#check3').attr('onclick','save('+discoverDataArray[2]["id"]+')');
    }}
    
</script>