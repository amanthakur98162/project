<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://127.0.0.1:8000/paytm-callback','http://127.0.0.1:8000/paytm-callback?retryAllowed=false&errorMessage=Your%20payment%20has%20been%20declined%20by%20your%20bank.%20Please%20try%20again%20or%20use%20a%20different%20method%20to%20complete%20the%20payment.&errorCode=227'
];
}
