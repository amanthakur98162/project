
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Ghardhundo - Real Estate HTML5 Template" />
    <meta name="author" content="potenzaglobalsolutions.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>

    <!-- Favicon -->
    <!-- <link rel="shortcut icon" href="images/favicon.ico" />


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">


    <link rel="stylesheet" href="css/font-awesome/all.min.css" />
    <link rel="stylesheet" href="css/flaticon/flaticon.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />

    <link rel="stylesheet" href="css/style.css" /> -->

  </head>
<body>
  <div class="container py-5">
    <div class="row justify-content-center">
      <div class="col" >
        <div class="section-title text-center">
          <h2>Browse by category</h2>
          <p>To browse and buy in your areas of interest, look for properties by category.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="category">
          <ul class="list-unstyled mb-0">
            <li class="category-item">
              <a href="{{url('list')}}">
                <div class="category-icon">
                  <i class="flaticon-building-2"></i>
                </div>
                <h6 class="mb-0">Apartment</h6>
                <span id="Apartment">(457)</span>
              </a>
            </li>
            <li class="category-item">
              <a href="{{url('pg')}}">
                <div class="category-icon">
                  <i class="flaticon-skyline"></i>
                </div>
                <h6 class="mb-0">Pg</h6>
                <span id="pg">(659)</span>
              </a>
            </li>
            <li class="category-item">
              <a href="{{url('villa')}}">
                <div class="category-icon">
                  <i class="flaticon-home-2"></i>
                </div>
                <h6 class="mb-0">Villa</h6>
                <span id="villa">(298)</span>
              </a>
            </li>
            <li class="category-item">
              <a href="{{url('hostel')}}">
                <div class="category-icon">
                  <i class="flaticon-apartment-1"></i>
                </div>
                <h6 class="mb-0">Hostel</h6>
                <span id="hostel">(235)</span>
              </a>
            </li>
            <li class="category-item">
              <a href="{{url('forrent')}}">
                <div class="category-icon">
                  <i class="flaticon-for-rent"></i>
                </div>
                <h6 class="mb-0">For Rent</h6>
                <span id="forrent">(478)</span>
              </a>
            </li>
            <li class="category-item">
              <a href="{{url('forsale')}}">
                <div class="category-icon">
                  <i class="flaticon-for-sale"></i>
                </div>
                <h6 class="mb-0">For sale</h6>
                <span id="sale"></span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <!-- <script src="jquery/jquery.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.js"></script> -->
  <script>
browse();
    browse=[];
        function browse(index){
          let url = 'count';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('').html(me)
          return false;
      }
      let data =  obj.data;
    browse = data;
    $('#Apartment').html('('+browse['Apartment']+')');
    $('#sale').html('('+browse['sale']+')');
    $('#forrent').html('('+browse['forrent']+')');
    $('#hostel').html('('+browse['hostel']+')');
    $('#pg').html('('+browse['pg']+')');
    $('#villa').html('('+browse['villa']+')');
    }}
</script>
</body>
</html>