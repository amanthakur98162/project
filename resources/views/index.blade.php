<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ghardhundo</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}"/>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
 <script src="https://timeago.yarp.com/jquery.timeago.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.2.1/moment.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

    <style>
      .success{color:#26ae61;}
    </style>
<style>

.loader-wrapper {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1000;
  background: white;
}
.loader-wrapper .loader {
  display: block;
  position: relative;
  left: 50%;
  top: 50%;
  width: 100px;
  height: 100px;
  margin-top: -50px;
  margin-left: -50px;
  border: 3px solid transparent;
  border-top-color: #F44336;
  border-radius: 50%;
  z-index: 1001;
  -webkit-animation: spin 1.5s infinite linear;
          animation: spin 1.5s infinite linear;
}
.loader-wrapper .loader:before, .loader-wrapper .loader:after {
  content: '';
  position: absolute;
  border-radius: 50%;
}
.loader-wrapper .loader:before {
  top: 5px;
  left: 5px;
  right: 5px;
  bottom: 5px;
  border: 3px solid transparent;
  border-top-color:#F44336;
  -webkit-animation: spin 2s infinite linear;
          animation: spin 2s infinite linear;
}
.loader-wrapper .loader:after {
  top: 15px;
  left: 15px;
  right: 15px;
  bottom: 15px;
  border: 3px solid transparent;
  border-top-color: #F44336;
  -webkit-animation: spin 1s infinite linear;
          animation: spin 1s infinite linear;
}

@-webkit-keyframes spin {
  0% {
    -webkit-transform: rotate(0);
            transform: rotate(0);
  }
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}

@keyframes spin {
  0% {
    -webkit-transform: rotate(0);
            transform: rotate(0);
  }
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}
/*Section */
.loader-section {
  position: fixed;
  top: 0;
  width: 51%;
  height: 100%;
  background:#001022;
  overflow: hidden;
  z-index: 1000;
}
.loader-section.section-left {
  left: 0;
}
.loader-section.section-right {
  right: 0;
}

/*Finished Loading Styles*/
.loaded .section-left {
  -webkit-transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  -webkit-transform: translateX(-100%);
          transform: translateX(-100%);
}
.loaded .section-right {
  -webkit-transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  -webkit-transform: translateX(75%);
          transform: translateX(100%);
}
.loaded .loader {
  -webkit-transition: all 0.3s ease-out;
  transition: all 0.3s ease-out;
  opacity: 0;
}
.loaded .loader-wrapper {
  visibility: hidden;
 
}

</style>
<style>
.heart {
  font-size: 25px;
	color:red;
}
</style>
  </head>

<body id="body">
 <div class="loader-wrapper">
    <div class="loader"style="overflow:hidden;"></div>
    
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>

@include('header')
@include('banner')
@include('browse')
 @include('newly')
 @include('discover')
 @include("plenty");
 @include("testmonial");
 @include("agent")
 @include("popular")
 @include("newscard")
 @include("counter")
 @include("looking")
 @include("footer")
  <script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.js')}}"></script>
  <script src="{{asset('libs/saved.js')}}"></script>
  <script>
    $(function(){
  $(".location a").click(function(){
    
    $(".demo:first-child").text($(this).text());
     $(".demo:first-child").val($(this).text());
  });

});
</script>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
  <script>
      setTimeout(function(){
  $('body').addClass('loaded');
}, 2000);
var DURATION_IN_SECONDS = {
  epochs: ['year', 'month', 'day', 'hour', 'minute'],
  year: 31536000,
  month: 2592000,
  day: 86400,
  hour: 3600,
  minute: 60
};

function getDuration(seconds) {
  var epoch, interval;

  for (var i = 0; i < DURATION_IN_SECONDS.epochs.length; i++) {
    epoch = DURATION_IN_SECONDS.epochs[i];
    interval = Math.floor(seconds / DURATION_IN_SECONDS[epoch]);
    if (interval >= 1) {
      return {
        interval: interval,
        epoch: epoch
      };
    }
  }

};

function timeSince(date) {
  var seconds = Math.floor((new Date() - new Date(date)) / 1000);
  var duration = getDuration(seconds);
  var suffix = (duration.interval > 1 || duration.interval === 0) ? 's ago' : '';
  return duration.interval + ' ' + duration.epoch + suffix;
};
</script>
</body>
</html>
