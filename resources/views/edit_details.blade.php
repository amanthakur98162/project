
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
</head>
<body>
     
    <div class="container my-5 p-5">
        <div class="row">
            <div class="col-md-12">
            <form action="{{url('updateForm')}}" method="post">
          @csrf
             <input type="hidden" name="id_" value="{{$data['id']}}">
            <div class="row ">
            <div class="form-group col-sm-6">
                <label>Firstname:</label>
                <input type="text" class="form-control"  name="firstname" id="firstname" value="{{$data['firstname']}}">
              </div>
              <div class="form-group col-sm-6">
                <label>Lastname:</label>
                <input type="text" class="form-control" name="lastname" id="lastname" value="{{$data['lastname']}}">
              </div>
            </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Email Address:</label>
                <input type="email" class="form-control"  name="email" id="email" value="{{$data['email']}}">
              </div>
              <div class="form-group col-sm-6">
                <label>Phone No:</label>
                <input type="text" class="form-control"  name="phone" id="phone" value="{{$data['phone']}}">
              </div>
              </div>
             <div class="row">
             <div class="form-group col-sm-6">
                <label>Identity Type:</label>
                <input type="text" class="form-control"  name="identity_type" id="identity_type" value="{{$data['identity_type']}}">
              </div>
              <div class="form-group col-sm-6">
                <label>Identity No:</label>
                <input type="text" class="form-control" name="identity_number" id="identity_number" value="{{$data['identity_number']}}">
              </div>
             </div>
              <div class="row">
              <div class="form-group col-sm-12">
              <label for="cars">Signup As:</label>
              <input type="text" class="form-control"  name="user_type" id="user_type" value="{{$data['user_type']}}" >
              </div>
              </div>
              <div class="row">
              <div class="form-group col-sm-6">
                <label>Locality</label>
                <input type="text" class="form-control" name="locality" id="locality" value="{{$data['locality']}}">
              </div>
      
              <div class="form-group col-sm-6">
                <label>City</label>
                <input type="text" class="form-control" name="city"  id="city" value="{{$data['city']}}">
              </div></div>
             <div class="row">
             <div class="form-group col-sm-12">
                <label>Address:</label>
              <textarea rows="4" cols="50" class="form-control" name="address" id="address" >
              {{$data['address']}}
            </textarea>
              </div>
             </div>
           
              <input type="submit" class="form-control btn btn-success">
             </form>
            </div>
        </div>
    </div>
</body>
</html>