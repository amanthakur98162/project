
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="{{asset('css/ion.rangeSlider.min.css')}}"/>
<!--Plugin JavaScript file-->
<script src="{{asset('js/ion.rangeSlider.min.js')}}"></script>
<style>

.range-slider {
    position: relative;
    height: 80px;
}
.irs--round .irs-bar {
    top: 36px;
    height: 4px;
    background-color: #219553;
}
.irs--round .irs-handle {
    top: 26px;
    width: 24px;
    height: 24px;
    border: 4px solid #219553;
    background-color:#219553;
    border-radius: 24px;
    box-shadow: 0 1px 3px rgb(0 0 255 / 30%);
}
.irs--round .irs-from, .irs--round .irs-to, .irs--round .irs-single {
    font-size: 14px;
    line-height: 1;
    text-shadow: none;
    padding: 3px 5px;
    background-color: #219553;
    color: white;
    border-radius: 4px;
}

</style>
</head>
<body>
  <div class="range-slider">
    <input type="text" class="js-range-slider" value="" />
</div>
<div class="extra-controls">
    <input type="hidden" class="js-input-from" id="from" name='from' value="0" />
    <input type="hidden" class="js-input-to" id="to"  name="to" value="0" />
</div>
<script>
   var $range = $(".js-range-slider"),
    $inputFrom = $(".js-input-from"),
    $inputTo = $(".js-input-to"),
    instance,
    min = 0,
    max = 1000,
    from = 0,
    to = 0;

$range.ionRangeSlider({
	skin: "round",
    type: "double",
    min: min,
    max: max,
    from: 200,
    to: 800,
    onStart: updateInputs,
    onChange: updateInputs
});
instance = $range.data("ionRangeSlider");

function updateInputs (data) {
	from = data.from;
    to = data.to;
    
    $inputFrom.prop("value", from);
    $inputTo.prop("value", to);	
}

$inputFrom.on("input", function () {
    var val = $(this).prop("value");
    
    // validate
    if (val < min) {
        val = min;
    } else if (val > to) {
        val = to;
    }
    
    instance.update({
        from: val
    });
});

$inputTo.on("input", function () {
    var val = $(this).prop("value");
    
    // validate
    if (val < from) {
        val = from;
    } else if (val > max) {
        val = max;
    }
    
    instance.update({
        to: val
    });
});

</script>
</body>
</html>