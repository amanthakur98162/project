<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About us</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    <style>
      .heart {
  font-size: 25px;
	color:red;
}
    .img-fluid {
    max-width: 100%;
}
    </style>
    <style>
input[type='checkbox']{
height:10px;
width:10px;
background-color: #26ae61;
position: relative;
outline: none;
-webkit-appearance: none;
transform: rotate(45deg);
}
input[type='checkbox']::before {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    top: -68%;
    left: 0px;
    border-radius: 75px 75px 0 0;
}

input[type='checkbox']::after {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    transform: rotate( 
-90deg
 );
    border-radius: 75px 75px 0 0;
    top: 12%;
    right: 75%;
}
input[type='checkbox']:checked{
  background-color: red;
}
input:checked[type='checkbox']::after{
  background-color: red;
}
input:checked[type='checkbox']::before{
  background-color: red;
}
</style>
</head>
<body>
@include('header')
<section class="space-ptb">
  <div class="container">
  <h1 class="text-center my-5">User Details</h1>
  @if($data[0]['data']==''&& $data[0]['userdetails'])
  <div class="row mt-4">
  <div class="col-md-12">
  <div class="agent agent-03 mt-4">
          <div class="row no-gutters">
            <div class="col-lg-4 text-center border-right">
              <div class="d-flex flex-column h-100">
                <div class="agent-avatar p-3 my-auto">
                  <img class="img" src="../profile_images/{{$data[0]['userdetails']['profile_pic']}}" height="250px" width="250px" alt="">
                </div>
                <div class="agent-listing text-center mt-auto">
                  <a href="#"><strong class="text-success d-inline-block mr-2">0</strong>Listed Properties </a>
                </div>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="d-flex h-100 flex-column">
                <div class="agent-detail">
                  <div class="d-sm-flex">
                    <div class="agent-name">
                      <h5 class="mb-0"> <a href="#">{{$data[0]['userdetails']['firstname']}} {{$data[0]['userdetails']['lastname']}}</a></h5>
                      <span>{{$data[0]['userdetails']['user_type']}}</span>
                    </div>
                    <div class="agent-social ml-auto mt-2 mt-sm-0">
                      <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i> </a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i> </a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin"></i> </a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="agent-info">
                    <p class="mt-3 mb-3">{{$data[0]['userdetails']['about']}}</p>
                    <ul class="list-unstyled mb-0">
                  
                      <li><strong>Email:&nbsp;</strong>{{$data[0]['userdetails']['email']}}</li>
                    </ul>
                    <ul class="list-unstyled mb-0">
                      <li><strong>Mobile:&nbsp;</strong>{{$data[0]['userdetails']['phone']}}</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
  </div>
  </div>
  @else
  <div class="row mt-4">
  <div class="col-md-12">
  <div class="agent agent-03 mt-4">
          <div class="row no-gutters">
            <div class="col-lg-4 text-center border-right">
              <div class="d-flex flex-column h-100">
                <div class="agent-avatar p-3 my-auto">
                  <img class="img" src="../profile_images/{{$data[0]['userdetail']['profile_pic']}}" height="250px" width="250px" alt="">
                </div>
                <div class="agent-listing text-center mt-auto">
                  <a href="#"> <strong class="text-success d-inline-block mr-2">{{Count($data)}}</strong>Listed Properties </a>
                </div>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="d-flex h-100 flex-column">
                <div class="agent-detail">
                  <div class="d-sm-flex">
                    <div class="agent-name">
                      <h5 class="mb-0"> <a href="#">{{$data[0]['userdetail']['firstname']}} {{$data[0]['userdetail']['lastname']}}</a></h5>
                      <span>{{$data[0]['userdetail']['user_type']}}</span>
                    </div>
                    <div class="agent-social ml-auto mt-2 mt-sm-0">
                      <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i> </a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i> </a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin"></i> </a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="agent-info">
                    <p class="mt-3 mb-3">{{$data[0]['userdetail']['about']}}</p>
                    <ul class="list-unstyled mb-0">
                  
                      <li><strong>Email:&nbsp;</strong>{{$data[0]['userdetail']['email']}}</li>
                    </ul>
                    <ul class="list-unstyled mb-0">
                      <li><strong>Mobile:&nbsp;</strong>{{$data[0]['userdetail']['phone']}}</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
  </div>
  </div>
  @endif
  
  <h1 class="text-center my-5">Listed Properties</h1>
  @if($data[0]['data']==''&& $data[0]['userdetails'] )
      <div class="row my-4">
      <div class="col-md-12 border py-5 px-5">
      <h4 class="text-center" style="color:#26ae61">No properties listed by user</h4>
      </div>
      </div>
  @else
       <div class="row mt-4">
        @for($i=0;$i<$j=count($data);$i++)
        <div class="col-sm-4">
        @csrf
            <div class="property-item">
              <div class="property-image bg-overlay-gradient-04">
              @if($data[$i]['Offer']=='Sold')
                  <img class="img" src="{{asset('images/download.png')}}" height="100px" width="200px"alt="" style="position:absolute;transform:rotate(30deg);z-index:1;top:80px;left:60px;">
                  @endif
                <img class="img-fluid" src="{{asset('images/property/grid/01.jpg')}}" alt="">
                <div class="property-lable">
                  <span class="badge badge-md badge-primary">{{$data[$i]['Property_Type']}}</span>
                  <span class="badge badge-md badge-info">{{$data[$i]['Offer']}}</span>
                </div>
                <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>
                <div class="property-agent">
                  <div class="property-agent-image">
                    <img class="img" src="../profile_images/{{$data[$i]['userdetail']['profile_pic']}}" height="34px"width="60px" alt="">
                  </div>
                  <div class="property-agent-info">
                    <a class="property-agent-name" href="#">{{$data[$i]['userdetail']['firstname']}} {{$data[$i]['userdetail']['lastname']}}</a>
                    <span class="d-block">{{$data[$i]['userdetail']['user_type']}}</span>
                    <ul class="property-agent-contact list-unstyled">
                      <li><a href="#"><i class="fas fa-mobile-alt"></i></a></li>
                      <li><a href="#"><i class="fas fa-envelope"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="property-agent-popup">
                  <a href="#"><i class="fas fa-camera"></i> 06</a>
                </div>
              </div>
              <div class="property-details">
                <div class="property-details-inner">
                  <h5 class="property-title"><a href="property-detail-style-01.html"> </a></h5>
                  <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i></span>
                  <span class="property-agent-date"><i class="far fa-clock fa-md"></i>10 days ago</span>
                  <div class="property-price"><span>$&nbsp;{{$data[$i]['Property_price']}}/ month</s> </div>
                  <ul class="property-info list-unstyled d-flex">
                    <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>{{$data[$i]['Bedrooms']}}</span></li>
                    <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>{{$data[$i]['Bathrooms']}}</span></li>
                    <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>sqft<span>{{$data[$i]['Area']}}m</span></li>
                  </ul>
                </div>
                <div class="property-btn">
                  <a class="property-link" href="{{url('property/'.$data[$i]['id'])}}">See Details</a>
                  <ul class="property-listing-actions list-unstyled mb-0">
                    <li class="property-compare d-none"><a data-toggle="tooltip" data-placement="top" title="" href="#" data-original-title="Compare"><i class="fas fa-exchange-alt"></i></a></li>
                    @if($data[$i]['savecheck']==1)
                    <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" data-value="{{$data[$i]['id']}}" id="check" onclick="getid(this)"></i></a></li>
                    @else
                    <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="far fa-heart" style="font-size:28px;color:red" data-value="{{$data[$i]['id']}}" id="check" onclick="getid(this)"></i></a></li>
                    @endif
                  </ul>
                </div>
              </div>
            </div>
          </div>
          @endfor
        </div>   
    </div>
    @endif
</section>

@include('emailsection')
@include('footer')
<script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.js')}}"></script>
  <script>
    $(function(){
  $(".location a").click(function(){
    
    $(".demo:first-child").text($(this).text());
     $(".demo:first-child").val($(this).text());
  });

});
</script>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script>
  <script>
   
      setTimeout(function(){
  $('body').addClass('loaded');
}, 2000);

getid=(data)=>{

        let p_id=$(data).attr('data-value')
        let _token= $("input[name=_token]").val()
        var check=1;
        $('#check').click(function(){
          $(this).removeClass("fa-heart-o")
          $(this).addClass("fa-heart")
        location.reload();
        })       
         let url ="/getsavedviewprofile";
         let formData = new FormData()
         formData.append('p_id',p_id)
         formData.append('check',check)
         formData.append('_token',_token)
         let xhr = new XMLHttpRequest
         xhr.open('POST',url)
         xhr.send(formData)
         xhr.onload = function(){
            let Obj = JSON.parse(xhr.responseText);
            let st = Obj.status;
            let me = Obj.message;
            if(st==false){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    footer: '<span style="color:red">'+me+'<span>',
                  })
                  if(me=='already saved'){
                    $(this).removeClass("fa-heart-o")
                    $(this).addClass("fa-heart")
                  }
                  return false;
            }
            Swal.fire({
                icon: 'success',
                title: me,
                showConfirmButton: false,
                timer: 2000
              })
        location.reload();
           }
}
</script>

</body>
</html>