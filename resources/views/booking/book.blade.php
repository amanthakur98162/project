<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery.js')}}"></script>
</head>
<style>
.form-group{
  color:green;
}
.form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
</style>
<body>
@section('main_content')
<div class="container">
  <div class="row">
   <div class="col-md-6 mx-auto my-5">
          @if(Session::has('error'))
           <div class="alert alert-success">{{Session::get('error')}}</div>
            @endif
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
         <form method="post" action="{{url('book')}}"  class="border border-light text-dark bg-light">
           <h1 class="text-center bg-light text-success">Bookings</h1>
           <div class="form-group">
           <label for="email">Username</label>
           @csrf
          <input type="text" class="form-control" name="uname">
          </div>
          <div class="form-group">
         <label for="email">Useremail</label>
          <input type="text" class="form-control" name="email">
          </div>
         <div class="form-group">
          <label for="booking">Bookingdate</label>
         <input type="date" class="form-control" name="bdate">
         </div>
         <input name="signin"  type="submit" class="btn btn-success btn-block my-4">
        </form>
     </div>
    </div>
   </div>
  </div>
</div>

<div class="container-fluid py-4">
<div class="row mx-auto">
<div class="col-md-6 mx-auto text-white">
<table class="table table-light table-border table-hover  table-responsive">
<thead class="bg-light text-center">
<tr>
<th>s no.</th>
<th>username</th>
<th>email</th>
<th>bookingdate</th>
<th>action</th>
</tr>
</thead>
<tbody>
@foreach($data as $d)
<tr>
<td>{{$d['id']}}</td>
<td>{{$d['username']}}</td>
<td>{{$d['useremail']}}</td>
<td>{{$d['bookingdate']}}</td>
<td>
<a href="{{url('bookingedit_page',$d['id'])}}"><button class="btn btn-success">edit</button></a>
<a href="{{url('bookingdelete_page',$d['id'])}}"><button class="btn btn-danger">delete</button></a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</body>
</html

