<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About us</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    <style>
.heart {
  font-size: 25px;
	color:red;
}
</style>
    <style>

input[type='checkbox']{
height:10px;
width:10px;
background-color: #26ae61;
position: relative;
outline: none;
-webkit-appearance: none;
transform: rotate(45deg);
}
input[type='checkbox']::before {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    top: -68%;
    left: 0px;
    border-radius: 75px 75px 0 0;
}

input[type='checkbox']::after {
    position: absolute;
    content: "";
    background-color: #26ae61;
    height: 7.5px;
    width: 10px;
    transform: rotate( 
-90deg
 );
    border-radius: 75px 75px 0 0;
    top: 12%;
    right: 75%;
}
input[type='checkbox']:checked{
  background-color: red;
}
input:checked[type='checkbox']::after{
  background-color: red;
}
input:checked[type='checkbox']::before{
  background-color: red;
}
.heart {
  font-size: 25px;
	color:red;
}
</style>
    <style>
    .img-fluid {
    max-width: 100%;
}
.w-5{
  height:20px;
}
</style>
</head>
<body>
@include('header')
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="section-title mb-3 mb-lg-4">
          <h2><span class="text-success">{{count($list)}}</span> Results</h2>
        </div>
      </div>
      <div class="col-md-6">
        <div class="property-filter-tag">
          <ul class="list-unstyled">
            <li><a href="#">Summer House <i class="fas fa-times-circle"></i> </a></li>
            <li><a class="filter-clear" href="#">Reset Search <i class="fas fa-redo-alt"></i> </a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 mb-5 mb-lg-0">
        <div class="sidebar ">
          <div class="widget">
            <div class="widget-title widget-collapse">
              <h6>Advanced filter</h6>
              <a class="ml-auto" data-toggle="collapse" href="#filter-property" role="button" aria-expanded="false" aria-controls="filter-property"> <i class="fas fa-chevron-down"></i> </a>
            </div>
            <div class="collapse show" id="filter-property">
              <form class="mt-3">
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                    <option data-select2-id="3">All Type</option>
                    <option>Villa</option>
                    <option>Apartment Building</option>
                    <option>Commercial</option>
                    <option>Office</option>
                    <option>Residential</option>
                    <option>Shop</option>
                    <option>Apartment</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select select2-hidden-accessible" data-select2-id="4" tabindex="-1" aria-hidden="true">
                    <option data-select2-id="6">For Rent</option>
                    <option>For Sale</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select select2-hidden-accessible" data-select2-id="7" tabindex="-1" aria-hidden="true">
                    <option data-select2-id="9">Distance from location</option>
                    <option>Within 1 mile</option>
                    <option>Within 3 miles</option>
                    <option>Within 5 miles</option>
                    <option>Within 10 miles</option>
                    <option>Within 15 miles</option>
                    <option>Within 30 miles</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select select2-hidden-accessible" data-select2-id="10" tabindex="-1" aria-hidden="true">
                    <option data-select2-id="12">Bedrooms</option>
                    <option>01</option>
                    <option>02</option>
                    <option>03</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select select2-hidden-accessible" data-select2-id="13" tabindex="-1" aria-hidden="true">
                    <option data-select2-id="15">Sort by</option>
                    <option>Most popular</option>
                    <option>Highest price</option>
                    <option>Lowest price</option>
                    <option>Most reduced</option>
                  </select>
                </div>
                <div class="input-group mb-2 select-border">
                  <select class="form-control basic-select select2-hidden-accessible" data-select2-id="16" tabindex="-1" aria-hidden="true">
                    <option data-select2-id="18">Select Floor</option>
                    <option>01</option>
                    <option>02</option>
                    <option>03</option>
                  </select>
                </div>
                <div class="input-group mb-2">
                  <input class="form-control" placeholder="Type (sq ft)">
                </div>
                <div class="input-group mb-2">
                  <input class="form-control" placeholder="Type (sq ft)">
                </div>
                 <div class="form-group property-price-slider mt-3">
                  <label>Select Price Range</label>
                  @include('range1')
                </div>
                <div class="input-group mb-2">
                  <button class="btn btn-success btn-block align-items-center" type="submit"><i class="fas fa-filter mr-1"></i><span>Filter</span></button>
                </div>
              </form>
            </div>
          </div>
          <div class="widget">
            <div class="widget-title widget-collapse">
              <h6>Status of property</h6>
              <a class="ml-auto" data-toggle="collapse" href="#status-property" role="button" aria-expanded="false" aria-controls="status-property"> <i class="fas fa-chevron-down"></i> </a>
            </div>
            <div class="collapse show" id="status-property">
              <ul class="list-unstyled mb-0 pt-3">
                <li><a href="{{url('forrent')}}">For rent<span class="ml-auto"></span></a></li>
                <li><a href="{{url('forsale')}}">For Sale<span class="ml-auto"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="widget">
            <div class="widget-title widget-collapse">
              <h6>Type of property</h6>
              <a class="ml-auto" data-toggle="collapse" href="#type-property" role="button" aria-expanded="false" aria-controls="type-property"> <i class="fas fa-chevron-down"></i> </a>
            </div>
            <div class="collapse show" id="type-property">
              <ul class="list-unstyled mb-0 pt-3">
                <li><a href="{{url('hostel')}}">Hostel<span class="ml-auto"></span></a></li>
                <li><a href="{{url('pg')}}">Pg<span class="ml-auto"></span></a></li>
                <li><a href="{{url('villa')}}">Villa<span class="ml-auto"></span></a></li>
                <li><a href="{{url('list')}}">Apartment<span class="ml-auto"></span></a></li>
                
              </ul>
            </div>
          </div>
          <div class="widget d-none">
            <div class="widget-title">
              <h6>Mortgage calculator</h6>
            </div>
            <form>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                </div>
                <input type="text" class="form-control" placeholder="Total Amount">
              </div>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                </div>
                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Down Payment">
              </div>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fas fa-percent"></i></div>
                </div>
                <input type="text" class="form-control" placeholder="Interest Rate">
              </div>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
                <input type="text" class="form-control" placeholder="Loan Term (Years)">
              </div>
              <div class="input-group mb-3 select-border">
                <select class="form-control basic-select select2-hidden-accessible" data-select2-id="19" tabindex="-1" aria-hidden="true">
                  <option data-select2-id="21">Monthly</option>
                  <option>Weekly</option>
                  <option>Yearly</option>
                </select>
              </div>
              <a class="btn btn-success btn-block" href="#">Calculate</a>
            </form>
          </div>
        </div>
      </div>
      <div class="col-lg-9">
        <div class="property-filter d-sm-flex">
          <ul class="property-short list-unstyled d-sm-flex mb-0">
            <li>
              <form class="form-inline">
                <div class="form-group d-lg-flex d-block">
                  <label class="justify-content-start">Short by: </label>
                  <div class="short-by">
                    <select class="form-control basic-select select2-hidden-accessible" data-select2-id="22" tabindex="-1" aria-hidden="true">
                      <option data-select2-id="24">Date new to old </option>
                      <option>Price Low to High</option>
                      <option>Price High to Low</option>
                      <option>Date Old to New</option>
                      <option>Date New to Old</option>
                    </select>
                  </div>
                </div>
              </form>
            </li>
          </ul>
          <ul class="property-view-list list-unstyled d-flex mb-0">
            <li>
              <form class="form-inline">
                <div class="form-group d-lg-flex d-block">
                  <label class="justify-content-start pr-2">Sort by: </label>
                  <div class="short-by">
                    <select class="form-control basic-select select2-hidden-accessible" data-select2-id="25" tabindex="-1" aria-hidden="true">
                      <option data-select2-id="27">12</option>
                      <option>18 </option>
                      <option>24 </option>
                      <option>64 </option>
                      <option>128</option>
                    </select>
                  </div>
                </div>
              </form>
            </li>
            <li><a href="index-half-map.html"><i class="fas fa-map-marker-alt fa-lg"></i></a></li>
            <li><a class="property-list-icon active" href="{{url('listing-list')}}">
              <span></span>
              <span></span>
              <span></span>
            </a></li>
            <li><a class="property-grid-icon" href="{{url('listing')}}">
              <span></span>
              <span></span>
              <span></span>
            </a></li>
          </ul>
        </div>
        <div class="property-item property-col-list mt-4">
        <div class="row mt-4">
        @for($i=0;$i<$j=count($list);$i++)
          <div class="col-sm-6">
          @csrf
            <div class="property-item">
              <div class="property-image bg-overlay-gradient-04">
              @if($list[$i]['Offer']=='Sold')
                  <img class="img" src="{{asset('images/download.png')}}" height="100px" width="300px"alt="" style="position:absolute;transform:rotate(30deg);z-index:1;top:80px;left:40px;">
                  @endif
                <img class="img-fluid" src="images/property/grid/01.jpg" alt="" style="position:relative">
                <div class="property-lable">
                  <span class="badge badge-md badge-primary">{{$list[$i]['Property_Type']}}</span>
                  <span class="badge badge-md badge-info">{{$list[$i]['Offer']}}</span>
                </div>
                <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>
                <div class="property-agent">
                  <div class="property-agent-image">
                
                    <img class="img" src="../profile_images/{{$list[$i]['userdetail']['profile_pic']}}" height="34px" width="60px"alt="">
                  </div>
                  <div class="property-agent-info">
                    <a class="property-agent-name" href="#">{{$list[$i]['userdetail']['firstname']}} {{$list[$i]['userdetail']['lastname']}}</a>
                    <span class="d-block">{{$list[$i]['userdetail']['user_type']}}</span>
                    <ul class="property-agent-contact list-unstyled">
                      <li><a href="#"><i class="fas fa-mobile-alt"></i> </a></li>
                      <li><a href="#"><i class="fas fa-envelope"></i> </a></li>
                    </ul>
                  </div>
                </div>
                <div class="property-agent-popup">
                  <a href="#"><i class="fas fa-camera"></i> 06</a>
                </div>
              </div>
              <div class="property-details">
                <div class="property-details-inner">
                  <h5 class="property-title"><a href="property-detail-style-01.html">{{$list[$i]['Property_title']}}</a></h5>
                  <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>{{$list[$i]['Google_Map_Address']}}</span>
                  <span class="property-agent-date"><i class="far fa-clock fa-md"></i>10 days ago</span>
                  <div class="property-price">${{$list[$i]['Property_price']}}<span> / month</span> </div>
                  <ul class="property-info list-unstyled d-flex">
                    <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>{{$list[$i]['Bedrooms']}}</span></li>
                    <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>{{$list[$i]['Bathrooms']}}</span></li>
                    <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>sqft<span>{{$list[$i]['Area']}}m</span></li>
                  </ul>
                </div>
                <div class="property-btn">
                  <a class="property-link" href="{{url('property/'.$list[$i]['id'])}}">See Details</a>
                  <ul class="property-listing-actions list-unstyled mb-0">
                    <li class="property-compare d-none"><a data-toggle="tooltip" data-placement="top" title="" href="#" data-original-title="Compare"><i class="fas fa-exchange-alt"></i></a></li>
                  @if($list[$i]['savecheck']==1)
                    <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="heart fa fa-heart" data-value="{{$list[$i]['id']}}" id="check" onclick="getid(this)"></i></a></li>
                 @else
                    <li class="property-favourites"><a  data-toggle="tooltip" data-placement="top"  title="Favourite" href="#"><i class="far fa-heart" style="font-size:28px;color:red" data-value="{{$list[$i]['id']}}" id="check" onclick="getid(this)"></i></a></li>
                    @endif
                  
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
          @endfor
          </div>
        </div>
        <div class="row">
          <div class="col-12">
          {{ $list->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@include('emailsection')
@include('footer')
<script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.js')}}"></script>
  <script src="{{asset('libs/saved.js')}}"></script>
  <script>
    $(function(){
  $(".location a").click(function(){
    
    $(".demo:first-child").text($(this).text());
     $(".demo:first-child").val($(this).text());
  });

});
</script>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script>
  <script>
   
      setTimeout(function(){
  $('body').addClass('loaded');
}, 2000);
getid=(data)=>{
        let p_id=$(data).attr('data-value');
        let _token= $("input[name=_token]").val()
        var check=1;
        $('#check').click(function(){
          $(this).removeClass("fa-heart-o")
          $(this).addClass("fa-heart")
        location.reload();
        })       
         let url = "getsaved";
         let formData = new FormData()
         formData.append('p_id',p_id)
         formData.append('check',check)
         formData.append('_token',_token)
         let xhr = new XMLHttpRequest
         xhr.open('POST',url)
         xhr.send(formData)
         xhr.onload = function(){
            let Obj = JSON.parse(xhr.responseText);
            let st = Obj.status;
            let me = Obj.message;
            if(st==false){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    footer: '<span style="color:red">'+me+'<span>',
                  })
                  if(me=='already saved'){
                    $(this).removeClass("fa-heart-o")
                    $(this).addClass("fa-heart")
                  }
                  return false;
            }
            Swal.fire({
                icon: 'success',
                title: me,
                showConfirmButton: false,
                timer: 2000
              })
        location.reload();
           }
}
</script>

</body>
</html>