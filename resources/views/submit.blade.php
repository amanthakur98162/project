
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ghardhundo</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
    <style>
    .custom-file-label::after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    z-index: 3;
    display: block;
    height: calc(1.5em + 1.75rem);
    padding: 10px 20px;
    line-height: 1.5;
    color: #495057;
    content: "Browse";
    background-color: #e9ecef;
    border-left: inherit;
    border-radius: 0 0.25rem 0.25rem 0;
}
    </style>
  </head>
<body>
@include('header')
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title d-flex align-items-center">
        @if(Session::has('error'))
                <div class="alert alert-success mb-3 text-center">{{Session::get('error')}}</div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-danger mb-3 text-center">{{Session::get('success')}}</div>
            @endif
             <div id="msg" class="text-center mb-4 py-3"></div>
          <h2>Submit Property</h2>
        </div>
        <div class="row">
          <div class="col-12">
            <ul class="nav nav-tabs nav-tabs-03 nav-fill" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="tab-01-tab" data-toggle="tab" href="#tab-01" role="tab" aria-controls="tab-01" aria-selected="true">
                  <span>01</span>
                  Basic Information
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="tab-02-tab" data-toggle="tab" href="#tab-02" role="tab" aria-controls="tab-02" aria-selected="false">
                  <span>02</span>
                Gallery</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="tab-03-tab" data-toggle="tab" href="#tab-03" role="tab" aria-controls="tab-03" aria-selected="false"><span>03</span>
                Location</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="tab-04-tab" data-toggle="tab" href="#tab-04" role="tab" aria-controls="tab-04" aria-selected="false">
                  <span>04</span>
                Detailed Information</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="tab-05-tab" data-toggle="tab" href="#tab-05" role="tab" aria-controls="tab-05" aria-selected="false">
                  <span>05</span>
                Floorplans</a>
              </li>
            </ul>
            <div class="tab-content mt-4" id="myTabContent">
              <div class="tab-pane fade show active" id="tab-01" role="tabpanel" aria-labelledby="tab-01-tab">
                 @csrf
                  <div class="form-row">

                      <input type="hidden"id="email" name="email" value=" {{Cookie::get('email')}}{{Session::get('email')}}">
  
                    <div class="form-group col-md-6">
                      <label>Property Title </label>
                      <input type="text" class="form-control" placeholder="Awesome family home" id="Property_Title" name="Property_Title" value="">
                    </div>
                    <div class="form-group col-md-6 select-border">
                      <label>Offer</label>
                      <select class="form-control "id="Offer" name="Offer">
                        <option  selected="selected" data-select2-id="3">For sale</option>
                        <option >For rent</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6 select-border">
                      <label>Rental Period</label>
                      <select class="form-control"tabindex="-1" aria-hidden="true" id="Rental_Period" name="">
                        <option selected="selected" data-select2-id="6">Monthly</option>
                        <option >Yearly</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6 select-border">
                      <label>Property Type</label>
                      <select class="form-control"tabindex="-1" aria-hidden="true" id="Property_Type" name="Property_Type">
                        <option  selected="selected" data-select2-id="9">Apartment</option>
                        <option>Villa</option>
                        <option >Hostel</option>
                        <option>Pg</option>

                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label class="d-block">Property price </label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                        </div>
                        <input type="text" class="form-control" placeholder="Total Amount" id="Property_price" name="Property_price">
                      </div>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Area</label>
                      <input class="form-control" placeholder="Type (sq ft)"id="Area" name="Area">
                    </div>
                    <div class="form-group col-md-6 select-border">
                      <label>Rooms</label>
                      <select class="form-control" aria-hidden="true" id="Rooms" name="Rooms">
                        <option>01</option>
                        <option>02</option>
                        <option >03</option>
                        <option >04</option>
                        <option >05</option>
                        <option >More than 05</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Video</label>
                      <input class="form-control" placeholder="URL to oEmbed supported service" id="Video" name="Video">
                    </div>
              </div>
              </div>
              <div class="tab-pane fade" id="tab-02" role="tabpanel" aria-labelledby="tab-02-tab">
                <div class="custom-file">
                <input type="file" onchange="previewFile()"id="Images" name="Images" style="position:absolute;left:10px; top:10px; z-index:1;opacity:.01;height:100px;width:100px;">
                  <img src="{{asset('images/01.jpg')}}" style="height: 100px;width:100px;border-radius:50%;position:relative;" alt="">
                </div>
              </div>
              <div class="tab-pane fade" id="tab-03" role="tabpanel" aria-labelledby="tab-03-tab">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d109741.02912921079!2d76.6934885768626!3d30.735062644281005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390fed0be66ec96b%3A0xa5ff67f9527319fe!2sChandigarh!5e0!3m2!1sen!2sin!4v1616318783705!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                  <div class="form-row mt-4">
                    <div class="form-group col-md-6">
                      <label>Google Maps Address </label>
                      <input type="text" class="form-control" placeholder="" id="Google_Maps_Address" name="">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Friendly Address </label>
                      <input type="text" class="form-control" placeholder="" id="Friendly_Address" name="Friendly_Address">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Longitude </label>
                      <input type="text" class="form-control" placeholder="-102.243340" id="Longitude" name="Longitude">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Latitude </label>
                      <input type="text" class="form-control" placeholder="21.852270" id="Latitude" name="Latitude">
                    </div>
                    <div class="form-group col-md-12 select-border">
                      <label>Regions</label>
                      <select class="form-control "id="Regions" name="Regions">
                        <option>Chandigarh</option>
                        <option>Mohali</option>
                        <option>Hamirpur</option>
                        <option>Bilaspur</option>
                        <option>Shimla</option>
                        <option>Manali</option>
                        <option>Dharmasala</option>
                        <option>Lahaulspiti</option>
                      </select>
                    </div>
              </div>
              </div>
              <div class="tab-pane fade" id="tab-04" role="tabpanel">
                  <div class="form-row mt-4">
                    <div class="form-group col-md-6 select-border">
                      <label>Building age</label>
                      <select class="form-control" id="Building_age" name="Building_age">
                        <option>0 to 1 years</option>
                        <option>1 to 2 years</option>
                        <option>2 to 4 years</option>
                        <option>4 to 6 years</option>
                        <option>6 to 8 years</option>
                        <option>Above 8 years</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6 select-border">
                      <label>Bedrooms</label>
                      <select class="form-control" id="Bedrooms" name="Bedrooms">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                      </select></div>
                    <div class="form-group col-md-6 select-border">
                      <label>Bathrooms</label>
                      <select class="form-control"id="Bathrooms" name="Bathrooms">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Parking </label>
                      <select class="form-control"id="Parking" name="Parking">
                        <option>Available</option>
                        <option>Not Available</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>AC</label>
                      <select class="form-control"id="AC" name="AC">
                        <option>Available</option>
                        <option>Not Available</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Heater </label>
                      <select class="form-control"id="Heater" name="Heater">
                        <option>Available</option>
                        <option>Not Available</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Sewer </label>
                      <select class="form-control"id="Sewer" name="Sewer">
                        <option>Available</option>
                        <option>Not Available</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Water </label>
                      <select class="form-control"id="Water" name="Water">
                        <option>Available</option>
                        <option>Not Available</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Kitchen </label>
                      <select class="form-control"id="Kitchen" name="Kitchen">
                        <option>Available</option>
                        <option>Not Available</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Balcony</label>
                      <select class="form-control"id="Balcony" name="Balcony">
                        <option>Available</option>
                        <option>Not Available</option>
                      </select>
                    </div>
                  </div>
              </div>
              
              
              <div class="tab-pane fade" id="tab-05" role="tabpanel" aria-labelledby="tab-05-tab">
                 <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Browser file</label>
                      <div class="custom-file">
                       <input  type="File" class="File custom-file-label cutom-file-input form-control" id="File" name="File">
                      </div>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Floorplan Title </label>
                      <input type="text" class="form-control" placeholder="Awesome family home"id="Floorplan_Title" name="Floorplan_Title">
                    </div>
                    <div class="form-group col-md-12">
                      <label>Area</label>
                      <input class="form-control" placeholder="Type (sq ft)"id="Areatype" name="Areatype">
                    </div>
                    <div class="form-group col-md-12">
                      <label>Description</label>
                      <textarea class="form-control" rows="4" placeholder="Description"id="Description" name="Description"></textarea>
                    </div>
                  </div>
                <a class="btn btn-primary mt-3"  onclick="submit()" ><i class="fas fa-plus-circle"id="" name=""></i> Upload floorplans</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('emailsection')
@include('footer')

<script src="{{asset('libs/const.js')}}"></script>
<script src="{{asset('libs/submit.js')}}"></script>
<script>
    $(function(){
  $(".location a").click(function(){
    
    $(".demo:first-child").text($(this).text());
     $(".demo:first-child").val($(this).text());
  });

});
</script>
<script>
function previewFile() {
  var preview = document.querySelector('img');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }
  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}
</script>
</body>
</html>