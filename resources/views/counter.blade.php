

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>DOC</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico" />
    <!-- <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
  </head>
<body>  
<section class="space-pb" id="container">
  <div class="space-ptb bg-holder bg-overlay-black-70" style="background-image:url('{{asset('images/bg/01.jpg')}}');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="text-white display-sm-4">Looking to buy a new property?</h2>
          <p class="text-white mt-4">Success isn’t really that difficult. There is a significant portion of the population here in North America, that actually want and need success to be hard! Why? So they then have a built-in excuse when.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="bg-white p-4 p-md-5 position-relative mt-md-n5 z-index-1 border mt-5">
          <div class="row">
            <div class="col-sm-3 mb-4">
              <div class="counter counter-big text-center">
                <div class="counter-content">
                  <span class="timer mb-1 d-block text-success   counter-value" data-to="4561" data-speed="10000" id="users">4561</span>
                  <label class="mb-0">All Users</label>
                </div>
              </div>
            </div>
            <div class="col-sm-3 mb-4">
              <div class="counter counter-big text-center">
                <div class="counter-content">
                  <span class=" timer mb-1 d-block text-success   counter-value" data-to="3256" data-speed="10000" id="properties">3256</span>
                  <label class="mb-0">Properties listed</label>
                </div>
              </div>
            </div>
            <div class="col-sm-3 mb-4">
              <div class="counter counter-big text-center">
              <div class="counter-content">
                  <span class="timer mb-1 d-block text-success   counter-value" data-to="1452" data-speed="10000" id="user">1452</span>
                  <label class="mb-0">User</label>
                </div>
              </div>
            </div>
            <div class="col-sm-3 mb-4">
              <div class="counter counter-big text-center">
                <div class="counter-content">
                  <span class="timer mb-1 d-block text-success   counter-value" data-to="1452" data-speed="10000" id="broker"></span>
                  <label class="mb-0">Broker</label>
                </div>
              </div>
            </div>
            <div class="col-12 text-center mt-5">
              <a class="btn btn-success" href="#"> Explorer all services</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <script src="jquery/jquery.js"></script> -->
<script>
function isCounterElementVisible($element) {
  var topView = $(window).scrollTop();
  var botView = topView + $(window).height();
  var topElement = $element.offset().top;
  var botElement = topElement + $element.height();
  return ((botElement <= botView) && (topElement >= topView));
}

$(window).scroll(function() {
  $(".counter-value").each(function() {
    isOnView = isCounterElementVisible($(this));
    if (isOnView && !$(this).hasClass('visibled')) {
      $(this).addClass('visibled');
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
        duration: 3000,
        easing: 'swing',
        step: function(now) {
          $(this).text(Math.ceil(now));
        }
      });
    }
  });
});
</script>
<script>
counter();
    browse=[];
        function counter(index){
          let url = 'counter';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('').html(me)
          return false;
      }
      let data =  obj.data;
    browse = data;
    $('#users').html(browse['users']);
    $('#properties').html(browse['properties']);
    $('#user').html(browse['user']);
    $('#broker').html(browse['broker']);
    }}
</script>
</body>
</html>