<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class save extends Model
{
    use HasFactory;
    protected $fillable = ['p_id','check','email','order_id'];
}
