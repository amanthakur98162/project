<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ghardhundo</title>
    <!-- <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}" /> -->

  </head>
<body>
<footer class="footer bg-dark  m-0">
  <div class="container pt-5">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="footer-contact-info">
          <h5 class="text-success mb-4">About Ghardhundo</h5>
          <p class="text-white mb-4">Ghardhundo helped thousands of clients to find the right property for their needs.</p>
          <ul class="list-unstyled mb-0">
            <li> <b> <i class="fas fa-map-marker-alt"></i></b><span>(Hamirpur) Himachal Pardesh</span> </li>
            <li> <b><i class="fas fa-microphone-alt"></i></b><span>+91 70 182 96 941</span> </li>
            <li> <b><i class="fas fa-headset"></i></b><span>support@Ghardhundo.com</span> </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
        <div class="footer-link">
          <h5 class="text-success mb-4">Useful links</h5>
          <ul class="list-unstyled mb-0">
            <li> <a href="{{url('list')}}">Apartment </a></li>
            <li> <a href="{{url('hostel')}}">Hostel</a> </li>
            <li> <a href="{{url('pg')}}">Pg</a> </li>
          </ul>
          <ul class="list-unstyled mb-4">
          <li> <a href="{{url('villa')}}">Villa</a> </li>
          <li><a href="{{url('forrent')}}">For rent</a></li>
            <li><a href="{{url('forsale')}}">For sale</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
        <div class="footer-recent-List">
          <h5 class="text-success mb-4">Recently listed properties</h5>
          <ul class="list-unstyled mb-0">
            <li>
              <div class="footer-recent-list-item">
                <img class="img-fluid" src="{{asset('images/property/list/01.jpg')}}" alt="">
                <div class="footer-recent-list-item-info">
                  <h6 class="text-white"><a class="category font-md mb-2" href="{{url('property')}}" id="recenttitle">Awesome family home</a></h6>
                  <a class="address mb-2 font-sm" href="#"  id="recentlocation">Vermont dr. hephzibah</a>
                  <span class="price text-white"  id="recentprice">$3,456,235 </span>
                </div>
              </div>
            </li>
            <li>
              <div class="footer-recent-list-item">
                <img class="img-fluid" src="{{asset('images/property/list/02.jpg')}}" alt="">
                <div class="footer-recent-list-item-info">
                  <h6 class="text-white"><a class="category font-md mb-2" href="{{url('property')}}" id="recenttitle1">Lawn court villa</a></h6>
                  <a class="address mb-2 font-sm" href="#" id="recentlocation1">Newport st. mebane, nc</a>
                  <span class="price text-white"id="recentprice1"> </span>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
        <h5 class="text-success mb-4">Subscribe us</h5>
        <div class="footer-subscribe">
          <p class="text-white">Sign up to our newsletter to get the latest news and offers.</p>
          <form>
            <div class="form-group">
              <input type="email" class="form-control" placeholder="Enter email">
            </div>
            <button type="submit" class="btn btn-primary btn-sm">Get notified</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4 text-center text-md-left">
          <a href="index.html"><img class="img-fluid footer-logo" src="images/logo-light.svg" alt=""> </a>
        </div>
        <div class="col-md-4 text-center my-3 mt-md-0 mb-md-0">
          <a id="back-to-top" class="back-to-top" href="#"><i class="fas fa-angle-double-up"></i> </a>
        </div>
        <div class="col-md-4 text-center text-md-right">
          <p class="mb-0 text-white"> &copy;Copyright 2021 <a href="#"> Ghar Dhundo </a> All Rights Reserved </p>
        </div>
      </div>
    </div>
  </div>
</footer>
  <!-- <script src="jquery/jquery.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.js"></script> -->
<script>
$(document).ready(function(){
	$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});

		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 1000);
			return false;
		});});
</script>
<script>
      recent();
    agentsDataArray=[];
  function recent(index){
  let url = 'recent';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('').html(me)
          return false;
      }
      let data =  obj.data
      tricksDataArray=data
    $('#recenttitle').html(tricksDataArray[0]['Property_title']);
    $('#recentlocation').html(tricksDataArray[0]['Google_Maps_Address']);
    $('#recentprice').html('$'+tricksDataArray[0]['Property_price']);
    $('#recenttitle1').html(tricksDataArray[1]['Property_title']);
    $('#recentlocation1').html(tricksDataArray[1]['Google_Maps_Address']);
    $('#recentprice1').html('$'+tricksDataArray[1]['Property_price']);
      }}
    </script>
</body>
</html>