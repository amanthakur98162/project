
<section class="testimonial-main bg-holder pt-5 px-5 space-ptb" style="background-image: url('{{asset('images/bg/02.jpg')}}');">
<div class="demoo">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div id="testimonial-slider" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner testimonial">
                    <div class="carousel-item active" data-interval="10000">
                        <div class="testimonial-content">
                            <div class="testimonial-icon">
                                <i class="fa fa-quote-left"></i>
                            </div>
                            <p class="description" id="about1">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent bibendum dolor sit amet eros imperdiet, sit amet hendrerit nisi vehicula.
                             </p>
                            </div>
                        <div class="descript">
                            <h3 class="title" id="name1">williamson</h3>
                            <span class="post"id="user_type1">Web Developer</span>
                        </div>
                  </div>
    <div class="carousel-item" data-interval="2000">
         <div class="testimonial-content">
              <div class="testimonial-icon">
                                <i class="fa fa-quote-left"></i>
                            </div>
                            <p class="description" id="about2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent bibendum dolor sit amet eros imperdiet, sit amet hendrerit nisi vehicula.
                            </p>
                        </div>
                        <div class="descript">
                              <h3 class="title" id="name2">williamson</h3>
                              <span class="post" id="user_type2">Web Developer</span>
                        </div>
                       </div>
                    </div>
                </div>
    <div class="carousel-item">
    <div class="testimonial-content">
                            <div class="testimonial-icon">
                                <i class="fa fa-quote-left"></i>
                            </div>
                            <p class="description" id="about3">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent bibendum dolor sit amet eros imperdiet, sit amet hendrerit nisi vehicula.
                            </p>    <div class="testimonial-icon">
                                <i class="fa fa-quote-left"></i>
                            </div>
                        </div>
                        <div class="descript">
                        <h3 class="title" id="name3">williamson</h3>
                        <span class="post" id="user_type3">Web Developer</span>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>        
  </div>
</section>
<script>
window.onload=function(){
     testmonial(); 
      agents();
      newly();
      discover();
    }
    testemonialDataArray=[];
        function testmonial(index){
          let url='testmonial';
  let xhr = new  XMLHttpRequest();
  xhr.open('get',url);
  xhr.send('');
  xhr.onload = function(){
   // console.log(typeof(xhr.responseText));
      let obj = JSON.parse(xhr.responseText)
    // console.log(obj); 
      let st = obj.status;
      let me = obj.message;
      if(st == false){
        $('').html(me)
          return false;
      }
      let data =  obj.data;
    testemonialDataArray = data;
    $('#about1').html(testemonialDataArray[0]['about']);
    $('#name1').html(testemonialDataArray[0]['firstname']+' '+testemonialDataArray[0]['lastname']);
    $('#user_type1').html(testemonialDataArray[0]['user_type']);
    $('#about2').html(testemonialDataArray[1]['about']);
    $('#name2').html(testemonialDataArray[1]['firstname']+' '+testemonialDataArray[0]['lastname']);
    $('#user_type2').html(testemonialDataArray[1]['user_type']);
    $('#about3').html(testemonialDataArray[2]['about']);
    $('#name3').html(testemonialDataArray[2]['firstname']+' '+testemonialDataArray[0]['lastname']);
    $('#user_type3').html(testemonialDataArray[2]['user_type']);
    }}
</script>