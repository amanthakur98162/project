<?php

namespace App\Http\Controllers;
use \App\Models\Booking;
use Illuminate\Http\Request;

class first extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function signupForm(Request $request)
    {
       // echo "<pre>";
       // print_r($request->all());
        //echo "</pre>";
       // die();
       $signin = new Booking();
       $signin->Username = $request->uname;
       $signin->Useremail = $request->email;
       $signin->BookingDate = $request->bdate;
       $signin->save();
       session()->flash('success','Data inserted successfully');
       return redirect('booksignup');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getParticularData = Booking::where('id',$id)->first();
        if($getParticularData){
            return view('booking.edit_page')->with('editData',$getParticularData);
        }
        session()->flash('error','Details are not in DB');
        return redirect('booksignup');
        // echo "<pre>";
        // print_r($getParticularData);
        // echo "</pre>";
        // echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateForm(Request $request){
        $getParticularData = Booking::where('id',$request->id)->first();
        //echo "<pre>";
        //print_r($request->all());
        //echo "</pre>";
        $getParticularData->Username = $request->username;
        $getParticularData->Useremail = $request->email;
        $getParticularData->BookingDate = $request->bdate;
        $getParticularData->save();
        session()->flash('success','Data updated successfully');
        return redirect('booksignup');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteDetails($id)
    {
        $getParticularData = Booking::where('id',$id)->first();
        if($getParticularData){
            $getParticularData->delete();
            session()->flash('success','Data deleted successfully');
            return redirect('booksignup');
        }
    }
    public function destroy($id)
    {
        //
    }
}
