<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <style>
    
      input, textarea { 
      outline: none;
      }

      h1 {
      margin-top: 0;
      font-weight: 500;
      }
      #form {
      position: relative;
      width: 80%;
      border-radius: 30px;
      background: #fff;
      }
      .form-left-decoration,
      .form-right-decoration {
      content: "";
      position: absolute;
      width: 50px;
      height: 20px;
      border-radius: 20px;
      background: #26ae61;
      }
      .form-left-decoration {
      bottom: 60px;
      left: -30px;
      }
      .form-right-decoration {
      top: 60px;
      right: -30px;
      }
      .form-left-decoration:before,
      .form-left-decoration:after,
      .form-right-decoration:before,
      .form-right-decoration:after {
      content: "";
      position: absolute;
      width: 50px;
      height: 20px;
      border-radius: 30px;
      background: #fff;
      }
      .form-left-decoration:before {
      top: -20px;
      }
      .form-left-decoration:after {
      top: 20px;
      left: 10px;
      }
      .form-right-decoration:before {
      top: -20px;
      right: 0;
      }
      .form-right-decoration:after {
      top: 20px;
      right: 10px;
      }
      .circle {
      position: absolute;
      bottom: 80px;
      left: -55px;
      width: 20px;
      height: 20px;
      border-radius: 50%;
      background: #fff;
      }
      .form-inner {
      padding: 40px;
      }
      .form-inner input,
      .form-inner textarea {
      display: block;
      width: 100%;
      padding: 15px;
      margin-bottom: 10px;
      border: none;
      border-radius: 20px;
      background: silver;
      }
      .form-inner textarea {
      resize: none;
      }
      .form-inner input,
      .form-inner textarea::placeholder {
  color:black;
}
     #btnn {
      width: 100%;
      padding: 10px;
      margin-top: 20px;
      border-radius: 20px;
      border: none;
      background:#26ae61; 
      font-size: 16px;
      font-weight: 400;
      color: #fff;
      }
      #btnn:hover {
      background: #000;
	  color:white;
      } 
      @media (min-width: 568px) {
      form {
      width: 60%;
      }
      }
    </style>
  </head>
  <body>
      <div class="row">
          <div class="col-md-8 decor my-5 mx-auto" id="form" >
      <div class="form-left-decoration"></div>
      <div class="form-right-decoration"></div>
      <div class="circle"></div>
      <div class="form-inner">
        <h1 class="text-center my-4 text-dark">Tips and advice</h1>
        <input type="hidden" placeholder="email" value="{{Session::get('email')}}{{Cookie::get('email')}}" id="gmail">
        <input type="text" placeholder="Tittle" value="" id="title">
        <textarea placeholder="Message..." rows="5" value=""id="msg"></textarea>
        <button  id="btnn" type="submit" onclick="tips()">Submit</button>
      </div>
          </div>
      </div>
   
  </body>
</html>