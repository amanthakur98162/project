
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
<script src="{{asset('jquery.js')}}"></script>
<title>Document</title>
</head>
<body>
<h1 class="text-center my-3" style="color:black;">Update Details</h1>
<div class="container my-5">
<div class="row">
<div class="col-md-6 mx-auto bg-info text-center">
<form action="{{url('updatePaymentForm')}}" method="post" class="text-white">
@csrf
<input type="hidden" name="id" value="{{$editData['id']}}">
<div class="form-group">
<input type="text" name="username"class="form-control my-4" value="{{$editData['username']}}">
</div>
<div class="form-group">
<input type="text" name="email" class="form-control" value="{{$editData['email']}}">
</div>
<div class="form-group">
<input type="text" name="bdate" class="form-control" value="{{$editData['bookingdate']}}">
</div>
<input type="submit" class="btn btn-primary float-right">
</form>
</div>
</div>
</div>
</body>
</html>