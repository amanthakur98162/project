<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\firstcon;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Base;
use App\Http\Controllers\first;
use App\Http\Controllers\paymcont;
use App\Http\Controllers\paymcont2;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
    // Mail::to('amanthakur98162@gmail.com')->send(new TestMail());
    // return'done';
});
Route::view('header','header');
Route::view('footer','footer');
Route::view('banner','banner');
Route::view('browse','browse');
Route::view('agent','agent');
Route::view('counter','counter');
Route::view('discover','discover');
Route::view('emailsection','emailsection');
Route::view('list','list');
Route::view('login','login');
Route::view('looking','looking');
Route::view('newly','newly');
Route::view('newscard','newscard');
Route::view('select2','select2');
Route::view('range1','range1');
Route::view('submit','submit');
Route::view('testmonial','testmonial');
Route::view('dashboard','dashboard');
Route::view('index','index');
Route::view('forgetpassword','forgetpassword');
Route::view('contact','contact');
Route::view('About','About');
Route::view('grid','grid');
Route::view('details','details');
Route::view('property','property');
Route::view('emailsection','emailsection');
Route::view('listing','listing');
Route::view('listing-list','listing-list');
Route::view('search','search');
Route::view('view-listing','view-listing');
Route::view('tips','tips');
Route::view('cart','cart');
Route::view('paymentsucess','paymentsucess');
//Route::view('dash','dash');
Route::view('mail','mail');
Route::view('mail_compose','mail_compose');
Route::view('payment-failed','payment-failed');
// Route::post('/login',[firstcon::class,'insert']);
Route::view('/login','login');
//using xhr
Route::post('signup',[firstcon::class,'Register']);
// verification
Route::get('verify_email/{token}',[firstcon::class,'verify_email']);
//Route::get('dash',[firstcon::class,'newGetData']);
Route::get('newGetData',[firstcon::class,'newGetData']);
//get_profile_data
Route::get('profileData',[firstcon::class,'profileData']);
//updateprofiledata
Route::post('updateprofiledata',[firstcon::class,'updateprofile']);
//update
Route::post('update',[firstcon::class,'update']);
Route::post('updateSubmitformdetails',[firstcon::class,'submitformdetails']);
Route::post('updateSubmitformdetails2',[firstcon::class,'submitformdetails2']);
Route::post('updateSubmitformdetails3',[firstcon::class,'submitformdetails3']);
Route::post('updateSubmitformdetails4',[firstcon::class,'submitformdetails4']);

// editdata
Route::get('delete/{id}',[firstcon::class,'delete']);
Route::get('deletesubmitdetails/{id}',[firstcon::class,'deletesubmitformdata']);
//login
Route::post('login',[firstcon::class,'login']);
//forget
Route::post('forget',[firstcon::class,'forget']);
//forgetpass
Route::get('forgetpassword/{token}',[firstcon::class,'reset_password']);
Route::post('forget_password',[firstcon::class,'forget_pass']);
//dashboard
Route::get('dash',[firstcon::class,'dashboard']);
// logout
Route::get('logout',[firstcon::class,'logout']);
//Submit_Details_form
Route::post('submit',[firstcon::class,'Submit']);
//getPropertydetails
Route::get('getPropertydetails',[firstcon::class,'getPropertydetails']);
Route::get('getPropertydetails2',[firstcon::class,'getPropertydetails2']);
//GetMypropertyData
Route::get('GetMypropertyData',[firstcon::class,'Myproperties']);
Route::get('property/{id}',[firstcon::class,'propertydata']);
Route::get('view-listing/{id}',[firstcon::class,'view_property']);
Route::get('listing-list',[firstcon::class,'listing_list']);
Route::get('listing',[firstcon::class,'listing']);
Route::get('viewprofile/{id}',[firstcon::class,'viewprofile']);
Route::get('ownergrid',[firstcon::class,'owner_grid']);
Route::get('ownerlist',[firstcon::class,'owner_list']);
Route::get('brokergrid',[firstcon::class,'broker_grid']);
Route::get('brokerlist',[firstcon::class,'broker_list']);
Route::get('data',[firstcon::class,'newly']);
Route::get('agents',[firstcon::class,'agent']);
Route::get('carousel',[firstcon::class,'discover']);
Route::get('testmonial',[firstcon::class,'testemonial']);
Route::get('list',[firstcon::class,'list']);
Route::get('hostel',[firstcon::class,'hostel']);
Route::get('pg',[firstcon::class,'pg']);
Route::get('villa',[firstcon::class,'villa']);
Route::get('forrent',[firstcon::class,'forrent']);
Route::get('forsale',[firstcon::class,'forsale']);
Route::get('count',[firstcon::class,'browse']);
Route::post('tips',[firstcon::class,'tips']);
Route::get('tricks',[firstcon::class,'tricks']);
Route::get('recent',[firstcon::class,'recent']);
Route::get('counter',[firstcon::class,'counter']);
Route::post('saved',[firstcon::class,'saved']);
Route::post('hosteldata',[firstcon::class,'getsaved']);
Route::post('pgdata',[firstcon::class,'getsaved']);
Route::post('villadata',[firstcon::class,'getsaved']);
Route::post('forsaledata',[firstcon::class,'getsaved']);
Route::post('forrentdata',[firstcon::class,'getsaved']);
Route::post('getlist',[firstcon::class,'getsaved']);
Route::post('getpg',[firstcon::class,'getsaved']);
Route::post('getsaved',[firstcon::class,'getsaved']);
Route::post('getsavedlisting',[firstcon::class,'getsaved']);
Route::post('/getsavedviewprofile',[firstcon::class,'getsaved']);
Route::post('getsavedviewlisting',[firstcon::class,'getsaved']);
Route::post('/getinproperty',[firstcon::class,'getsaved']);
Route::get('Saved_home',[firstcon::class,'Saved_home']);
Route::post('cart',[firstcon::class,'cart']);
Route::get('getcartdata',[firstcon::class,'getcartdata']);
Route::get('dashcount',[firstcon::class,'dashcount']);
Route::post('removewishlist',[firstcon::class,'removewishlist']);
Route::post('removecart',[firstcon::class,'removecart']);
Route::get('sold',[firstcon::class,'sold']);
// Route::get('search',[firstcon::class,'searchrslt']);
//mail sending
Route::get('send',[MailController::class,'sendEmail']);
Route::post('searchh',[firstcon::class,'search']);
Route::get('search',[firstcon::class,'searchrslt'])->name('search');
//payments
Route::post('/make_payment',[paymcont::class,'make_payment']);
Route::post('/paytm-callback',[paymcont::class,'paytmCallback']);
 Route::get('paymentsignup',[paymcont2::class,'paymentsignup']);
Route::get('payment_data',[paymcont::class,'paymentForm']);
Route::get('edit_payment_data/{a}',[paymcont2::class,'editDetails']);

//Route::get('delete_payment_data/{a}',[PaymentController::class,'deleteDetails']);
Route::post('payments',[paymcont2::class,'paymentSignupForm']);
Route::post('updateForm',[paymcont2::class,'updateForm']);

//bookings
//Route::view('home','book');
Route::get('booksignup',[base::class,'signup']);
Route::post('book',[first::class,'signupForm']);
Route::get('bookingedit_page/{a}',[first::class,'edit']);
Route::post('book',[first::class,'signupForm']);
Route::post('bookingupdateForm',[first::class,'updateForm']);
Route::get('bookingdelete_page/{a}',[first::class,'deleteDetails']);
