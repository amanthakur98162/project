<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ghardhundo</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:300,500,600,700%7CRoboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/select2/select2.css')}}">
     <script src="{{asset('js/select2/select2.full.js')}}"></script>
     <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <style>
    
   .short-by{
       width:179px;
   }
   .short-by-2{
       width:100px;
   }
    </style>
  </head>
<body>
@include('header')
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="section-title mb-3 mb-lg-4">
          <h2><span class="text-success">{{count($list['userdata'])}}</span> Results</h2>
        </div>
      </div>
      <div class="col-md-6">
        <div class="property-filter-tag">
          <ul class="list-unstyled">
            <li><a href="#">Investment <i class="fas fa-times-circle"></i> </a></li>
            <li><a class="filter-clear" href="#">Reset Search <i class="fas fa-redo-alt"></i> </a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
     <div class="col-lg-3 col-md-4 mb-5 mb-md-0">
        <div class="sidebar">
          <div class="widget">
            <div class="widget-title">
              <h6>Featured property</h6>
            </div>
            <div class="property-item mb-0">
              <div class="property-image bg-overlay-gradient-04">
                <img class="img-fluid" src="images/property/grid/06.jpg" alt="">
                <div class="property-lable">
                  <span class="badge badge-md badge-success">Studio</span>
                  <span class="badge badge-md badge-info">New </span>
                </div>
                <div class="property-agent-popup">
                  <a href="#"><i class="fas fa-camera"></i> 02</a>
                </div>
              </div>
              <div class="property-details">
                <div class="property-details-inner">
                  <h5 class="property-title"><a href="{{url('property')}}">184 lexington avenue</a></h5>
                  <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>Hamilton rd. willoughby, oh</span>
                  <span class="property-agent-date"><i class="far fa-clock fa-md"></i>3 years ago</span>
                  <div class="property-price">$236.00<span> / month</span> </div>
                </div>
                <div class="property-btn">
                  <a class="property-link" href="{{url('property')}}">See Details</a>
                </div>
              </div>
          </div>
          </div>
          <div class="widget">
            <div class="widget-title">
              <h6>Recently listed properties</h6>
            </div>
            @for($i=0;$i<$j=count($list['prodata']);$i++)
                <div class="recent-list-item">
                  <img class="img-fluid" src="{{asset('images/property/list/01.jpg')}}" alt="">
                  <div class="recent-list-item-info">
                    <a class="address mb-2" href="{{url('property/'.$list['prodata'][$i]['id'])}}">{{$list['prodata'][$i]['Property_title']}}</a>
                    <span class="text-success">${{$list['prodata'][$i]['Property_price']}}</span>
                  </div>
                </div>
                @endfor
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-8">
        <div class="property-filter d-sm-flex">
          <ul class="property-short list-unstyled d-sm-flex mb-0">
            <li>
              <form class="form-inline">
                <div class="form-group d-lg-flex d-block">
                  <label class="justify-content-start">Short by:</label>
                  <div class="short-by">
                  <select class="form-control js-select2 " js-select2-id="1" tabindex="-1" aria-hidden="true">
                      <option >Date new to old</option>
                      <option>Price Low to High</option>
                      <option>Price High to Low</option>
                      <option>Date Old to New</option>
                      <option>Date New to Old</option>
                    </select>
                  </div>
                </div>
              </form>
            </li>
          </ul>
          <ul class="property-view-list list-unstyled d-flex mb-0">
            <li>
              <form class="form-inline">
                <div class="form-group d-lg-flex d-block">
                  <label class="justify-content-start pr-2">Sort by: </label>
                  <div class="short-by-2">
                  <select class="form-control js-select2 " js-select2-id="1" tabindex="-1" aria-hidden="true">
                      <option >12</option>
                      <option>18 </option>
                      <option>24 </option>
                      <option>64 </option>
                      <option>128</option>
                    </select>
                  </div>
                </div>
              </form>
            </li>
            <li><a href="index-half-map.html"><i class="fas fa-map-marker-alt fa-lg"></i></a></li>
            <li><a class="property-list-icon active" href="{{url('brokerlist')}}">
              <span></span>
              <span></span>
              <span></span>
            </a></li>
            <li><a class="property-grid-icon" href="{{url('brokergrid')}}">
              <span></span>
              <span></span>
              <span></span>
            </a></li>
          </ul>
        </div>
        @for($i=0;$i<$j=count($list['userdata']);$i++)
        <div class="agent agent-03 mt-4">
          <div class="row no-gutters">
            <div class="col-lg-4 text-center border-right">
              <div class="d-flex flex-column h-100">
                <div class="agent-avatar p-3 my-auto">
                  <img class="img" src="../profile_images/{{$list['userdata'][$i]['profile_pic']}}" height="250px" width="250px" alt="">
                </div>
                <div class="agent-listing text-center mt-auto">
                  <a href="#"> <strong class="text-success d-inline-block mr-2">20</strong>Listed Properties </a>
                </div>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="d-flex h-100 flex-column">
                <div class="agent-detail">
                  <div class="d-sm-flex">
                    <div class="agent-name">
                      <h5 class="mb-0"> <a href="#">{{$list['userdata'][$i]['firstname']}} {{$list['userdata'][$i]['lastname']}}</a></h5>
                      <span>{{$list['userdata'][$i]['user_type']}}</span>
                    </div>
                    <div class="agent-social ml-auto mt-2 mt-sm-0">
                      <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i> </a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i> </a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin"></i> </a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="agent-info">
                    <p class="mt-3 mb-3">{{$list['userdata'][$i]['about']}}</p>
                    <ul class="list-unstyled mb-0">
                  
                      <li><strong>Email:&nbsp;</strong>{{$list['userdata'][$i]['email']}}</li>
                    </ul>
                    <ul class="list-unstyled mb-0">
                      <li><strong>Mobile:&nbsp;</strong>{{$list['userdata'][$i]['phone']}}</li>
                    </ul>
                  </div>
                </div>
                <div class="agent-button">
                  <a class="btn btn-light btn-lg btn-block" href="{{url('viewprofile/'.$list['userdata'][$i]['id'])}}">View Profile</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endfor
        <div class="row">
          <div class="col-12">
            <ul class="pagination mt-5">
              <li class="page-item disabled mr-auto">
                <span class="page-link b-radius-none">Prev</span>
              </li>
              <li class="page-item active" aria-current="page"><span class="page-link">1 </span> <span class="sr-only">(current)</span></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item ml-auto">
                <a class="page-link b-radius-none" href="#">Next</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('emailsection')
@include('footer')

  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.js"></script>
<script>
            $(document).ready(function() {
                $(".js-select2").select2();
                // $(".js-select2-multi").select2();

                // $(".large").select2({
                //     dropdownCssClass: "big-drop",
                // });
            });
      </script>
</body>
</html>